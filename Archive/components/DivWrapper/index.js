import React from "react";
import { DivStyle } from "./style";

export const DivWrapper = (props) => {
  const {
    children,
    className,
    width,
    height,
    padding,
    onMouseOver,
    onMouseLeave,
    onClick,
    margin,
    top,
    left,
    bottom,
    bg,
    index,
    bgcolor,
    display,
    filter,
  } = props;
  return (
    <DivStyle
      className={className}
      width={width}
      height={height}
      padding={padding}
      margin={margin}
      top={top}
      left={left}
      bottom={bottom}
      bg={bg}
      index={index}
      bgcolor={bgcolor}
      display={display}
      filter={filter}
      onMouseOver={onMouseOver}
      onMouseLeave={onMouseLeave}
      onClick={onClick}
    >
      {children}
    </DivStyle>
  );
};
