import styled from "styled-components";

export const DivStyle = styled.div`
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  padding: ${(props) => props.padding};
  margin: ${(props) => props.margin};
  z-index: ${(props) => props.index};
  background-image: url(${(props) => props.bg});
  position: relative;
  background-position: center;
  background-repeat: no-repeat;
  background-size: cover;
  background: ${(props) => props.bgcolor};
  display: ${(props) => props.display};
  filter: ${(props) => props.filter};
  &.display-none {
    display: none;
  }
  &.widget-wrapper {
    width: 100%;
    cursor: pointer;
    transition-duration: 2s;
  }
  &.product-header {
    background-color: ${(props) => props.theme.color.red};
    padding: 0.6em 1em;
    border-radius: 7px 7px 0 7px;
  }
  &.product-line {
    width: calc(100% - 10px);
    height: 3px;
    background-color: ${(props) => props.theme.color.red};
    margin-left: 10px;
    margin-top: -3px;
  }
  &.video-bg {
    position: absolute;
    height: 100vh;
    width: 100%;
    overflow: hidden;
    z-index: -1;
    object-fit: cover;
  }
  &.absolute {
    position: absolute;
    top: ${(props) => props.top};
    left: ${(props) => props.left};
    bottom: ${(props) => props.bottom};
  }
  &.shadow {
    -webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.75);
    -moz-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.75);
    box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.75);
  }
  &.hexagonx {
    overflow: hidden;
    visibility: hidden;
    -webkit-transform: rotate(120deg);
    -moz-transform: rotate(120deg);
    -ms-transform: rotate(120deg);
    -o-transform: rotate(120deg);
    transform: rotate(120deg);
    cursor: pointer;
  }
  &.hexagon-in1 {
    overflow: hidden;
    width: 100%;
    height: 100%;
    -webkit-transform: rotate(-60deg);
    -moz-transform: rotate(-60deg);
    -ms-transform: rotate(-60deg);
    -o-transform: rotate(-60deg);
    transform: rotate(-60deg);
  }
  &.hexagon-in2 {
    width: 100%;
    height: 100%;
    background-repeat: no-repeat;
    background-position: 50%;
    background-image: url(${(props) => props.bg});
    visibility: visible;
    display: flex;
    justify-content: center;
    align-items: center;
    -webkit-transform: rotate(-60deg);
    -moz-transform: rotate(-60deg);
    -ms-transform: rotate(-60deg);
    -o-transform: rotate(-60deg);
    transform: rotate(-60deg);
  }
  &.hexagon-in2:hover {
    background-image: linear-gradient(
        rgb(230, 49, 51, 0.5),
        rgb(230, 49, 51, 0.5)
      ),
      url(${(props) => props.bg});
  }

  &.hexagon2 {
    width: 200px;
    height: 400px;
    margin: -80px 0 0 2px;
  }
  ////////////////////////////////////////////////////////////////
  &.hexagon {
    position: relative;
    display: flex;
    justify-content: center;
    align-items: center;
    width: 200px;
    height: 115.47px;
    margin: 57.74px 0;
    background-image: url(${(props) => props.bg});
    background-size: auto 230.9401px;
    background-position: center;
    transition-duration: 0.5s;
  }

  &.hexTop,
  &.hexBottom {
    position: absolute;
    z-index: 1;
    width: 141.42px;
    height: 141.42px;
    overflow: hidden;
    -webkit-transform: scaleY(0.5774) rotate(-45deg);
    -ms-transform: scaleY(0.5774) rotate(-45deg);
    transform: scaleY(0.5774) rotate(-45deg);
    background: inherit;
    left: 29.29px;
  }

  /*counter transform the bg image on the caps*/
  &.hexTop:after,
  &.hexBottom:after {
    content: "";
    position: absolute;
    width: 200px;
    height: 115.47005383792516px;
    -webkit-transform: rotate(45deg) scaleY(1.7321) translateY(-57.735px);
    -ms-transform: rotate(45deg) scaleY(1.7321) translateY(-57.735px);
    transform: rotate(45deg) scaleY(1.7321) translateY(-57.735px);
    -webkit-transform-origin: 0 0;
    -ms-transform-origin: 0 0;
    transform-origin: 0 0;
    background: inherit;
  }

  &.hexTop {
    top: -70.7107px;
  }

  &.hexTop:after {
    background-position: center top;
  }

  &.hexBottom {
    bottom: -70.7107px;
  }

  &.hexBottom:after {
    background-position: center bottom;
  }

  &.hexagon:after {
    content: "";
    position: absolute;
    top: 0px;
    left: 0;
    width: 200px;
    height: 115.4701px;
    z-index: 2;
    background: inherit;
  }
  &.hexagon-hover {
    background-color: #ff8a8b;
    filter: grayscale(100%) brightness(40%) sepia(100%) hue-rotate(-50deg)
      saturate(600%) contrast(0.8);
  }
  :hover {
    &.hexagon {
      background-color: #ff8a8b;
      filter: grayscale(100%) brightness(40%) sepia(100%) hue-rotate(-50deg)
        saturate(600%) contrast(0.8);
    }
    &.widget-wrapper {
      background-image: linear-gradient(rgb(0, 0, 0, 0.5), rgb(0, 0, 0, 0.5)),
        url(${(props) => props.bg});
    }
  }

  @media only screen and (max-width: 768px) {
    &.hexagon2 {
      width: 100px;
      height: 200px;
      margin: -83px 0 0 2px;
    }
    &.hexagon {
      position: relative;
      width: 100px;
      height: 57.74px;
      margin: 28.87px 0;
      background-image: url(${(props) => props.bg});
      background-size: auto 115.4701px;
      background-position: center;
    }

    &.hexTop,
    &.hexBottom {
      position: absolute;
      z-index: 1;
      width: 70.71px;
      height: 70.71px;
      overflow: hidden;
      -webkit-transform: scaleY(0.5774) rotate(-45deg);
      -ms-transform: scaleY(0.5774) rotate(-45deg);
      transform: scaleY(0.5774) rotate(-45deg);
      background: inherit;
      left: 14.64px;
    }

    /*counter transform the bg image on the caps*/
    &.hexTop:after,
    &.hexBottom:after {
      content: "";
      position: absolute;
      width: 100px;
      height: 57.73502691896258px;
      -webkit-transform: rotate(45deg) scaleY(1.7321) translateY(-28.8675px);
      -ms-transform: rotate(45deg) scaleY(1.7321) translateY(-28.8675px);
      transform: rotate(45deg) scaleY(1.7321) translateY(-28.8675px);
      -webkit-transform-origin: 0 0;
      -ms-transform-origin: 0 0;
      transform-origin: 0 0;
      background: inherit;
    }

    &.hexTop {
      top: -35.3553px;
    }

    &.hexTop:after {
      background-position: center top;
    }

    &.hexBottom {
      bottom: -35.3553px;
    }

    &.hexBottom:after {
      background-position: center bottom;
    }

    &.hexagon:after {
      content: "";
      position: absolute;
      top: 0px;
      left: 0;
      width: 100px;
      height: 57.735px;
      z-index: 2;
      background: inherit;
    }
  }
`;
