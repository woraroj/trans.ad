import styled from "styled-components";

export const LogoWrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  font-size: 15px;
  background-color: #3760aa;
  width: 35px;
  height: 35px;
  color: white;
  margin-right: 0.5em;
  transition-duration: 0.25s;
  cursor: pointer;
  &.large {
    width: 50px;
    height: 50px;
  }
  :hover {
    background-color: #e63133;
  }
`;
export const BlueBar = styled.div`
  background-color: #3760aa;
  height: 7px;
  width: 100%;
`;
