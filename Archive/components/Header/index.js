import React, { useState, useEffect } from "react";
import Link from "next/link";
import { DivWrapper, Flex, Text } from "components";
import { Container } from "reactstrap";
import {
  Wrapper,
  A,
  FlagWrapper,
  NavWrapper,
  SlideDrawerStyled,
  SpanStyle,
  HamburgerWrapper,
  ProductWrapper,
  ProductSubWrapper,
} from "./style.js";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHome, faUsers, faSearch } from "@fortawesome/free-solid-svg-icons";
import { useRouter } from "next/router";
import * as Scroll from "react-scroll";
export const Header = () => {
  const router = useRouter();
  let LinkScroll = Scroll.Link;

  const [hover, setHover] = useState();

  const [isCollapsed, setCollapsed] = useState(true);

  useEffect(() => {
    if (!isCollapsed) {
      document.body.classList.add("lock-screen");
    } else {
      document.body.classList.remove("lock-screen");
    }
  }, [isCollapsed]);
  return (
    <DivWrapper className="absolute" width="100%">
      <Container>
        <DivWrapper>
          <Flex justifyContent="space-between">
            <Wrapper className="logo">
              <img src="/assets/logo.png" style={{ width: "180px" }} />
            </Wrapper>
            <Wrapper className="nav">
              <Flex>
                <div
                  style={{
                    position: "relative",
                    display: "flex",
                    cursor: "pointer",
                  }}
                  onMouseOver={() => setHover("home")}
                  onMouseLeave={() => setHover()}
                >
                  <Link href="/en/home">
                    <Wrapper
                      className={
                        hover === "home"
                          ? "active noshadow noradius"
                          : router.route.includes("home")
                          ? "active"
                          : "unactive"
                      }
                    >
                      <A className="active">HOME</A>
                      <FontAwesomeIcon
                        icon={faHome}
                        style={{ color: "white", width: "15px" }}
                      />
                    </Wrapper>
                  </Link>
                  <ProductWrapper
                    className={hover === "home" ? "" : "unactive"}
                  >
                    {router.route.includes("home") ? (
                      <>
                        <LinkScroll to="homeintro" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              1. Introduction
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="partner" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">2. Our Partner</Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                      </>
                    ) : (
                      <>
                        <Link href="/en/home#homeintro">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                1. Introduction
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/home#partner">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                2. Our Partner
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                      </>
                    )}
                  </ProductWrapper>
                  <A
                    className={
                      router.route.includes("home")
                        ? "hide"
                        : hover === "home"
                        ? "hide"
                        : ""
                    }
                  >
                    HOME
                  </A>
                </div>

                <div
                  style={{
                    position: "relative",
                    display: "flex",
                    cursor: "pointer",
                  }}
                  onMouseOver={() => setHover("aboutus")}
                  onMouseLeave={() => setHover()}
                >
                  <Link href="/en/aboutus">
                    <Wrapper
                      className={
                        hover === "aboutus"
                          ? "active noshadow noradius"
                          : router.route.includes("aboutus")
                          ? "active"
                          : "unactive"
                      }
                    >
                      <A className="active">ABOUT US</A>
                      <FontAwesomeIcon
                        icon={faUsers}
                        style={{ color: "white", width: "15px" }}
                      />
                    </Wrapper>
                  </Link>
                  <ProductWrapper
                    className={hover === "aboutus" ? "" : "unactive"}
                  >
                    {router.route.includes("aboutus") ? (
                      <>
                        <LinkScroll to="aboutintro" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              1. Introduction
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="overview" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              2. Overview of Trans.Ad Group's Business
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="cer" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              3. Quality Certification
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="news" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              4. Company news and Reference Project
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                      </>
                    ) : (
                      <>
                        <Link href="/en/aboutus#aboutintro">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                1. Introduction
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/aboutus#overview">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                2. Overview of Trans.Ad Group's Business
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/aboutus#cer">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                3. Quality Certification
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/aboutus#news">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                4. Company news and Reference Project
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                      </>
                    )}
                  </ProductWrapper>
                  <A
                    className={
                      router.route.includes("aboutus")
                        ? "hide"
                        : hover === "aboutus"
                        ? "hide"
                        : ""
                    }
                  >
                    ABOUT US
                  </A>
                </div>

                <div
                  style={{
                    position: "relative",
                    display: "flex",
                    cursor: "pointer",
                  }}
                  onMouseOver={() => setHover("product")}
                  onMouseLeave={() => setHover()}
                >
                  <Link href="/en/product">
                    <Wrapper
                      className={
                        hover === "product"
                          ? "active noshadow noradius"
                          : router.route.includes("product")
                          ? "active  "
                          : "unactive"
                      }
                    >
                      <A className="active">PRODUCTS</A>

                      <FontAwesomeIcon
                        icon={faUsers}
                        style={{ color: "white", width: "15px" }}
                      />
                    </Wrapper>
                  </Link>
                  <ProductWrapper
                    className={hover === "product" ? "" : "unactive"}
                  >
                    <Link
                      href="/en/product/[...type]"
                      as={`/en/product/digital displays`}
                    >
                      <a style={{ textDecoration: "none" }}>
                        <ProductSubWrapper>
                          <Text.H5 className="nowrap">
                            1. Digital Display
                          </Text.H5>
                        </ProductSubWrapper>
                      </a>
                    </Link>
                    <Link
                      href="/en/product/[...type]"
                      as={`/en/product/digital%20content%20management%20platform`}
                    >
                      <a style={{ textDecoration: "none" }}>
                        <ProductSubWrapper>
                          <Text.H5 className="nowrap">
                            2. Digital Content Management Platform
                          </Text.H5>
                        </ProductSubWrapper>
                      </a>
                    </Link>
                    <Link
                      href="/en/product/[...type]"
                      as={`/en/product/communication%20systems`}
                    >
                      <a style={{ textDecoration: "none" }}>
                        <ProductSubWrapper>
                          <Text.H5 className="nowrap">
                            3. Communication Systems
                          </Text.H5>
                        </ProductSubWrapper>
                      </a>
                    </Link>
                    <Link
                      href="/en/product/[...type]"
                      as={`/en/product/network%20security`}
                    >
                      <a style={{ textDecoration: "none" }}>
                        <ProductSubWrapper>
                          <Text.H5 className="nowrap">
                            4. Network Security
                          </Text.H5>
                        </ProductSubWrapper>
                      </a>
                    </Link>
                  </ProductWrapper>

                  <A
                    className={
                      router.route.includes("product")
                        ? "hide"
                        : hover === "product"
                        ? "hide"
                        : ""
                    }
                  >
                    PRODUCTS
                  </A>
                </div>

                <div
                  style={{
                    position: "relative",
                    display: "flex",
                    cursor: "pointer",
                  }}
                  onMouseOver={() => setHover("service")}
                  onMouseLeave={() => setHover()}
                >
                  <Link href="/en/service">
                    <Wrapper
                      className={
                        hover === "service"
                          ? "active noshadow noradius"
                          : router.route.includes("service")
                          ? "active  "
                          : "unactive"
                      }
                    >
                      <A className="active">SERVICES</A>
                      <FontAwesomeIcon
                        icon={faUsers}
                        style={{ color: "white", width: "15px" }}
                      />
                    </Wrapper>
                  </Link>
                  <ProductWrapper
                    className={hover === "service" ? "" : "unactive"}
                  >
                    {router.route.includes("service") ? (
                      <>
                        <LinkScroll to="1" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              1. Project Management
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="2" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              2. Systems Intergration
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="3" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              3. Content Management
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="4" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              4. Maintenance Services
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="5" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              5. Digital Creatives and 3D Content Production
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="6" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap"> 6. Consultant</Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                      </>
                    ) : (
                      <>
                        <Link href="/en/service#1">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                1. Project Management
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/service#2">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                2. Systems Intergration
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/service#3">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                3. Content Management
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/service#4">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                4. Maintenance Services
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/service#5">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                5. Digital Creatives and 3D Content Production
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/service#6">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                6. Consultant
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                      </>
                    )}
                  </ProductWrapper>
                  <A
                    className={
                      router.route.includes("service")
                        ? "hide"
                        : hover === "service"
                        ? "hide"
                        : ""
                    }
                  >
                    SERVICES
                  </A>
                </div>
                <div
                  style={{
                    position: "relative",
                    display: "flex",
                    cursor: "pointer",
                  }}
                  onMouseOver={() => setHover("reference")}
                  onMouseLeave={() => setHover()}
                >
                  <Link href="/en/reference">
                    <Wrapper
                      className={
                        hover === "reference"
                          ? "active noshadow noradius"
                          : router.route.includes("reference")
                          ? "active  "
                          : "unactive"
                      }
                    >
                      <A className="active"> REFERENCES</A>
                      <FontAwesomeIcon
                        icon={faUsers}
                        style={{ color: "white", width: "15px" }}
                      />
                    </Wrapper>
                  </Link>
                  <ProductWrapper
                    className={hover === "reference" ? "" : "unactive"}
                  >
                    {router.route.includes("reference") ? (
                      <>
                        <LinkScroll to="transportation" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              1. Transportation
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="out" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              {" "}
                              2. Out of home media
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="office" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              3. Office building
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="other" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">4. Others</Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                      </>
                    ) : (
                      <>
                        <Link href="/en/reference#transportation">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                1. Transportation
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/reference#out">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                2. Out of home media
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/reference#office">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                3. Office building
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/reference#other">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">4. Others</Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                      </>
                    )}
                  </ProductWrapper>
                  <A
                    className={
                      router.route.includes("reference")
                        ? "hide"
                        : hover === "reference"
                        ? "hide"
                        : ""
                    }
                  >
                    REFERENCES
                  </A>
                </div>
                <div
                  style={{
                    position: "relative",
                    display: "flex",
                    cursor: "pointer",
                  }}
                  onMouseOver={() => setHover("contact")}
                  onMouseLeave={() => setHover()}
                >
                  <Link href="/en/contact">
                    <Wrapper
                      className={
                        hover === "contact"
                          ? "active noshadow noradius"
                          : router.route.includes("contact")
                          ? "active  "
                          : "unactive"
                      }
                    >
                      <A className="active">CONTACT</A>
                      <FontAwesomeIcon
                        icon={faUsers}
                        style={{ color: "white", width: "15px" }}
                      />
                    </Wrapper>
                  </Link>
                  <ProductWrapper
                    className={hover === "contact" ? "" : "unactive"}
                  >
                    {router.route.includes("contact") ? (
                      <>
                        <LinkScroll to="con" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">1. Contact</Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="why" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              2. Why join us and Benefits
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="act" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              3. Company Activities
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="job" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              4. Jobs Opening
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                      </>
                    ) : (
                      <>
                        <Link href="/en/contact#con">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">1. Contact</Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/contact#why">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                2. Why join us and Benefits
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/contact#act">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                3. Company Activities
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/contact#job">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                4. Jobs Opening
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                      </>
                    )}
                  </ProductWrapper>
                  <A
                    className={
                      router.route.includes("contact")
                        ? "hide"
                        : hover === "contact"
                        ? "hide"
                        : ""
                    }
                  >
                    CONTACT
                  </A>
                </div>
              </Flex>
              <FlagWrapper
                className="disable"
                background={`url(/assets/flag-usa.jpg)`}
              />
              <Link href="/">
                <a>
                  <FlagWrapper
                    className="active"
                    background={`url(/assets/flag-thai.png)`}
                  />
                </a>
              </Link>
              <div
                style={{
                  margin: "0 2em",
                  marginTop: "0.8em",
                }}
              >
                <FontAwesomeIcon
                  icon={faSearch}
                  style={{
                    color: "black",
                    width: "15px",
                  }}
                />
              </div>
            </Wrapper>
            <NavWrapper className="responsive-visble " index="10">
              <Hamburger
                setCollapsed={setCollapsed}
                isCollapsed={isCollapsed}
                bgColor="white"
              />
              <SlideDrawerStyled collapsed={isCollapsed}>
                <Flex
                  padding="2em"
                  flexDirection="column"
                  justifyContent="space-between"
                  height="100%"
                  overflow="scroll"
                >
                  <>
                    <Link href="/en/home">
                      <A
                        className={
                          router.route === "/en/home"
                            ? "respon-active red"
                            : "blue"
                        }
                        onClick={() => setCollapsed(!isCollapsed)}
                      >
                        <Text.H2 weight={400}>HOME</Text.H2>
                      </A>
                    </Link>
                    <Link href="/en/aboutus">
                      <A
                        className={
                          router.route === "/en/aboutus"
                            ? "respon-active red"
                            : "blue"
                        }
                        onClick={() => setCollapsed(!isCollapsed)}
                      >
                        <Text.H2 weight={400}>ABOUT US</Text.H2>
                      </A>
                    </Link>
                    <Link href="/en/product">
                      <A
                        className={
                          router.route.includes("product")
                            ? "respon-active red"
                            : "blue"
                        }
                        onClick={() => setCollapsed(!isCollapsed)}
                      >
                        <Text.H2 weight={400}>PRODUCTS</Text.H2>
                      </A>
                    </Link>
                    <Link href="/en/service">
                      <A
                        className={
                          router.route === "/en/service"
                            ? "respon-active red"
                            : "blue"
                        }
                        onClick={() => setCollapsed(!isCollapsed)}
                      >
                        <Text.H2 weight={400}>SERVICES</Text.H2>
                      </A>
                    </Link>
                    <Link href="/en/reference">
                      <A
                        className={
                          router.route === "/en/reference"
                            ? "respon-active red"
                            : "blue"
                        }
                        onClick={() => setCollapsed(!isCollapsed)}
                      >
                        <Text.H2>REFERENCE</Text.H2>
                      </A>
                    </Link>
                    <Link href="/en/contact">
                      <A
                        className={
                          router.route.includes("contact")
                            ? "respon-active red"
                            : "blue"
                        }
                        onClick={() => setCollapsed(!isCollapsed)}
                      >
                        <Text.H2>CONTACT US</Text.H2>
                      </A>
                    </Link>
                  </>
                  <Flex flexDirection="column" margin="5em 0 0 0">
                    <img
                      src="/assets/logo.png"
                      style={{ width: "180px", margin: "0.5em 0" }}
                    />
                    <Text.H5 weight={300} fontColor="white">
                      Trans.Ad Solutions Co.,Ltd. 21th Floor, TST Tower, No. 21
                      Viphawadi-Rangsit Road, Chomphon, Chatuchak, Bangkok 10900
                      TH
                    </Text.H5>
                  </Flex>
                </Flex>
              </SlideDrawerStyled>
            </NavWrapper>
          </Flex>
        </DivWrapper>
      </Container>
    </DivWrapper>
  );
};
const Hamburger = (props) => {
  const { setCollapsed, isCollapsed, isHome, bgColor } = props;

  return (
    <HamburgerWrapper onClick={() => setCollapsed(!isCollapsed)}>
      <SpanStyle
        className="one"
        bgColor={bgColor}
        collapsed={isCollapsed}
      ></SpanStyle>
      <SpanStyle
        bgColor={bgColor}
        collapsed={isCollapsed}
        className="two"
      ></SpanStyle>
      <SpanStyle
        className="three"
        bgColor={bgColor}
        collapsed={isCollapsed}
      ></SpanStyle>
    </HamburgerWrapper>
  );
};
