import styled from "styled-components";

export const Wrapper = styled.div`
  background-color: rgb(255, 255, 255, 0.75);
  display: flex;
  justify-content: center;
  align-content: baseline;
  border-radius: 0 0 25px 25px;
  -webkit-box-shadow: 0px 0px 13px -2px rgba(0, 0, 0, 0.75);
  -moz-box-shadow: 0px 0px 13px -2px rgba(0, 0, 0, 0.75);
  box-shadow: 0px 0px 13px -2px rgba(0, 0, 0, 0.75);
  z-index: 99;
  &.noshadow {
    box-shadow: none;
  }
  &.noradius {
    border-radius: 0 !important;
  }
  &.logo {
    display: flex;
    justify-content: center;
    align-items: center;
    width: 250px;
  }
  &.nav {
    height: 60px;
    padding-top: 5px;
  }

  &.unactive {
    display: flex;
    flex-direction: column;
    align-items: center;
    background-color: ${(props) => props.theme.color.red};
    position: absolute;
    height: 100px;
    margin-top: -150%;
    padding-top: 20px;
    border-radius: 0 0 15px 15px;
    z-index: 99;
    transition-duration: 0.5s;
  }
  &.active {
    display: flex;
    flex-direction: column;
    align-items: center;
    background-color: ${(props) => props.theme.color.red};
    height: 100px;

    margin-top: -20px;
    padding-top: 10px;
    border-radius: 0 0 15px 15px;
    z-index: 99;
    transition-duration: 0.5s;
  }

  @media only screen and (max-width: 1330px) {
    &.nav {
      display: none;
    }
    &.responsive-display-none {
      display: none;
    }
    &.responsive-visble {
      display: block;
    }
  }
`;

export const A = styled.a`
  margin: 1.5em 1.5em;
  font-size: 12px;
  font-weight: 500;
  cursor: pointer;
  &.hide {
    display: none;
  }
  &.active {
    color: ${(props) => props.theme.color.white};
    margin: 0 1.5em 0.5em 1.5em;
  }
  &.blue {
    color: ${(props) => props.theme.color.white};
  }
  &.red {
    color: ${(props) => props.theme.color.red};
  }
  &.respon-active {
    margin-left: -2em;
  }
  :hover {
    color: ${(props) => props.theme.color.red};
    text-decoration: none;
    &.active {
      color: ${(props) => props.theme.color.white};
    }
    &.blue {
      color: ${(props) => props.theme.color.blue};
    }
    &.red {
      color: ${(props) => props.theme.color.white};
    }
  }
  transition-duration: 0.25s;
`;

export const FlagWrapper = styled.div`
  border-radius: 50%;
  width: 30px;
  height: 30px;
  background: ${(props) => props.background};
  background-size: cover;
  margin: 0.5em 0.5em 0 0.5em;
  &.responsive {
    margin: 0 1em 0 0;
  }
  &.active {
    transition-duration: 0.2s;
    cursor: pointer;
    filter: opacity(20%);
    &:hover {
      border: 2px solid rgb(12, 84, 140);
      filter: opacity(100%);
    }
  }

  &.disable {
  }
`;

export const NavWrapper = styled.div`
  display: flex;
  justify-content: ${(props) => props.justifyContent};
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  align-items: center;
  z-index: 99 !important;
  &.responsive-visble {
    display: none;
  }

  @media only screen and (max-width: 1330px) {
    &.responsive-display-none {
      display: none;
    }
    &.responsive-visble {
      display: block;
    }
  }
`;

export const HamburgerWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  height: 100%;
  width: 38px;
  z-index: 99 !important;
  cursor: pointer;
`;

export const SpanStyle = styled.span`
  display: block;

  height: 4px;
  cursor: pointer;
  margin-bottom: 5px;
  position: relative;
  z-index: 99;
  border-radius: 3px;
  transform-origin: 0px 0px;
  transition: transform 0.5s cubic-bezier(0.77, 0.2, 0.05, 1);
  background: 0.5s cubic-bezier(0.77, 0.2, 0.05, 1);
  opacity: 0.55s ease;
  &.one {
    width: 33px;
    transform: ${(props) =>
      props.collapsed ? "none" : "rotate(45deg) translate(-2px,0px)"};
    background: ${(props) =>
      props.collapsed ? "#f22a1e" : `${props.theme.color.red}`};
    transform-origin: ${(props) => (props.collapsed ? "0% 0%" : "none")};
  }
  &.two {
    width: 33px;
    transform-origin: ${(props) => (props.collapsed ? "0% 100%" : "none")};
    opacity: ${(props) => (props.collapsed ? "1" : "0")};
    background: ${(props) => (props.collapsed ? "#45a441" : `${props.red}`)};
    transform: ${(props) =>
      props.collapsed ? "none" : "rotate(0deg) scale(0.2, 0.2)"};
  }
  &.three {
    width: 33px;
    transform: ${(props) =>
      props.collapsed ? "none" : "rotate(-45deg) translate(-5px, 0)"};
    background: ${(props) =>
      props.collapsed ? "#2655a3" : `${props.theme.color.red}`};
  }
`;

export const SlideDrawerStyled = styled.div`
  height: 100%;
  background: rgb(5, 5, 5, 0.95);
  position: fixed;
  display: flex;
  flex-direction: column;
  padding: 50px 2em 2em 2em;
  top: 0;
  right: 0;
  width: 50%;
  z-index: 90;
  box-shadow: 1px 0px 7px rgba(0, 0, 0, 0.5);
  transform: ${(props) =>
    props.collapsed ? "translateX(100%)" : "translateX(0)"};
  transition: transform 0.5s ease-out;
  overflow: scroll;
  @media only screen and (max-width: 575px) {
    width: 100%;
  }
`;

export const ProductWrapper = styled.div`
  display: flex;
  position: absolute;
  flex-direction: column;

  width: ${(props) => props.width};

  background-color: ${(props) => props.theme.color.red};
  margin-top: 55px;
  padding-top: 30px;
  transition-duration: 0.5s;
  border-radius: 0 0 15px 15px;
  -webkit-box-shadow: 0px 10px 13px -2px rgba(0, 0, 0, 0.75);
  -moz-box-shadow: 0px 10px 13px -2px rgba(0, 0, 0, 0.75);
  box-shadow: 0px 10px 13px -2px rgba(0, 0, 0, 0.75);

  &.unactive {
    margin-top: -500%;
    position: absolute;
  }
`;

export const ProductSubWrapper = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding: 15px 15px;
  width: 100%;
  border-top: 1px solid white;
  color: white;
  :hover {
    background-color: ${(props) => props.theme.color.white};
    color: ${(props) => props.theme.color.blue};
  }
`;
