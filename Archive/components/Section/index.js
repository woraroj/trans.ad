import React from "react";
import { SectionStyle } from "./style";
export const Section = (props) => {
  const {
    children,
    className,
    width,
    height,
    bgcolor,
    bg,
    padding,
    minHeight,
    position,
  } = props;
  return (
    <SectionStyle
      className={className}
      width={width}
      height={height}
      minHeight={minHeight}
      bgcolor={bgcolor}
      padding={padding}
      bg={bg}
      position={position}
    >
      {children}
    </SectionStyle>
  );
};
