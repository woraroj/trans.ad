import React from "react";
import { Flex } from "components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFacebookF,
  faInstagram,
  faYoutube,
} from "@fortawesome/free-brands-svg-icons";
import { Text, LogoWrapper } from "./style";
export const SocialBar = () => {

  const handleClick = (url) => {
    window.open(url,"_blank")
  }
  return (
    <div>
      <Text>visit us on</Text>
      <Flex >
        <LogoWrapper onClick={() => handleClick('https://www.facebook.com/TransAD-Solutions-CoLtd-709091799233263/')}>
          <FontAwesomeIcon icon={faFacebookF} style={{width:'15px'}}/>
        </LogoWrapper>
        <LogoWrapper onClick={() => handleClick('https://www.instagram.com/')}>
          <FontAwesomeIcon icon={faInstagram} style={{width:'20px'}}/>
        </LogoWrapper>
        <LogoWrapper onClick={() => handleClick('https://www.youtube.com/')}>
          <FontAwesomeIcon icon={faYoutube} style={{width:'25px'}}/>
        </LogoWrapper>
      </Flex>
    </div>
  );
};
