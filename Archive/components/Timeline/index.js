import React, { useState, useEffect, useRef } from "react";
import { Section, DivWrapper } from "components";
import { Container } from "reactstrap";
import styled from "styled-components";
import Slider from "react-slick";
export const Timeline = () => {
  const settings = {
    dots: false,
    infinite: false,
    slidesToShow: 1,
    slidesToScroll: 1,
    variableWidth: true,
  };
  const settings2 = {
    dots: true,
    infinite: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    variableWidth: true,
  };
  const slider1 = useRef();

  // const history = [{year:'',image:'/public/assets/history/2021.jpeg'}]
  return (
    <Section>
      <DivWrapper height="500px">
        <Slider {...settings}>
          <YearBtn onClick={(e) => slider1.current.slickGoTo(0)}>2021</YearBtn>
          <YearBtn onClick={(e) => slider1.current.slickGoTo(1)}>2020</YearBtn>
          <YearBtn>2019</YearBtn>
          <YearBtn>2018</YearBtn>
          <YearBtn>2017</YearBtn>
          <YearBtn>2016</YearBtn>
          <YearBtn>2015</YearBtn> <YearBtn>2014</YearBtn>
          <YearBtn>2013</YearBtn> <YearBtn>2012</YearBtn>
          <YearBtn>2011</YearBtn>
        </Slider>
        <Slider
          style={{ margin: "2em 0" }}
          {...settings2}
          ref={(slider) => (slider1.current = slider)}
        >
          <ImageWrap>2021</ImageWrap>
          <ImageWrap>2020</ImageWrap>
          <ImageWrap>2019</ImageWrap>
          <ImageWrap>2018</ImageWrap>
          <ImageWrap>2017</ImageWrap>
          <ImageWrap>2016</ImageWrap>
          <ImageWrap>2015</ImageWrap> <ImageWrap>2014</ImageWrap>
          <ImageWrap>2013</ImageWrap> <ImageWrap>2012</ImageWrap>
          <ImageWrap>2011</ImageWrap>
        </Slider>
      </DivWrapper>
    </Section>
  );
};

const YearBtn = styled.div`
  width: 150px !important;
  height: 30px;
  border: 1px solid red;
  display: flex;
  align-items: center;
`;

const ImageWrap = styled.div`
  width: 350px !important;
  height: 350px !important;
  background: grey;
  border: 1px solid white;
`;
