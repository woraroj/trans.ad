import React from "react";
import { Section, Flex, Text, Button } from "components";
import { Container } from "reactstrap";
import Link from "next/link";
import {
  DirectLink,
  Element,
  Events,
  animateScroll as scroll,
  scrollSpy,
  scroller,
} from "react-scroll";
export const AboutIntro = ({ introText, introImg }) => {
  return (
    <Section padding="5em 0">
      <Element name="aboutintro" />
      <Container>
        <Flex className="shadow">
          <Flex className="responsive-hide " width="50%">
            <img
              src={introImg && `${process.env.api_url}` + introImg.url}
              style={{ width: "100%", height: "100%", objectFit: "cover" }}
            />
          </Flex>
          <Flex
            className="responsive-full-width "
            padding="2em"
            width="50%"
            flexDirection="column"
            alignItems="flex-start"
          >
            <Text.H3 weight={500} margin="0 0 1em 0" fontColor="blue">
              INTRODUCTION
            </Text.H3>
            <Text.H5
              space="1px"
              weight={200}
              margin="0 0 1em 0"
              fontColor="grey"
              lineHeight="23px"
            >
              {introText && introText}
            </Text.H5>
            <Link href="/en/reference">
              <a>
                <Button className="inline" bgcolor="blue" margin="2em 0">
                  Reference Project →
                </Button>
              </a>
            </Link>
          </Flex>
        </Flex>
      </Container>
    </Section>
  );
};
