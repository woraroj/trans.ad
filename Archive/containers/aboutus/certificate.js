import React from "react";
import { Section, Flex, DivWrapper, Text } from "components";
import { Container } from "reactstrap";
import {
  DirectLink,
  Element,
  Events,
  animateScroll as scroll,
  scrollSpy,
  scroller,
} from "react-scroll";
export const Certificate = ({ data }) => {
  return (
    <Section padding="2em 0">
      <Element name="cer" />
      <Container>
        <Flex className="shadow responsive-column">
          <Flex className="responsive-full-width " width="50%">
            <img
              src="/assets/cer1.jpeg"
              style={{ width: "100%", height: "100%", objectFit: "cover" }}
            />
          </Flex>
          <Flex
            className="responsive-full-width "
            padding="2em"
            width="50%"
            flexDirection="column"
            alignItems="flex-start"
          >
            <Text.H3 weight={500} margin="0 0 1em 0" fontColor="blue">
              QUALITY CERTIFICATION
            </Text.H3>
            <Text.H4
              space="1px"
              weight={500}
              margin="1em 0"
              fontColor="blue"
              lineHeight="23px"
            >
              Scope
            </Text.H4>
            <Text.H5
              space="1px"
              weight={200}
              margin="0 0 1em 0"
              fontColor="grey"
              lineHeight="23px"
            >
              Provision of Digital Display Products, Communication System, and
              Maintenance Service
            </Text.H5>
            <Text.H4
              space="1px"
              weight={500}
              margin="1em 0"
              fontColor="blue"
              lineHeight="23px"
            >
              Slogan
            </Text.H4>
            <Text.H5
              space="1px"
              weight={200}
              margin="0 0 1em 0"
              fontColor="grey"
              lineHeight="23px"
            >
              Deliver products and services on-time within budget and comply to
              customer requirements
            </Text.H5>
          </Flex>
        </Flex>
      </Container>
    </Section>
  );
};
