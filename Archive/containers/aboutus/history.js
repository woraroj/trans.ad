import React, { useState } from "react";
import { Section, Flex, Text, DivWrapper } from "components";
import {
  Container,
  Nav,
  NavItem,
  NavLink,
  TabContent,
  TabPane,
} from "reactstrap";
import styled from "styled-components";
import {
  DirectLink,
  Element,
  Events,
  animateScroll as scroll,
  scrollSpy,
  scroller,
} from "react-scroll";
export const History = ({ data }) => {
  const [activeTab, setActiveTab] = useState("1");

  const toggle = (tab) => {
    if (activeTab !== tab) setActiveTab(tab);
  };

  return (
    <Section padding="3em 0">
      <Container>
        <Flex flexDirection="column" alignItems="center">
          <Element name="overview" />
          <Text.H3 weight={500} fontColor="blue">
            OVERVIEW OF TRANS.AD GROUP'S BUSINESS
          </Text.H3>
          <Text.H5 weight={400} fontColor="grey">
            A professional provider of integrated communication systems
            solutions
          </Text.H5>
          <DivWrapper margin="2em 0">
            <Nav tabs style={{ borderBottom: "1px solid #f90023" }}>
              {data &&
                data
                  .sort((a, b) => a.index - b.index)
                  .map((tab, index) => (
                    <NavItemStyle
                      className={activeTab === `${tab.index}` && "active"}
                    >
                      <NavLinkStyle
                        className={activeTab === `${tab.index}` && "active"}
                        onClick={() => {
                          toggle(`${tab.index}`);
                        }}
                      >
                        {tab.tabHeader}
                      </NavLinkStyle>
                    </NavItemStyle>
                  ))}
            </Nav>
            <TabContent activeTab={activeTab}>
              {data &&
                data
                  .sort((a, b) => a.index - b.index)
                  .map((tabData, index) => (
                    <TabPaneStyle tabId={`${index + 1}`}>
                      <Flex
                        className="shadow responsive-column"
                        minHeight="500px"
                      >
                        <Flex className="responsive-full-width " width="50%">
                          <img
                            src={
                              data && `${process.env.api_url}` + tabData.img.url
                            }
                            style={{
                              width: "100%",
                              height: "100%",
                              objectFit: "contain",
                            }}
                          />
                        </Flex>
                        <Flex
                          className="responsive-full-width "
                          padding="2em"
                          width="50%"
                          flexDirection="column"
                          alignItems="flex-start"
                          justifyContent="center"
                        >
                          <Text.H3
                            weight={500}
                            margin="0 0 1em 0"
                            fontColor="blue"
                          >
                            {tabData.header}
                          </Text.H3>
                          <Text.H5
                            space="1px"
                            weight={200}
                            margin="0 0 1em 0"
                            fontColor="grey"
                            lineHeight="23px"
                          >
                            {tabData.text}
                          </Text.H5>
                        </Flex>
                      </Flex>
                    </TabPaneStyle>
                  ))}
              {/* <TabPaneStyle tabId="2">
                <Flex className="shadow responsive-column" minHeight="500px">
                  <Flex className="responsive-full-width " width="50%">
                    <img
                      src="/assets/history/his2.png"
                      style={{
                        width: "100%",
                        height: "100%",
                        objectFit: "contain",
                      }}
                    />
                  </Flex>
                  <Flex
                    className="responsive-full-width "
                    padding="2em"
                    width="50%"
                    flexDirection="column"
                    alignItems="flex-start"
                    justifyContent="center"
                  >
                    <Text.H3 weight={500} margin="0 0 1em 0" fontColor="blue">
                      Roctec Technology
                    </Text.H3>
                    <Text.H5
                      space="1px"
                      weight={200}
                      margin="0 0 1em 0"
                      fontColor="grey"
                      lineHeight="23px"
                    >
                      • A major System Integrator & Product Developer in Hong
                      Kong, providing communication systems solution for HK rail
                      transit, airport, government bureaus and other clients
                    </Text.H5>
                    <Text.H5
                      space="1px"
                      weight={200}
                      margin="0 0 1em 0"
                      fontColor="grey"
                      lineHeight="23px"
                    >
                      • Offer maintenance services and reselling of networking
                      related equipment and hardware
                    </Text.H5>
                  </Flex>
                </Flex>
              </TabPaneStyle>
              <TabPaneStyle tabId="3">
                <Flex className="shadow responsive-column" minHeight="500px">
                  <Flex className="responsive-full-width " width="50%">
                    <img
                      src="/assets/history/his3.jpeg"
                      style={{
                        width: "100%",
                        height: "100%",
                        objectFit: "contain",
                      }}
                    />
                  </Flex>
                  <Flex
                    className="responsive-full-width "
                    padding="2em"
                    width="50%"
                    flexDirection="column"
                    alignItems="flex-start"
                    justifyContent="center"
                  >
                    <Text.H3 weight={500} margin="0 0 1em 0" fontColor="blue">
                      Trans.Ad Malaysia
                    </Text.H3>
                    <Text.H5
                      space="1px"
                      weight={200}
                      margin="0 0 1em 0"
                      fontColor="grey"
                      lineHeight="23px"
                    >
                      • Provide digital advertising equipment leasing and
                      support services such as maintenance, content control
                      system and media management
                    </Text.H5>
                  </Flex>
                </Flex>
              </TabPaneStyle>
              */}
            </TabContent>
          </DivWrapper>
        </Flex>
      </Container>
    </Section>
  );
};

const NavItemStyle = styled(NavItem)`
  border: 1px solid rgb(0, 0, 0, 0);
  cursor: pointer;
  transition-duration: 0.25s;

  &.active {
    background-color: ${(props) => props.theme.color.red} !important;
    border: 0;
  }
  :hover {
    border: 1px solid rgb(249, 0, 35, 0.5);
  }
`;

const NavLinkStyle = styled(NavLink)`
  &.active {
    background-color: ${(props) => props.theme.color.red} !important;
    color: ${(props) => props.theme.color.white} !important;
    border: 0;
  }
`;

const TabPaneStyle = styled(TabPane)``;
