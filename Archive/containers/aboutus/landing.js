import React from "react";
import { Section, Flex, Text, Button, SocialBar, DivWrapper } from "components";
import { Container } from "reactstrap";
import VideoCover from "react-video-cover";
export const Landing = ({ data }) => {
  return (
    <Section width="100%">
      <DivWrapper className="video-bg">
        <VideoCover
          videoOptions={{
            src: data && `${process.env.api_url}` + data.videoBg.url,
            autoPlay: "autoplay",
            muted: "muted",
            loop: true,
            playsInline: "playsinline",
          }}
          style={{ zIndex: "0" }}
        />
      </DivWrapper>
      <Container>
        <Flex
          flexDirection="column"
          justifyContent="center"
          alignItems="flex-start"
          height="100vh"
        >
          <Flex
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
            width="100%"
          ></Flex>

          <DivWrapper className="absolute" bottom="15%">
            <SocialBar />
          </DivWrapper>
        </Flex>
      </Container>
    </Section>
  );
};
