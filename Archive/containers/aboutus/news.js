import React, { useState } from "react";
import { Section, Text, Flex, Button } from "components";
import { Container, Modal, ModalHeader, ModalBody } from "reactstrap";
import {
  DirectLink,
  Element,
  Events,
  animateScroll as scroll,
  scrollSpy,
  scroller,
} from "react-scroll";
export const News = ({ data }) => {
  return (
    <Section padding="3em 0" minHeight="500px">
      <Element name="news" />

      <Container>
        <Text.H3 textAlign="center" weight={500} fontColor="blue">
          COMPANY NEWS
        </Text.H3>
        <Flex className="responsive-column " margin="2em 0 0 0" wrap="wrap">
          {data && data.map((news, index) => <NewsBadge news={news} />)}
        </Flex>
      </Container>
    </Section>
  );
};

const NewsBadge = ({ news }) => {
  const [hover, setHover] = useState(false);
  const [modal, setModal] = useState(false);

  const toggle = () => setModal(!modal);

  return (
    <>
      <Modal isOpen={modal} toggle={toggle}>
        <ModalHeader toggle={toggle}> {news.header}</ModalHeader>
        <ModalBody>
          <Flex flexDirection="column">
            <img
              src={news && `${process.env.api_url}` + news.img.url}
              width="100%"
            />
            <Text.H4 fontColor="blue" margin="2em 0 1em 0">
              {news.header}
            </Text.H4>
            <Text.H5 margin="0 0 2em 0">{news.text} </Text.H5>
          </Flex>
        </ModalBody>
      </Modal>
      <Flex
        className="responsive-full-width "
        width="33%"
        justifyContent="flex-start"
        alignItems="flex-start"
      >
        <Flex flexDirection="column" alignItems="flex-start" padding="1.5em">
          <img
            src={news && `${process.env.api_url}` + news.img.url}
            style={{ objectFit: "cover", height: "200px", width: "100%" }}
          />
          <Text.H4 fontColor="blue" margin="0.5em 0">
            {news.header}
          </Text.H4>
          <Text.H5 className="limit" fontColor="grey">
            {news.text}
          </Text.H5>
          <Flex
            onMouseOver={() => setHover(true)}
            onMouseLeave={() => setHover(false)}
            cursor="pointer"
            alignItems="center"
            onClick={() => toggle()}
          >
            <Text.H5
              fontColor="blue"
              margin={hover ? "0.5em 1em" : "0.5em 0.25em"}
            >
              View more
            </Text.H5>
            <Text.H5 fontColor="blue">→ </Text.H5>
          </Flex>
        </Flex>
      </Flex>
      {/* <Flex
        className="responsive-full-width responsive-height250px filter"
        bg={news && `${process.env.api_url}` + news.img.url}
        bgFilter="linear-gradient(rgb(0, 0, 0, 0.5), rgb(0, 0, 0, 0.5))"
        flexDirection="column"
        width="33%"
        minHeight="300px"
        justifyContent="center"
        alignItems="center"
        cursor="pointer"
        onMouseOver={() => setHover(true)}
        onMouseLeave={() => setHover(false)}
      >
        <Text.H3 fontColor="white" weight={hover === true ? 500 : 100}>
          {news.header}
        </Text.H3>

        <Button
          onClick={() => toggle()}
          margin="0.5em 0"
          bgcolor="red"
          className={
            hover === true ? "inline visible-show" : "inline visible-hide"
          }
        >
          View now
        </Button>
      </Flex> */}
    </>
  );
};
