import React, { useState, useEffect } from "react";
import {
  Section,
  Flex,
  DivWrapper,
  Text,
  Button,
  ContactForm,
} from "components";
import { Container, Spinner } from "reactstrap";
import Link from "next/link";
import Slider from "react-slick";
import {
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useRouter } from "next/router";
import {
  DirectLink,
  Element,
  Events,
  animateScroll as scroll,
  scrollSpy,
  scroller,
} from "react-scroll";
export const Content = ({ data, job }) => {
  const router = useRouter();

  useEffect(() => {
    let id = router.asPath.split("#")[1];
    if (data && router.asPath.includes("#")) {
      scroller.scrollTo(`${id}`, {
        duration: 500,
        delay: 0,
        smooth: "easeInOutQuart",
        offset: -200,
      });
    }
  });
  var settings = {
    responsive: [
      {
        breakpoint: 768,
        settings: {
          dots: false,
          autoplay: false,
          autoplaySpeed: 2000,
          speed: 100,
          centerMode: true,
          arrows: false,
          infinite: true,
          slidesToScroll: 1,
          slidesToShow: 1,
          swipeToSlide: true,
          variableWidth: true,
          adaptiveHeight: true,
        },
      },
      {
        breakpoint: 992,
        settings: {
          dots: false,
          autoplay: false,
          autoplaySpeed: 2000,
          speed: 100,
          centerMode: true,
          arrows: true,
          infinite: true,
          slidesToScroll: 1,
          slidesToShow: 1,
          swipeToSlide: true,
          variableWidth: true,
          adaptiveHeight: true,
        },
      },
    ],
    focusOnSelect: true,
    dots: true,
    infinite: true,
    // lazyLoad: true,
    centerMode: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    variableWidth: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };
  function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block" }}
        onClick={onClick}
      >
        <FontAwesomeIcon
          icon={faChevronRight}
          style={{ width: "30px", height: "30px" }}
          color="red"
        />
      </div>
    );
  }

  function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block", left: "-35px" }}
        onClick={onClick}
      >
        <FontAwesomeIcon
          icon={faChevronLeft}
          color="red"
          style={{ width: "30px", height: "30px", marginRight: "10px" }}
        />
      </div>
    );
  }
  return (
    <Section padding="3em 0">
      <Container>
        <Flex className="responsive-column ">
          <Flex
            className="responsive-full-width responsive-order-1"
            alignItems="flex-start"
            flexDirection="column"
            width="50%"
            padding="2em"
          >
            <Element name="con" />
            {/* <ContactForm /> */}
            <ContactInfo />
          </Flex>
          <Flex
            className="responsive-full-width "
            alignItems="center"
            width="50%"
            margin="3em 0 "
          >
            <iframe
              src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15498.059945295825!2d100.5593862!3d13.8080863!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xdde5a95ad9b6cd95!2sTrans.Ad%20Solutions%20Co.%2CLtd.!5e0!3m2!1sen!2sth!4v1627796955139!5m2!1sen!2sth"
              style={{ border: "0", width: "100%" }}
              height="450"
              loading="lazy"
            ></iframe>
          </Flex>
        </Flex>
        <Text.H4
          textAlign="center"
          margin="2em 0 0 0"
          fontColor="blue"
          weight={400}
        >
          Visit our international website
        </Text.H4>
        <Flex wrap="wrap" margin="0 0 2em 0" justifyContent="center">
          <Button
            onClick={() => window.open("https://www.transad.my/", "_blank")}
            className="contact-btn"
          >
            Trans.Ad Malaysia
          </Button>
          <Button
            onClick={() => window.open("https://www.transad.vn/", "_blank")}
            className="contact-btn"
          >
            Trans.Ad Vietnam
          </Button>
          <Button
            onClick={() => window.open("http://roctec.com.hk/", "_blank")}
            className="contact-btn"
          >
            ROTEC Technology
          </Button>
        </Flex>
        <Benefit />

        <Text.H3
          textAlign="left"
          margin="3em 0 2em 0"
          fontColor="blue"
          weight={500}
        >
          COMPANY ACTIVITIES
        </Text.H3>
        <Element name="act" />
        {data ? (
          <DivWrapper margin="2em 0">
            <Slider {...settings}>
              {data &&
                data.party.map((ref, index) => (
                  <PhotoWrapper
                    img={ref && `${process.env.api_url}` + ref.url}
                    text={ref.caption}
                  />
                ))}
            </Slider>
          </DivWrapper>
        ) : (
          <Flex height="500px" justifyContent="center" alignItems="center">
            <Spinner color="danger" />
          </Flex>
        )}
        <Flex
          margin="2em 0"
          flexDirection="column"
          width="100%"
          alignItems="flex-start"
        >
          <DivWrapper className="product-header">
            <Text.H4 fontColor="white" weight={400}>
              JOBS OPENING
            </Text.H4>
          </DivWrapper>
          <Element name="job" />
          <DivWrapper className="product-line"></DivWrapper>
        </Flex>

        <div>
          <img src="/assets/photo-007.jpeg" width="100%" />
          <Flex className="responsive-column " wrap="wrap">
            {job && job.map((jobData, index) => <JobWrapper job={jobData} />)}
          </Flex>
        </div>
      </Container>
    </Section>
  );
};
const PhotoWrapper = ({ img, text }) => {
  const [hover, setHover] = useState(false);
  return (
    <Flex
      className="bg-img about-ref-wrapper"
      onMouseOver={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
      bg={img}
    >
      <Flex
        className={hover ? "" : "hide"}
        bgcolor="xRed"
        justifyContent="center"
        alignItems="center"
        width="100%"
        height="100%"
      >
        <Text.H4 fontColor="white"> {text}</Text.H4>
      </Flex>
    </Flex>
  );
};
const ContactInfo = () => {
  return (
    <>
      <Text.H2 fontColor="blue" weight={500}>
        CONTACT US
      </Text.H2>
      <img src="/assets/office.jpeg" width="100%" style={{ margin: "1em 0" }} />
      <Text.H4 margin="0 0 0.5em 0" fontColor="blue">
        Trans.Ad Solutions Co., Ltd.
      </Text.H4>
      <Text.H5 margin="0 0 1em 0" fontColor="grey">
        21, TST Tower, 21/F, Vibhavadi-Rangsit Road, Chomphon, Chatuchak,
        Bangkok 10900 Thailand
      </Text.H5>
      <Text.H5 margin="0 0 1em 0" fontColor="grey">
        <a href="tel:+66020019900"> Tel: +66 (0) 2-001-9900-2</a>
      </Text.H5>
      <Text.H5 margin="0 0 1em 0" fontColor="grey">
        Fax: +66 (0) 2-001-9903
      </Text.H5>
      <Text.H5 margin="0 0 1em 0" fontColor="grey">
        <a href="mailto:info@transad.co.th ">Email: info@transad.co.th </a>
      </Text.H5>
    </>
  );
};
const Benefit = () => {
  return (
    <Flex flexDirection="column" alignItems="flex-start">
      <Element name="why" />
      <Text.H3 weight={500} fontColor="blue" textAlign="left" margin="1em 0">
        WHY JOIN US ?
      </Text.H3>
      <Text.H5 fontColor="grey" lineHeight="25px">
        Trans.Ad is a leading company in digital displays and communication
        systems. We have grown exponentially in the past years and continue to
        grow in the future. At Trans.Ad, we believe that corporate success comes
        from the well being of everyone in the workplace. We strive to offer our
        employees stimulating working environments, safe and pleasant spaces,
        motivating remuneration and respect for work/life balance. We value
        personal and professional development of all our employees that is why
        we provide necessary training to enhance our employees’ skills and to
        give them opportunity to work with international team of expertise.
      </Text.H5>
      <Text.H3
        weight={500}
        fontColor="blue"
        textAlign="left"
        margin="2em 0 0 0"
      >
        BENEFITS
      </Text.H3>
      <Flex
        flexDirection="column"
        margin="2em 0"
        width="100%"
        alignItems="center"
      >
        {/* //////////////////////////////////////////////////////////////////////////// */}
        <Flex
          margin="2em 0
           0 0 "
          justifyContent="center"
          className="hexagon-img2-wrap"
          height="100%"
        >
          <Flex
            className="hexagon-img2"
            position="relative"
            justifyContent="center"
            flexDirection="column"
            alignItems="center"
            bgcolor="#45a441"
          >
            <Flex className="hexTop-img2"></Flex>
            <Flex className="hexBottom-img2"></Flex>
            <img
              src="/assets/perks bonuses.svg"
              style={{ width: "50%", zIndex: "5" }}
            />
            <Text.H6
              className="bring-front"
              width="100%"
              textAlign="center"
              fontColor="white"
            >
              PERKS BONUS
            </Text.H6>
          </Flex>
          <Flex
            className="hexagon-img2"
            position="relative"
            justifyContent="center"
            flexDirection="column"
            alignItems="center"
            bgcolor="#2655a3"
          >
            <Flex className="hexTop-img2"></Flex>
            <Flex className="hexBottom-img2"></Flex>
            <img
              src="/assets/payment paise.svg"
              style={{ width: "50%", zIndex: "5" }}
            />
            <Text.H6
              className="bring-front"
              width="100%"
              textAlign="center"
              fontColor="white"
            >
              PAYMEN PAISE
            </Text.H6>
          </Flex>
          <Flex
            className="hexagon-img2"
            position="relative"
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
            bgcolor="#f22a1e"
          >
            <Flex className="hexTop-img2"></Flex>
            <Flex className="hexBottom-img2"></Flex>
            <img
              src="/assets/social security.svg"
              style={{ width: "50%", zIndex: "5" }}
            />
            <Text.H6
              className="bring-front"
              width="100%"
              textAlign="center"
              fontColor="white"
            >
              SOCIAL SECURITY
            </Text.H6>
          </Flex>
          {/* <Flex
            className="hexagon-img2"
            position="relative"
            justifyContent="center"
            flexDirection="column"
            alignItems="center"
            bgcolor="#45a441"
          >
            <Flex className="hexTop-img2"></Flex>
            <Flex className="hexBottom-img2"></Flex>
            <img
              src="/assets/perks bonuses.svg"
              style={{ width: "50%", zIndex: "5" }}
            />
            <Text.H6
              className="bring-front"
              width="100%"
              textAlign="center"
              fontColor="white"
            >
              PROVIDENT FUND
            </Text.H6>
          </Flex> */}
        </Flex>
        {/* //////////////////////////////////////////////////////////////////////////// */}
        <Flex
          margin="-53.74px  0 0 0 "
          className="hexagon-img2-wrap"
          justifyContent="center"
          height="100%"
        >
          <Flex
            className="hexagon-img2"
            position="relative"
            justifyContent="center"
            flexDirection="column"
            alignItems="center"
            bgcolor="#f22a1e"
          >
            <Flex className="hexTop-img2"></Flex>
            <Flex className="hexBottom-img2"></Flex>
            <img
              src="/assets/Health insurance.svg"
              style={{ width: "50%", zIndex: "5" }}
            />
            <Text.H6
              className="bring-front"
              width="100%"
              textAlign="center"
              fontColor="white"
            >
              HEALTH INSURANCE
            </Text.H6>
          </Flex>
          <Flex
            className="hexagon-img2"
            position="relative"
            justifyContent="center"
            flexDirection="column"
            alignItems="center"
            bgcolor="#2655a3"
          >
            <Flex className="hexTop-img2"></Flex>
            <Flex className="hexBottom-img2"></Flex>
            <img
              src="/assets/Paid vacation.svg"
              style={{ width: "50%", zIndex: "5" }}
            />
            <Text.H6
              className="bring-front"
              width="100%"
              textAlign="center"
              fontColor="white"
            >
              PAID VACATION
            </Text.H6>
          </Flex>
          {/* <Flex
            className="hexagon-img2"
            position="relative"
            justifyContent="center"
            flexDirection="column"
            alignItems="center"
            bgcolor="#45a441"
          >
            <Flex className="hexTop-img2"></Flex>
            <Flex className="hexBottom-img2"></Flex>
            <img
              src="/assets/Company Activities.svg"
              style={{ width: "50%", zIndex: "5" }}
            />
            <Text.H6
              className="bring-front"
              width="100%"
              textAlign="center"
              fontColor="white"
            >
              COMPANY ACTIVITIES
            </Text.H6>
          </Flex> */}
          {/* <Flex
            className="hexagon-img2"
            position="relative"
            justifyContent="center"
            flexDirection="column"
            alignItems="center"
            bgcolor="#45a441"
          >
            <Flex className="hexTop-img2"></Flex>
            <Flex className="hexBottom-img2"></Flex>
            <img
              src="/assets/payment paise.svg"
              style={{ width: "50%", zIndex: "5" }}
            />
            <Text.H6
              className="bring-front"
              width="100%"
              textAlign="center"
              fontColor="white"
            >
              WELFARES
            </Text.H6>
          </Flex> */}
        </Flex>
        {/* //////////////////////////////////////////////////////////////////////////// */}
        <Flex
          margin="-53.74px  0 0 0 "
          justifyContent="center"
          className="hexagon-img2-wrap"
          maxWidth="800px"
          height="100%"
        >
          <Flex
            className="hexagon-img2"
            position="relative"
            justifyContent="center"
            flexDirection="column"
            alignItems="center"
            bgcolor="#45a441"
          >
            <Flex className="hexTop-img2"></Flex>
            <Flex className="hexBottom-img2"></Flex>
            <img
              src="/assets/Provident fund.svg"
              style={{ width: "50%", zIndex: "5" }}
            />
            <Text.H6
              className="bring-front"
              width="100%"
              textAlign="center"
              fontColor="white"
            >
              PROVIDENT FUND
            </Text.H6>
          </Flex>
          <Flex
            className="hexagon-img2"
            position="relative"
            justifyContent="center"
            flexDirection="column"
            alignItems="center"
            bgcolor="#45a441"
          >
            <Flex className="hexTop-img2"></Flex>
            <Flex className="hexBottom-img2"></Flex>
            <img
              src="/assets/welfare.svg"
              style={{ width: "50%", zIndex: "5" }}
            />
            <Text.H6
              className="bring-front"
              width="100%"
              textAlign="center"
              fontColor="white"
            >
              WELFARES
            </Text.H6>
          </Flex>
          <Flex
            className="hexagon-img2"
            position="relative"
            justifyContent="center"
            flexDirection="column"
            alignItems="center"
            bgcolor="#45a441"
          >
            <Flex className="hexTop-img2"></Flex>
            <Flex className="hexBottom-img2"></Flex>
            <img
              src="/assets/Company Activities.svg"
              style={{ width: "50%", zIndex: "5" }}
            />
            <Text.H6
              className="bring-front"
              width="100%"
              textAlign="center"
              fontColor="white"
            >
              COMPANY ACTIVITIES
            </Text.H6>
          </Flex>
        </Flex>
      </Flex>
    </Flex>
  );
};
const JobWrapper = ({ job }) => {
  return (
    <Flex
      className="responsive-full-width "
      width="50%"
      justifyContent="flex-start"
      alignItems="center"
    >
      <img
        src={"/assets/JOBS OPENING1.svg"}
        style={{ objectFit: "cover", width: "120px" }}
      />
      <Flex flexDirection="column" padding="1.5em">
        <Text.H4 fontColor="blue">{job.department}</Text.H4>
        <Text.H5>{job.name}</Text.H5>
        <Link href="/en/contact/[job]" as={`/en/contact/${job.name}`}>
          <a>
            <Button bgcolor="red" margin="1em 0 0 0">
              Visit
            </Button>
          </a>
        </Link>
      </Flex>
    </Flex>
  );
};
