import React from "react";
import { Section, Flex, Text, DivWrapper } from "components";
import { Container } from "reactstrap";
import Slider from "react-slick";
import {
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
export const Certified = () => {
  var settings = {
    responsive: [
      {
        breakpoint: 768,
        settings: {
          dots: true,
          autoplay: false,
          autoplaySpeed: 2000,
          speed: 200,

          arrows: false,
          infinite: true,
          slidesToScroll: 1,
          slidesToShow: 1,
          swipeToSlide: true,
          variableWidth: true,
          adaptiveHeight: false,
        },
      },
      {
        breakpoint: 992,
        settings: {
          dots: true,
          autoplay: false,
          autoplaySpeed: 2000,
          speed: 200,
          centerMode: true,
          arrows: true,
          infinite: true,
          slidesToScroll: 1,
          slidesToShow: 1,
          swipeToSlide: true,
          variableWidth: true,
          adaptiveHeight: true,
        },
      },
    ],
    focusOnSelect: true,
    dots: true,
    infinite: true,
    centerMode: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    variableWidth: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };
  function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block" }}
        onClick={onClick}
      >
        <FontAwesomeIcon
          icon={faChevronRight}
          style={{ width: "30px", height: "30px" }}
          color="red"
        />
      </div>
    );
  }

  function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block", left: "-35px" }}
        onClick={onClick}
      >
        <FontAwesomeIcon
          icon={faChevronLeft}
          color="red"
          style={{ width: "30px", height: "30px", marginRight: "10px" }}
        />
      </div>
    );
  }
  return (
    <Section>
      <Container>
        <Flex flexDirection="column" alignItems="center">
          <Text.H3 weight={500} fontColor="blue">
            CERTIFIED
          </Text.H3>
          <Text.H5 space="1px" weight={200} fontColor="grey">
            Example intro Example intro Example intro
          </Text.H5>
        </Flex>
        <DivWrapper className="slwrapper" padding="3em 0">
          <Slider {...settings}>
            <Flex className="hexagon" color="rgb(236, 237, 239)"></Flex>
            <Flex className="hexagon" color="rgb(236, 237, 239)"></Flex>
            <Flex className="hexagon" color="rgb(236, 237, 239)"></Flex>
            <Flex className="hexagon" color="rgb(236, 237, 239)"></Flex>
            <Flex className="hexagon" color="rgb(236, 237, 239)"></Flex>
            <Flex className="hexagon" color="rgb(236, 237, 239)"></Flex>
          </Slider>
        </DivWrapper>
      </Container>
    </Section>
  );
};
