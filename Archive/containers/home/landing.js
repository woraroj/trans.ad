import React from "react";
import { Section, Flex, Text, Button, SocialBar, DivWrapper } from "components";
import { Container } from "reactstrap";
import Slider from "react-slick";
import Link from "next/link";
import {
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
export const Landing = ({ bg }) => {
  var settings = {
    responsive: [
      {
        breakpoint: 768,
        settings: {
          dots: false,
          infinite: true,
          speed: 500,
          autoplay: true,
          autoplaySpeed: 5000,
          arrows: true,
          slidesToShow: 1,
          slidesToScroll: 1,
          swipeToSlide: true,
          arrows: false,
        },
      },
    ],
    infinite: true,
    speed: 500,
    autoplay: true,
    autoplaySpeed: 5000,
    arrows: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    swipeToSlide: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };
  function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block", right: "35px", zIndex: "99" }}
        onClick={onClick}
      >
        <FontAwesomeIcon
          icon={faChevronRight}
          style={{ width: "30px", height: "30px" }}
          color="red"
        />
      </div>
    );
  }

  function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block", left: "35px", zIndex: "99" }}
        onClick={onClick}
      >
        <FontAwesomeIcon
          icon={faChevronLeft}
          color="red"
          style={{ width: "30px", height: "30px", marginRight: "10px" }}
        />
      </div>
    );
  }
  return (
    <Section width="100%">
      <DivWrapper
        className="absolute home"
        width="100vw"
        height="100vh"
        index="0"
      >
        <Slider {...settings}>
          {bg &&
            bg.map((data, index) => (
              <DivWrapper
                bg={bg && `${process.env.api_url}` + data.url}
                height="100vh"
                width="100vw"
                key={index}
              />
            ))}
        </Slider>
      </DivWrapper>
      <Container>
        <Flex
          position="relative"
          flexDirection="column"
          justifyContent="center"
          alignItems="flex-start"
          height="100vh"
        >
          <Text.H2
            className="text-shadow"
            weight={500}
            space="2.5px"
            fontColor="white"
          >
            TRANS.AD SOLUTION
          </Text.H2>
          <Text.H5
            className="text-shadow"
            fontColor="white"
            weight={300}
            margin="1em 0 1.5em 0"
          >
            One Stop Solutions Provider for Communication & Digital Display
            System
          </Text.H5>
          <Link href="/en/reference">
            <a>
              <Button bgcolor="red">Reference Project →</Button>
            </a>
          </Link>

          <DivWrapper margin="5em 0 0 0">
            <SocialBar />
          </DivWrapper>
        </Flex>
      </Container>
    </Section>
  );
};
