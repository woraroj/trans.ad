import React, { useState } from "react";
import { Section, Flex, Text, DivWrapper } from "components";
import { Container } from "reactstrap";
import CountUp from "react-countup";
import VisibilitySensor from "react-visibility-sensor";
import {
  DirectLink,
  Element,
  Events,
  animateScroll as scroll,
  scrollSpy,
  scroller,
} from "react-scroll";
export const Partner = ({ customerImg }) => {
  const [sensorActive, setSensorActive] = useState(false);
  const [onHover, setHover] = useState();
  return (
    <Section padding="2em 0">
      <Element name="partner" />
      <Container>
        <Flex flexDirection="column" alignItems="center">
          <Flex justifyContent="center" width="100%" alignItems="flex-end">
            <Flex
              width="95px"
              margin="3em 0.5em"
              flexDirection="column"
              alignItems="center"
            >
              <Text.H3 margin="0.5em 0" weight={500} fontColor="blue">
                <CountUp start={sensorActive ? 0 : null} end={300} duration={6}>
                  {({ countUpRef, start }) => (
                    <VisibilitySensor
                      onChange={(isVisible) => {
                        if (isVisible) {
                          setSensorActive(true);
                        }
                      }}
                    >
                      <span ref={countUpRef} />
                    </VisibilitySensor>
                  )}
                </CountUp>
              </Text.H3>
              <Text.H5 fontColor="grey">CUSTOMERS</Text.H5>
            </Flex>
            <Flex
              width="95px"
              margin="3em 0.5em"
              flexDirection="column"
              alignItems="center"
            >
              <Text.H5 fontColor="grey" textAlign="center">
                MORE THAN
              </Text.H5>
              <Text.H3 margin="0.5em 0" weight={500} fontColor="blue">
                <CountUp
                  start={sensorActive ? 0 : null}
                  end={500}
                  duration={6.5}
                >
                  {({ countUpRef, start }) => (
                    <VisibilitySensor
                      onChange={(isVisible) => {
                        if (isVisible) {
                          setSensorActive(true);
                        }
                      }}
                    >
                      <span ref={countUpRef} />
                    </VisibilitySensor>
                  )}
                </CountUp>
              </Text.H3>
              <Text.H5 fontColor="grey">PROJECTS</Text.H5>
            </Flex>
            <Flex
              width="95px"
              margin="3em 0.5em"
              flexDirection="column"
              alignItems="center"
            >
              <Text.H3 margin="0.5em 0" weight={500} fontColor="blue">
                <CountUp start={sensorActive ? 0 : null} end={24} duration={7}>
                  {({ countUpRef, start }) => (
                    <VisibilitySensor
                      onChange={(isVisible) => {
                        if (isVisible) {
                          setSensorActive(true);
                        }
                      }}
                    >
                      <span ref={countUpRef} />
                    </VisibilitySensor>
                  )}
                </CountUp>
              </Text.H3>
              <Text.H5 fontColor="grey">YEARS</Text.H5>
            </Flex>
          </Flex>
          <Text.H3 weight={500} margin="2em 0 0 0" fontColor="blue">
            OUR PARTNER
          </Text.H3>
          <Text.H5 space="1px" weight={200} fontColor="grey">
            Proudly Partner
          </Text.H5>

          <Flex
            margin="5em 0 0 0"
            justifyContent="center"
            maxWidth="800px"
            height="100%"
          >
            <Flex
              className=" hexagon-img"
              position="relative"
              justifyContent="center"
              alignItems="center"
              content="content"
              onMouseOver={() => setHover(1)}
              onMouseLeave={() => setHover()}
            >
              <Flex className="hexTop-img"></Flex>
              <Flex className="hexBottom-img"></Flex>
              <img
                src={
                  customerImg && `${process.env.api_url}` + customerImg[0].url
                }
                style={{ width: "40%", zIndex: "5" }}
              />
              {onHover === 1 && (
                <DivWrapper
                  className="absolute d-flex justify-content-center"
                  index="99"
                >
                  <Text.H3 fontColor="white">BTS</Text.H3>
                </DivWrapper>
              )}
            </Flex>
            <Flex
              margin="0.5em"
              className=" hexagon-img"
              position="relative"
              justifyContent="center"
              alignItems="center"
              onMouseOver={() => setHover(2)}
              onMouseLeave={() => setHover()}
            >
              <Flex className="hexTop-img"></Flex>
              <Flex className="hexBottom-img"></Flex>
              <img
                src={
                  customerImg && `${process.env.api_url}` + customerImg[1].url
                }
                style={{ width: "90%", zIndex: "5" }}
              />
              {onHover === 2 && (
                <DivWrapper
                  className="absolute d-flex justify-content-center"
                  index="99"
                >
                  <Text.H3 fontColor="white">VGI</Text.H3>
                </DivWrapper>
              )}
            </Flex>

            <Flex
              margin="0.5em"
              className=" hexagon-img"
              position="relative"
              justifyContent="center"
              alignItems="center"
              onMouseOver={() => setHover(3)}
              onMouseLeave={() => setHover()}
            >
              <Flex className="hexTop-img"></Flex>
              <Flex className="hexBottom-img"></Flex>
              <img
                src={
                  customerImg && `${process.env.api_url}` + customerImg[2].url
                }
                style={{ width: "90%", zIndex: "5" }}
              />
              {onHover === 3 && (
                <DivWrapper
                  className="absolute d-flex justify-content-center"
                  index="99"
                >
                  <Text.H3 fontColor="white">MACO</Text.H3>
                </DivWrapper>
              )}
            </Flex>
          </Flex>
          <Flex className="hexagon-img2-wrap" margin="-54.74px 0.5em 3em 0.5em">
            <Flex
              margin="0.5em"
              className=" hexagon-img"
              justifyContent="center"
              alignItems="center"
              position="relative"
              onMouseOver={() => setHover(4)}
              onMouseLeave={() => setHover()}
            >
              <Flex className="hexTop-img"></Flex>
              <Flex className="hexBottom-img"></Flex>
              <img
                src={
                  customerImg && `${process.env.api_url}` + customerImg[3].url
                }
                style={{ width: "70%", zIndex: "5" }}
              />
              {onHover === 4 && (
                <DivWrapper
                  className="absolute d-flex justify-content-center"
                  index="99"
                >
                  <Text.H3 fontColor="white">PLAN B</Text.H3>
                </DivWrapper>
              )}
            </Flex>
            <Flex
              margin="0.5em"
              justifyContent="center"
              className="hexagon-img"
              position="relative"
              alignItems="center"
              onMouseOver={() => setHover(5)}
              onMouseLeave={() => setHover()}
            >
              <Flex className="hexTop-img"></Flex>
              <Flex className="hexBottom-img"></Flex>
              <img
                src={
                  customerImg && `${process.env.api_url}` + customerImg[4].url
                }
                style={{ width: "50%", zIndex: "5" }}
              />
              {onHover === 5 && (
                <DivWrapper
                  className="absolute d-flex justify-content-center"
                  index="99"
                >
                  <Text.H3 fontColor="white">TOP NEWS</Text.H3>
                </DivWrapper>
              )}
            </Flex>
          </Flex>
        </Flex>
      </Container>
    </Section>
  );
};
