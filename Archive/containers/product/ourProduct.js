import React, { useState } from "react";
import { Section, Flex, Text, DivWrapper, Button } from "components";
import { Container, Collapse } from "reactstrap";
import Link from "next/link";
export const OurProduct = ({ productTypeData }) => {
  return (
    <Section padding="3em 0">
      <Container>
        <Text.H3 margin="0 0 1em 0" fontColor="blue">
          PRODUCTS
        </Text.H3>
        <Flex className="responsive-column ">
          <BoxContent
            bg={productTypeData.bg1}
            type={productTypeData.type1}
            width="50%"
          />
          <BoxContent
            bg={productTypeData.bg2}
            type={productTypeData.type2}
            width="50%"
          />
        </Flex>
        <Flex className="responsive-column ">
          <BoxContent
            bg={productTypeData.bg3}
            type={productTypeData.type3}
            width="50%"
          />
          <BoxContent
            bg={productTypeData.bg4}
            type={productTypeData.type4}
            width="50%"
          />
        </Flex>
      </Container>
    </Section>
  );
};

const BoxContent = ({ bg, width, type }) => {
  const [hover, setHover] = useState(false);
  return (
    <Flex
      className="responsive-full-width responsive-height250px filter bg-img "
      bg={bg && `${process.env.api_url}` + bg.url}
      flexDirection="column"
      width={width}
      minHeight="300px"
      justifyContent="center"
      alignItems="center"
      cursor="pointer"
      bgFilter={
        hover
          ? "linear-gradient(rgb(230, 49, 51,0.5), rgb(230, 49, 51,0.5))"
          : "linear-gradient(rgb(0, 0, 0, 0.5), rgb(0, 0, 0, 0.5))"
      }
      onMouseOver={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
    >
      <Text.H4
        fontColor="white"
        textAlign="center"
        weight={hover === true ? 500 : 100}
      >
        {type.toUpperCase()}
      </Text.H4>
      {/* {type === "digital displays" ? ( */}
      <Link href="/en/product/[...type]" as={`/en/product/${type}`}>
        <a>
          <Button
            margin="0.5em 0"
            bgcolor="red"
            className={
              hover === true ? "inline visible-show" : "inline visible-hide"
            }
          >
            View now
          </Button>
        </a>
      </Link>
      {/* ) : (
        <Button
          margin="0.5em 0"
          bgcolor="red"
          className={
            hover === true ? "inline visible-show" : "inline visible-hide"
          }
        >
          Not available
        </Button>
      )} */}
    </Flex>
  );
};
