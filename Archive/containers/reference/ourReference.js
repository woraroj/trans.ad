import React, { useEffect } from "react";
import { Section, Flex, Text, DivWrapper } from "components";
import { Container } from "reactstrap";
import { useRouter } from "next/router";
import {
  Link,
  DirectLink,
  Element,
  Events,
  animateScroll as scroll,
  scrollSpy,
  scroller,
} from "react-scroll";
export const OurReference = ({ refData }) => {
  const router = useRouter();
  const duplicateType = [
    ...new Set(refData && refData.map((data, index) => data.refType)),
  ];
  const duplicateSubType = [
    ...new Set(refData && refData.map((data, index) => data.refSubType)),
  ];
  useEffect(() => {
    let id = router.asPath.split("#")[1];
    if (refData && router.asPath.includes("#")) {
      scroller.scrollTo(`${id}`, {
        duration: 500,
        delay: 0,
        smooth: "easeInOutQuart",
        offset: -200,
      });
    }
  });
  return (
    <Section padding="3em 0">
      <Container>
        <Text.H3 weight={500} fontColor="blue" margin="2em 0">
          REFERENCES
        </Text.H3>
        <ReferenceHeader text={duplicateType[0]} />
        <Element name="transportation" />
        <ReferenceComponent refData={refData} data={duplicateSubType[0]} />
        <ReferenceComponent refData={refData} data={duplicateSubType[1]} />
        <ReferenceComponent refData={refData} data={duplicateSubType[6]} />
        <ReferenceHeader text={duplicateType[1]} />
        <Element name="out" />
        <ReferenceComponent refData={refData} data={duplicateSubType[2]} />
        <ReferenceComponent refData={refData} data={duplicateSubType[3]} />
        <ReferenceHeader text={duplicateType[2]} />
        <Element name="office" />
        <ReferenceComponent refData={refData} data={duplicateSubType[4]} />
        <ReferenceComponent refData={refData} data={duplicateSubType[5]} />
        <ReferenceHeader text={duplicateType[3]} />
        <Element name="other" />
        <ReferenceComponent refData={refData} data={duplicateSubType[7]} />
      </Container>
    </Section>
  );
};

const ReferenceComponent = ({ data, refData }) => {
  return (
    <Flex
      className="responsive-align-center "
      flexDirection="column"
      width="100%"
      alignItems="flex-start"
      margin="2em 0"
    >
      <Text.H4 fontColor="blue" margin="1em 0">
        {data}
      </Text.H4>
      <div className="grid1">
        {refData
          .filter((ref) => ref.refSubType === data)
          .map((data, index) => (
            <Flex
              // className="hoverWrap"
              flexDirection="column"
              jus
              width="320px"
              key={index}
            >
              <img
                src={`${process.env.api_url}` + data.refImg.url}
                width="320px"
                height="200px"
              />
              <Flex flexDirection="column" width="100%" padding="10px">
                <Text.H5 margin="0.5em 0" fontColor="blue">
                  {data.refHeader}
                </Text.H5>
                <Text.H6 lineHeight="20px" weight={200}>
                  {data.refText}
                </Text.H6>
              </Flex>
            </Flex>
          ))}
      </div>
    </Flex>
  );
};

const ReferenceHeader = ({ text, id }) => {
  return (
    <Flex flexDirection="column" width="100%" alignItems="flex-start">
      <DivWrapper className="product-header">
        <Text.H4 fontColor="white" weight={400}>
          {text}
        </Text.H4>
      </DivWrapper>
      <DivWrapper className="product-line"></DivWrapper>
    </Flex>
  );
};
