import React, { useState, useEffect } from "react";
import { Section, Flex, Text, DivWrapper, Button } from "components";
import { Container, Collapse } from "reactstrap";
import Link from "next/link";
import styled from "styled-components";
import { useRouter } from "next/router";
import {
  DirectLink,
  Element,
  Events,
  animateScroll as scroll,
  scrollSpy,
  scroller,
} from "react-scroll";
export const Intro = ({ data }) => {
  const [isCollapse, setCollapse] = useState();
  const router = useRouter();
  useEffect(() => {
    let id = router.asPath.split("#")[1];
    if (data && router.asPath.includes("#")) {
      scroller.scrollTo(`${id}`, {
        duration: 500,
        delay: 0,
        smooth: "easeInOutQuart",
        offset: -200,
      });
    }
  });
  return (
    <Section padding="5em 0" id="service">
      <Container>
        <Flex flexDirection="column" alignItems="center">
          <Text.H3 weight={500} fontColor="blue">
            SERVICES
          </Text.H3>

          <Flex className="responsive-column " margin="2em 0 0 0" width="100%">
            <BoxContent
              bg={data && `${process.env.api_url}` + data.serv1Img.url}
              type={data && data.serv1HeaderTh}
              width="33%"
              setCollapse={setCollapse}
              index="1"
            />
            <BoxContent
              bg={data && `${process.env.api_url}` + data.serv2Img.url}
              type={data && data.serv2HeaderTh}
              width="33%"
              setCollapse={setCollapse}
              index="2"
            />
            <BoxContent
              bg={data && `${process.env.api_url}` + data.serv3Img.url}
              type={data && data.serv3HeaderTh}
              width="33%"
              setCollapse={setCollapse}
              index="3"
            />
          </Flex>
          <Flex className="responsive-column " margin="0 0 2em 0" width="100%">
            <BoxContent
              bg={data && `${process.env.api_url}` + data.serv4Img.url}
              type={data && data.serv4HeaderTh}
              width="33%"
              setCollapse={setCollapse}
              index="4"
            />
            <BoxContent
              bg={data && `${process.env.api_url}` + data.serv5Img.url}
              type={data && data.serv5HeaderTh}
              width="33%"
              setCollapse={setCollapse}
              index="5"
            />
            <BoxContent
              bg={data && `${process.env.api_url}` + data.serv6Img.url}
              type={data && data.serv6HeaderTh}
              width="33%"
              setCollapse={setCollapse}
              index="6"
            />
          </Flex>
          {/* /////////////////////////////////////////////////////////////////////// */}
          <DivWrapper margin="3em 0">
            {/* <Collapse isOpen={isCollapse === "1"}> */}
            <Flex className="shadow" margin="2em 0" minHeight="500px">
              <Element name="1" />
              <Flex className="responsive-hide " width="50%">
                <img
                  src={data && `${process.env.api_url}` + data.serv1Img.url}
                  style={{
                    width: "100%",
                    height: "100%",
                    objectFit: "cover",
                  }}
                />
              </Flex>
              <Flex
                className="responsive-full-width "
                padding="2em"
                width="50%"
                flexDirection="column"
                alignItems="flex-start"
              >
                <Text.H3 weight={500} margin="0 0 1em 0" fontColor="blue">
                  {data && data.serv1HeaderTh}
                </Text.H3>
                <Text.H5
                  space="1px"
                  weight={200}
                  margin="0 0 1em 0"
                  fontColor="grey"
                  lineHeight="23px"
                >
                  {data && data.serv1TextTh}
                </Text.H5>
              </Flex>
            </Flex>
            {/* </Collapse> */}
            {/* <Collapse isOpen={isCollapse === "2"}> */}
            <Flex className="shadow" margin="2em 0" id="2" minHeight="500px">
              <Element name="2" />
              <Flex className="responsive-hide " width="50%">
                <img
                  src={data && `${process.env.api_url}` + data.serv2Img.url}
                  style={{
                    width: "100%",
                    height: "100%",
                    objectFit: "cover",
                  }}
                />
              </Flex>
              <Flex
                className="responsive-full-width "
                padding="2em"
                width="50%"
                flexDirection="column"
                alignItems="flex-start"
              >
                <Text.H3 weight={500} margin="0 0 1em 0" fontColor="blue">
                  {data && data.serv2HeaderTh}
                </Text.H3>
                <Text.H5
                  space="1px"
                  weight={200}
                  margin="0 0 1em 0"
                  fontColor="grey"
                  lineHeight="23px"
                >
                  {data && data.serv2TextTh}
                </Text.H5>
              </Flex>
            </Flex>
            {/* </Collapse> */}
            {/* <Collapse isOpen={isCollapse === "3"}> */}
            <Flex className="shadow" margin="2em 0" id="3" minHeight="500px">
              <Element name="3" />
              <Flex className="responsive-hide " width="50%">
                <img
                  src={data && `${process.env.api_url}` + data.serv3Img.url}
                  style={{
                    width: "100%",
                    height: "100%",
                    objectFit: "cover",
                  }}
                />
              </Flex>
              <Flex
                className="responsive-full-width "
                padding="2em"
                width="50%"
                flexDirection="column"
                alignItems="flex-start"
              >
                <Text.H3 weight={500} margin="0 0 1em 0" fontColor="blue">
                  {data && data.serv3HeaderTh}
                </Text.H3>
                <Text.H5
                  space="1px"
                  weight={200}
                  margin="0 0 1em 0"
                  fontColor="grey"
                  lineHeight="23px"
                >
                  {data && data.serv3TextTh}
                </Text.H5>
              </Flex>
            </Flex>
            {/* </Collapse> */}
            {/* <Collapse isOpen={isCollapse === "4"}> */}
            <Flex className="shadow" margin="2em 0" id="4" minHeight="500px">
              <Element name="4" />
              <Flex className="responsive-hide " width="50%">
                <img
                  src={data && `${process.env.api_url}` + data.serv4Img.url}
                  style={{
                    width: "100%",
                    height: "100%",
                    objectFit: "cover",
                  }}
                />
              </Flex>
              <Flex
                className="responsive-full-width "
                padding="2em"
                width="50%"
                flexDirection="column"
                alignItems="flex-start"
              >
                <Text.H3 weight={500} margin="0 0 1em 0" fontColor="blue">
                  {data && data.serv4HeaderTh}
                </Text.H3>
                <Text.H5
                  space="1px"
                  weight={200}
                  margin="0 0 1em 0"
                  fontColor="grey"
                  lineHeight="23px"
                >
                  {data && data.serv4TextTh}
                </Text.H5>
              </Flex>
            </Flex>
            {/* </Collapse> */}
            {/* <Collapse isOpen={isCollapse === "5"}> */}
            <Flex className="shadow" margin="2em 0" id="5" minHeight="500px">
              <Element name="5" />
              <Flex className="responsive-hide " width="50%">
                <img
                  src={data && `${process.env.api_url}` + data.serv5Img.url}
                  style={{
                    width: "100%",
                    height: "100%",
                    objectFit: "cover",
                  }}
                />
              </Flex>
              <Flex
                className="responsive-full-width "
                padding="2em"
                width="50%"
                flexDirection="column"
                alignItems="flex-start"
              >
                <Text.H3 weight={500} margin="0 0 1em 0" fontColor="blue">
                  {data && data.serv5HeaderTh}
                </Text.H3>
                <Text.H5
                  space="1px"
                  weight={200}
                  margin="0 0 1em 0"
                  fontColor="grey"
                  lineHeight="23px"
                >
                  {data && data.serv5TextTh}
                </Text.H5>
              </Flex>
            </Flex>
            {/* </Collapse> */}
            {/* <Collapse isOpen={isCollapse === "6"}> */}
            <Flex className="shadow" margin="2em 0" id="6" minHeight="500px">
              <Element name="6" />
              <Flex className="responsive-hide " width="50%">
                <img
                  src={data && `${process.env.api_url}` + data.serv6Img.url}
                  style={{
                    width: "100%",
                    height: "100%",
                    objectFit: "cover",
                  }}
                />
              </Flex>
              <Flex
                className="responsive-full-width "
                padding="2em"
                width="50%"
                flexDirection="column"
                alignItems="flex-start"
              >
                <Text.H3 weight={500} margin="0 0 1em 0" fontColor="blue">
                  {data && data.serv6HeaderTh}
                </Text.H3>
                <Text.H5
                  space="1px"
                  weight={200}
                  margin="0 0 1em 0"
                  fontColor="grey"
                  lineHeight="23px"
                >
                  {data && data.serv6TextTh}
                </Text.H5>
              </Flex>
            </Flex>
            {/* </Collapse> */}
          </DivWrapper>
        </Flex>
      </Container>
    </Section>
  );
};

const BoxContent = ({ bg, width, type, setCollapse, index }) => {
  const [hover, setHover] = useState(false);
  return (
    <Flex
      className="responsive-full-width responsive-height250px filter bg-img "
      bg={bg}
      flexDirection="column"
      width={width}
      minHeight="300px"
      justifyContent="center"
      alignItems="center"
      cursor="pointer"
      bgFilter={
        hover
          ? "linear-gradient(rgb(230, 49, 51,0.5), rgb(230, 49, 51,0.5))"
          : "linear-gradient(rgb(0, 0, 0, 0.5), rgb(0, 0, 0, 0.5))"
      }
      onMouseOver={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
    >
      <Text.H4
        fontColor="white"
        textAlign="center"
        weight={hover === true ? 500 : 100}
      >
        {type && type.toUpperCase()}
      </Text.H4>
      <Link href={`#${index}`}>
        <a>
          <Button
            // onClick={() => setCollapse(index)}
            margin="0.5em 0"
            bgcolor="red"
            className={
              hover === true ? "inline visible-show" : "inline visible-hide"
            }
          >
            View
          </Button>
        </a>
      </Link>
    </Flex>
  );
};
