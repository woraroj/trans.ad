import React, { useEffect } from "react";
import Head from "next/head";
import { Section } from "components";
import { motion } from "framer-motion";
import useSWR from "swr";
import {
  Landing,
  Certificate,
  History,
  Reference,
  News,
  AboutIntro,
} from "../../../containers/aboutus";

import { useRouter } from "next/router";
import {
  DirectLink,
  Element,
  Events,
  animateScroll as scroll,
  scrollSpy,
  scroller,
} from "react-scroll";
const Aboutus = () => {
  const fetcher = (url) => fetch(url).then((r) => r.json());
  const { data: aboutusData, error: aboutusErr } = useSWR(
    `${process.env.api_url}/aboutus`,
    fetcher
  );
  const { data: introData, error: intorErr } = useSWR(
    `${process.env.api_url}/home`,
    fetcher
  );
  const router = useRouter();
  useEffect(() => {
    let id = router.asPath.split("#")[1];
    if (aboutusData && router.asPath.includes("#")) {
      scroller.scrollTo(`${id}`, {
        duration: 500,
        delay: 0,
        smooth: "easeInOutQuart",
        offset: -200,
      });
    }
  });
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{ padding: "0" }}
    >
      <Section>
        <Head>
          <title>เกี่ยวกับเรา • TRANS.AD SOLUTION</title>
          <link rel="icon" href="/assets/logo.png" />
        </Head>

        <Landing data={aboutusData && aboutusData} />
        <Section bg="/assets/bg.png">
          <AboutIntro
            introImg={introData && introData.introImg}
            introText={introData && introData.introTh}
          />
          <History data={aboutusData && aboutusData.aboutus_overviews} />
          <Certificate />
          <News data={aboutusData && aboutusData.aboutus_news} />
          <Reference />
        </Section>
      </Section>
    </motion.div>
  );
};
export default Aboutus;
