import React, { useEffect, useState } from "react";
import { Section, Flex, Text, Button, SocialBar, DivWrapper } from "components";
import { Container, Spinner } from "reactstrap";
import { motion } from "framer-motion";
import { useRouter } from "next/router";

import Head from "next/head";
import fetch from "isomorphic-unfetch";
import useSWR from "swr";
import Custom404 from "../../404";
import Link from "next/link";
const ServiceType = () => {
  const router = useRouter();
  const [slug, setSlug] = useState();
  const fetcher = (url) => fetch(url).then((r) => r.json());

  const { data, error } = useSWR(
    `${process.env.api_url}/jobs?name=${slug}`,
    fetcher
  );

  useEffect(() => {
    window.scrollTo(0, 0);
  });

  if (Array.isArray(data) && data.length === 0) {
    return <Custom404 />;
  }

  useEffect(() => {
    window.scrollTo(0, 0);
    if (router && router.query.job) {
      const param = router.query.job;
      setSlug(param);
    }
  }, [router]);
  return (
    <motion.div
      initial={{ x: -60, opacity: 0 }}
      animate={{ x: 0, opacity: 1 }}
      exit={{ x: -60, opacity: 0 }}
    >
      <Section width="100%" minHeight="100vh" padding="0 0 5em 0">
        <Head>
          <title>
            {data && data.length > 0
              ? `${data[0].name} • TRANS.AD SOLUTION`
              : "Loading..."}
          </title>
          <link rel="icon" href="/assets/logo.png" />
        </Head>

        <Landing name={slug} />
        {data ? (
          <JobDescription data={data[0]} />
        ) : (
          <Flex
            width="100%"
            height="80vh"
            justifyContent="center"
            alignItems="center"
          >
            <Spinner color="danger" />
          </Flex>
        )}
      </Section>
    </motion.div>
  );
};

const Landing = ({ name }) => {
  return (
    <Section position="relative" width="100%" className="filter">
      <Flex
        className="bg-img contain filter"
        bgFilter="linear-gradient(rgb(0, 0, 0, 0.5), rgb(0, 0, 0, 0.5))"
        bg="/assets/photo-007.jpeg"
        flexDirection="column"
        justifyContent="center"
        alignItems="center"
        height="60vh"
      >
        <Flex
          flexDirection="column"
          justifyContent="center"
          alignItems="center"
          width="100%"
        >
          <Text.H1
            weight={400}
            space="2.5px"
            margin="1em 0 0 0"
            fontColor="white"
          >
            {name}
          </Text.H1>
          <Text.H5
            weight={200}
            space="2.5px"
            margin="0 0 1em 0"
            fontColor="white"
          >
            One Stop Solutions Provider for Communication & Digital Display
            System
          </Text.H5>
        </Flex>

        <DivWrapper className="absolute" bottom="15%">
          <SocialBar />
        </DivWrapper>
      </Flex>
    </Section>
  );
};

const JobDescription = ({ data }) => {
  return (
    <Container>
      <Flex padding="5em 0" flexDirection="column">
        <Flex margin=" 0.25em 0">
          <Text.H3 margin="0 1em 0 0" fontColor="blue" weight={500}>
            {data.name}
          </Text.H3>
          <Button bgcolor="blue">ดาวโหลดใบสมัคร</Button>
        </Flex>
        <Text.H4 fontColor="blue" weight={300}>
          {data.department}
        </Text.H4>
        <Flex wrap="wrap" margin="1.5em 0">
          <Flex flexDirection="column" margin="0 5em 0 0">
            <Flex margin="0.5em 0">
              <Text.H5 fontColor="grey">ประเภทงาน:</Text.H5>
              <Text.H5 fontColor="blue" weight={500} margin="0 0.5em">
                งานประจำ
              </Text.H5>
            </Flex>
            <Flex margin="0.5em 0">
              <Text.H5 fontColor="grey">จำนวน:</Text.H5>
              <Text.H5 fontColor="blue" weight={500} margin="0 0.5em">
                {data.quantity} อัตรา
              </Text.H5>
            </Flex>
            <Flex margin="0.5em 0">
              <Text.H5 fontColor="grey">เพศ:</Text.H5>
              <Text.H5 fontColor="blue" weight={500} margin="0 0.5em">
                {data.gender}
              </Text.H5>
            </Flex>
            <Flex margin="0.5em 0">
              <Text.H5 fontColor="grey">เงินเดือน(บาท):</Text.H5>
              <Text.H5 fontColor="blue" weight={500} margin="0 0.5em">
                {data.wage}
              </Text.H5>
            </Flex>
          </Flex>
          <Flex flexDirection="column">
            <Flex margin="0.5em 0">
              <Text.H5 fontColor="grey">ประสบการณ์:</Text.H5>
              <Text.H5 fontColor="blue" weight={500} margin="0 0.5em">
                {data.exp}
              </Text.H5>
            </Flex>
            <Flex margin="0.5em 0">
              <Text.H5 fontColor="grey">สถานที่:</Text.H5>
              <Text.H5 fontColor="blue" weight={500} margin="0 0.5em">
                {data.location}
              </Text.H5>
            </Flex>
            <Flex margin="0.5em 0">
              <Text.H5 fontColor="grey">การศึกษา:</Text.H5>
              <Text.H5 fontColor="blue" weight={500} margin="0 0.5em">
                {data.education}
              </Text.H5>
            </Flex>
          </Flex>
        </Flex>
        <Flex flexDirection="column">
          {/* responsibility */}
          <Text.H4 margin="0.75em 0" fontColor="blue" weight={500}>
            หน้าที่ความรับผิดชอบ
          </Text.H4>
          <Text.H5 lineHeight="30px" fontColor="grey">
            • Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
            pellentesque nulla vitae elementum iaculis.
          </Text.H5>
          <Text.H5 lineHeight="30px" fontColor="grey">
            • Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
            pellentesque nulla vitae elementum iaculis.
          </Text.H5>
          <Text.H5 lineHeight="30px" fontColor="grey">
            • Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
            pellentesque nulla vitae elementum iaculis.
          </Text.H5>
          <Text.H5 lineHeight="30px" fontColor="grey">
            • Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
            pellentesque nulla vitae elementum iaculis.
          </Text.H5>
          <Text.H5 lineHeight="30px" fontColor="grey">
            • Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
            pellentesque nulla vitae elementum iaculis.
          </Text.H5>
          <Text.H5 lineHeight="30px" fontColor="grey">
            • Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
            pellentesque nulla vitae elementum iaculis.
          </Text.H5>
          {/* requirement */}
          <Text.H4 margin="2em 0 0.75em 0" fontColor="blue" weight={500}>
            คุณสมบัติ
          </Text.H4>
          <Text.H5 lineHeight="30px" fontColor="grey">
            • Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
            pellentesque nulla vitae elementum iaculis.
          </Text.H5>
          <Text.H5 lineHeight="30px" fontColor="grey">
            • Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
            pellentesque nulla vitae elementum iaculis.
          </Text.H5>
          <Text.H5 lineHeight="30px" fontColor="grey">
            • Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
            pellentesque nulla vitae elementum iaculis.
          </Text.H5>
          <Text.H5 lineHeight="30px" fontColor="grey">
            • Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
            pellentesque nulla vitae elementum iaculis.
          </Text.H5>
          <Text.H5 lineHeight="30px" fontColor="grey">
            • Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
            pellentesque nulla vitae elementum iaculis.
          </Text.H5>
          <Text.H5 lineHeight="30px" fontColor="grey">
            • Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec
            pellentesque nulla vitae elementum iaculis.
          </Text.H5>
        </Flex>
      </Flex>
    </Container>
  );
};
export default ServiceType;
