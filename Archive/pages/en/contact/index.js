import React from "react";
import Head from "next/head";
import { Spinner } from "reactstrap";
import { Section, Flex } from "components";
import useSWR from "swr";
import { Landing, Content } from "../../../containers";
import { motion } from "framer-motion";
const Contact = () => {
  const fetcher = (url) => fetch(url).then((r) => r.json());
  const { data: landingData, error: landinfErr } = useSWR(
    `${process.env.api_url}/home`,
    fetcher
  );

  const { data: partyData, error: partyErr } = useSWR(
    `${process.env.api_url}/contact`,
    fetcher
  );

  const { data: JobData, error: JobErr } = useSWR(
    `${process.env.api_url}/jobs`,
    fetcher
  );

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{ padding: "0" }}
    >
      <Section minHeight="80vh">
        <Head>
          <title>ติดต่อเรา • TRANS.AD SOLUTION</title>
          <link rel="icon" href="/assets/logo.png" />
        </Head>
        <Landing bg={landingData && landingData.bannerBg} />
        <Section bg="/assets/bg.png" width="100%">
          {partyData ? (
            <Content data={partyData && partyData} job={JobData} />
          ) : (
            <Flex
              width="100%"
              height="80vh"
              justifyContent="center"
              alignItems="center"
            >
              <Spinner color="danger" />
            </Flex>
          )}
        </Section>
      </Section>
    </motion.div>
  );
};
export default Contact;
