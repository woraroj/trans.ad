import React, { useEffect } from "react";
import Head from "next/head";
import { Section } from "components";
import useSWR from "swr";
import {
  Landing,
  Intro,
  Partner,
  Customer,
  Certified,
} from "../../../containers";
import { motion } from "framer-motion";
import { useRouter } from "next/router";
import {
  DirectLink,
  Element,
  Events,
  animateScroll as scroll,
  scrollSpy,
  scroller,
} from "react-scroll";
const Home = () => {
  const fetcher = (url) => fetch(url).then((r) => r.json());
  const { data, error } = useSWR(`${process.env.api_url}/home`, fetcher);
  const router = useRouter();
  useEffect(() => {
    let id = router.asPath.split("#")[1];
    if (data && router.asPath.includes("#")) {
      scroller.scrollTo(`${id}`, {
        duration: 500,
        delay: 0,
        smooth: "easeInOutQuart",
        offset: -200,
      });
    }
  });
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{ padding: "0" }}
    >
      <Section>
        <Head>
          <title>หน้าแรก • TRANS.AD SOLUTION</title>
          <link rel="icon" href="/assets/logo.png" />
        </Head>
        <Landing bg={data && data.bannerBg} />
        <Section bg="/assets/bg.png">
          <Intro
            introImg={data && data.introImg}
            introText={data && data.introTh}
          />
          <Partner customerImg={data && data.customer} />
        </Section>
      </Section>
    </motion.div>
  );
};
export default Home;
