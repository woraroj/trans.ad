import React, { useState, useEffect } from "react";
import { Section, Flex, Text, DivWrapper, Button } from "components";
import { Container, Spinner } from "reactstrap";
import Head from "next/head";
import { motion } from "framer-motion";
import { useRouter } from "next/router";
import { Landing } from "../../../containers";

import axios from "axios";
import Custom404 from "../../404";
import useSWR from "swr";
import {
  Link,
  DirectLink,
  Element,
  Events,
  animateScroll as scroll,
  scrollSpy,
  scroller,
} from "react-scroll";
import {
  FacebookShareButton,
  TwitterShareButton,
  EmailShareButton,
} from "next-share";

const ProductType = () => {
  const router = useRouter();
  const { type } = router.query;
  const fetcher = (url) => fetch(url).then((r) => r.json());
  const { data: landingData, error: landingError } = useSWR(
    `${process.env.api_url}/home`,
    fetcher
  );
  const { data: typeData, error: typeDataError } = useSWR(
    `${process.env.api_url}/products?type=${type}`,
    fetcher
  );
  // useEffect(() => {});
  if (Array.isArray(typeData) && typeData.length === 0) {
    return <Custom404 />;
  }

  function capitalizeLetter(str) {
    var splitStr = str.toLowerCase().split(" ");
    for (var i = 0; i < splitStr.length; i++) {
      splitStr[i] =
        splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
    }
    return splitStr.join(" ");
  }

  const duplicateElement = [
    ...new Set(typeData && typeData.map((data, index) => data.subType)),
  ];

  const shareFacebook = (id, share) => {
    axios
      .put(`${process.env.api_url}/products/${id}`, {
        shareCountFacebook: share + 1,
      })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  const shareLink = (id, share) => {
    axios
      .put(`${process.env.api_url}/products/${id}`, {
        shareCountLink: share + 1,
      })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  const shareTwit = (id, share) => {
    axios
      .put(`${process.env.api_url}/products/${id}`, {
        shareCountTwit: share + 1,
      })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  const shareEmail = (id, share) => {
    axios
      .put(`${process.env.api_url}/products/${id}`, {
        shareCountEmail: share + 1,
      })
      .then(function (response) {
        console.log(response);
      })
      .catch(function (error) {
        console.log(error);
      });
  };

  useEffect(() => {
    // if (router.asPath.includes("#")) {
    if (typeData) scrollTo();
    // }
  });

  const scrollTo = () => {
    scroller.scrollTo("product", {
      duration: 500,
      delay: 0,
      smooth: "easeInOutQuart",
      offset: -200,
    });
  };

  return (
    <motion.div
      initial={{ x: -60, opacity: 0 }}
      animate={{ x: 0, opacity: 1 }}
      exit={{ x: -60, opacity: 0 }}
    >
      <Section width="100%" minHeight="100vh" id="pro">
        <Head>
          <title>
            {typeData && typeData.length > 0
              ? `${capitalizeLetter(typeData[0].type)} • TRANS.AD SOLUTION`
              : "Loading..."}
          </title>
          <meta
            property="og:url"
            content="	http://transad2.in3.fcomet.com/en/product/"
          />
          <meta property="og:type" content="website" />
          <meta property="fb:app_id" content="315780520323401" />
          <meta property="og:title" content="TRANS.AD SOLUTION" key="title" />
          <meta property="og:description" content="Product" />
          <meta property="og:image" content={"/assets/logo.png"} />
          <link rel="icon" href="/assets/logo.png" />
        </Head>
        {typeData ? (
          <>
            <Landing bg={landingData && landingData.bannerBg} />
            <Container>
              <Section padding="3em 0">
                <Text.H3 margin="0 0 1em 0" fontColor="blue">
                  PRODUCTS
                </Text.H3>
                <Element className="element" name="product"></Element>
                <ReferenceHeader text={type && type.toUpperCase()} />
                {typeData &&
                  duplicateElement.map((data, index) => (
                    <div key={index}>
                      <Text.H4 margin="2em 0" fontColor="blue">
                        {data}
                      </Text.H4>
                      {typeData
                        .filter((sub) => sub.subType === data)
                        .map((product, index) => {
                          if (index % 2 == 0) {
                            return (
                              <Flex
                                className="productWrap responsive-column responsive-full-height "
                                key={index}
                              >
                                <Flex
                                  className="bg-img responsive-full-width contain"
                                  bg={
                                    product &&
                                    `${process.env.api_url}` + product.img.url
                                  }
                                  width="400px"
                                ></Flex>
                                <Flex className="productWrap-content responsive-order-1">
                                  <>
                                    <Text.H4
                                      fontColor="blue"
                                      margin="0 0 0.5em 0"
                                    >
                                      {product && product.header}
                                    </Text.H4>
                                    <Text.H6 lineHeight="25px" fontColor="grey">
                                      {product && product.text}
                                    </Text.H6>
                                  </>

                                  <Flex alignItems="center">
                                    <Flex
                                      margin="0 10px 0 0"
                                      alignItems="center"
                                      onClick={() => (
                                        shareLink(
                                          product.id,
                                          product.shareCountLink
                                            ? product.shareCountLink
                                            : 0
                                        ),
                                        navigator.clipboard.writeText(
                                          "http://transad2.in3.fcomet.com/en/product/"
                                        )
                                      )}
                                    >
                                      <img
                                        src="/assets/link.svg"
                                        width="40px"
                                      />
                                      <Text.H6 margin="0 0.25em">
                                        {product.shareCountLink
                                          ? product.shareCountLink
                                          : 0}
                                      </Text.H6>
                                    </Flex>

                                    <Flex
                                      margin="0 10px 0 0"
                                      alignItems="center"
                                      onClick={() =>
                                        shareFacebook(
                                          product.id,
                                          product.shareCountFacebook
                                            ? product.shareCountFacebook
                                            : 0
                                        )
                                      }
                                    >
                                      <FacebookShareButton
                                        url={
                                          "http://transad2.in3.fcomet.com/en/product/"
                                        }
                                        quote={
                                          "TRANS.AD SOLUTION • PRODUCT •" +
                                          product.header
                                        }
                                        hashtag={`#TRANS_AD_SOLUTION`}
                                      >
                                        <Flex alignItems="center">
                                          {/* <FontAwesomeIcon
                                            icon={faFacebookSquare}
                                            width="25px"
                                            color="rgb(8,119,231)"
                                            // style={{ marginBottom: "-35px" }}
                                          /> */}
                                          <img
                                            src="/assets/facebook.svg"
                                            width="40px"
                                          />
                                          <Text.H6 margin="0 0.25em">
                                            {product.shareCountFacebook
                                              ? product.shareCountFacebook
                                              : 0}
                                          </Text.H6>
                                        </Flex>
                                      </FacebookShareButton>
                                    </Flex>
                                    <Flex
                                      margin="0 10px 0 0"
                                      alignItems="center"
                                      onClick={() =>
                                        shareTwit(
                                          product.id,
                                          product.shareCountTwit
                                            ? product.shareCountTwit
                                            : 0
                                        )
                                      }
                                    >
                                      <TwitterShareButton
                                        url={
                                          "http://transad2.in3.fcomet.com/en/product/"
                                        }
                                        title={
                                          "TRANS.AD SOLUTION • PRODUCT •" +
                                          product.header
                                        }
                                      >
                                        <img
                                          src="/assets/Twis.svg"
                                          width="40px"
                                        />
                                      </TwitterShareButton>
                                      <Text.H6 margin="0 0.25em">
                                        {product.shareCountTwit
                                          ? product.shareCountTwit
                                          : 0}
                                      </Text.H6>
                                    </Flex>
                                    <Flex
                                      margin="0 10px 0 0"
                                      alignItems="center"
                                      onClick={() =>
                                        shareEmail(
                                          product.id,
                                          product.shareCountEmail
                                            ? product.shareCountEmail
                                            : 0
                                        )
                                      }
                                    >
                                      <EmailShareButton
                                        url={"https://github.com/next-share"}
                                        subject={"Next Share"}
                                        body="body"
                                      >
                                        <img
                                          src="/assets/Email.svg"
                                          width="40px"
                                        />
                                      </EmailShareButton>
                                      <Text.H6 margin="0 0.25em">
                                        {product.shareCountEmail
                                          ? product.shareCountEmail
                                          : 0}
                                      </Text.H6>
                                    </Flex>
                                  </Flex>
                                </Flex>
                              </Flex>
                            );
                          } else {
                            return (
                              <Flex
                                className="productWrap responsive-column responsive-full-height "
                                key={index}
                              >
                                <Flex className="productWrap-content responsive-order-1">
                                  <>
                                    <Text.H4 fontColor="blue">
                                      {product && product.header}
                                    </Text.H4>
                                    <Text.H6 lineHeight="25px" fontColor="grey">
                                      {product && product.text}
                                    </Text.H6>
                                  </>
                                  <Flex alignItems="center">
                                    <Flex
                                      margin="0 10px 0 0"
                                      alignItems="center"
                                      onClick={() => (
                                        shareLink(
                                          product.id,
                                          product.shareCountLink
                                            ? product.shareCountLink
                                            : 0
                                        ),
                                        navigator.clipboard.writeText(
                                          "http://transad2.in3.fcomet.com/en/product/"
                                        )
                                      )}
                                    >
                                      <img
                                        src="/assets/link.svg"
                                        width="40px"
                                      />
                                      <Text.H6 margin="0 0.25em">
                                        {product.shareCountLink
                                          ? product.shareCountLink
                                          : 0}
                                      </Text.H6>
                                    </Flex>

                                    <Flex
                                      margin="0 10px 0 0"
                                      alignItems="center"
                                      onClick={() =>
                                        shareFacebook(
                                          product.id,
                                          product.shareCountFacebook
                                            ? product.shareCountFacebook
                                            : 0
                                        )
                                      }
                                    >
                                      <FacebookShareButton
                                        url={
                                          "http://transad2.in3.fcomet.com/en/product/"
                                        }
                                        quote={
                                          "TRANS.AD SOLUTION • PRODUCT •" +
                                          product.header
                                        }
                                        hashtag={`#TRANS_AD_SOLUTION`}
                                      >
                                        <Flex alignItems="center">
                                          {/* <FontAwesomeIcon
                                            icon={faFacebookSquare}
                                            width="25px"
                                            color="rgb(8,119,231)"
                                            // style={{ marginBottom: "-35px" }}
                                          /> */}
                                          <img
                                            src="/assets/facebook.svg"
                                            width="40px"
                                          />
                                          <Text.H6 margin="0 0.25em">
                                            {product.shareCountFacebook
                                              ? product.shareCountFacebook
                                              : 0}
                                          </Text.H6>
                                        </Flex>
                                      </FacebookShareButton>
                                    </Flex>
                                    <Flex
                                      margin="0 10px 0 0"
                                      alignItems="center"
                                      onClick={() =>
                                        shareTwit(
                                          product.id,
                                          product.shareCountTwit
                                            ? product.shareCountTwit
                                            : 0
                                        )
                                      }
                                    >
                                      <TwitterShareButton
                                        url={
                                          "http://transad2.in3.fcomet.com/en/product/"
                                        }
                                        title={
                                          "TRANS.AD SOLUTION • PRODUCT •" +
                                          product.header
                                        }
                                      >
                                        <img
                                          src="/assets/Twis.svg"
                                          width="40px"
                                        />
                                      </TwitterShareButton>
                                      <Text.H6 margin="0 0.25em">
                                        {product.shareCountTwit
                                          ? product.shareCountTwit
                                          : 0}
                                      </Text.H6>
                                    </Flex>
                                    <Flex
                                      margin="0 10px 0 0"
                                      alignItems="center"
                                      onClick={() =>
                                        shareEmail(
                                          product.id,
                                          product.shareCountEmail
                                            ? product.shareCountEmail
                                            : 0
                                        )
                                      }
                                    >
                                      <EmailShareButton
                                        url={
                                          "http://transad2.in3.fcomet.com/en/product/"
                                        }
                                        subject={
                                          "TRANS.AD SOLUTION • PRODUCT •" +
                                          product.header
                                        }
                                        body={product.text}
                                      >
                                        <img
                                          src="/assets/Email.svg"
                                          width="40px"
                                        />
                                      </EmailShareButton>
                                      <Text.H6 margin="0 0.25em">
                                        {product.shareCountEmail
                                          ? product.shareCountEmail
                                          : 0}
                                      </Text.H6>
                                    </Flex>
                                  </Flex>
                                </Flex>
                                <Flex
                                  className="bg-img responsive-full-width contain"
                                  bg={
                                    product &&
                                    `${process.env.api_url}` + product.img.url
                                  }
                                  width="400px"
                                ></Flex>
                              </Flex>
                            );
                          }
                        })}
                    </div>
                  ))}
              </Section>
            </Container>
          </>
        ) : (
          <Flex height="80vh" justifyContent="center" alignItems="center">
            <Spinner color="danger" />
          </Flex>
        )}
      </Section>
    </motion.div>
  );
};
const ReferenceHeader = ({ text }) => {
  return (
    <Flex flexDirection="column" width="100%" alignItems="flex-start">
      <DivWrapper className="product-header">
        <Text.H4 fontColor="white" weight={400}>
          {text}
        </Text.H4>
      </DivWrapper>
      <DivWrapper className="product-line"></DivWrapper>
    </Flex>
  );
};

export default ProductType;
