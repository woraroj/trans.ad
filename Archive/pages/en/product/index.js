import React from "react";
import Head from "next/head";
import { Section } from "components";
import useSWR from "swr";
import { Landing, OurProduct } from "../../../containers";
import { motion } from "framer-motion";
const Product = () => {
  const fetcher = (url) => fetch(url).then((r) => r.json());
  const { data: homeData, error: homeError } = useSWR(
    `${process.env.api_url}/home`,
    fetcher
  );
  const { data: productTypeData, error: productError } = useSWR(
    `${process.env.api_url}/product-type`,
    fetcher
  );

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{ padding: "0" }}
    >
      <Section minHeight="80vh">
        <Head>
          <title>สินค้า • TRANS.AD SOLUTION</title>
          <link rel="icon" href="/assets/logo.png" />
        </Head>
        <Landing bg={homeData && homeData.bannerBg} />
        <Section bg="/assets/bg.png" width="100%">
          {productTypeData && <OurProduct productTypeData={productTypeData} />}
        </Section>
      </Section>
    </motion.div>
  );
};
export default Product;
