import React from "react";
import Head from "next/head";
import { Section, Flex } from "components";
import useSWR from "swr";
import { Landing, OurReference } from "../../../containers";
import { Spinner } from "reactstrap";
import { motion } from "framer-motion";
const Reference = () => {
  const fetcher = (url) => fetch(url).then((r) => r.json());
  const { data: landingData, error: landingError } = useSWR(
    `${process.env.api_url}/home`,
    fetcher
  );
  const { data: refData, error: refDataError } = useSWR(
    `${process.env.api_url}/references`,
    fetcher
  );

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{ padding: "0" }}
    >
      <Section minHeight="80vh">
        <Head>
          <title>ตัวอย่างงาน • TRANS.AD SOLUTION</title>
          <link rel="icon" href="/assets/logo.png" />
        </Head>
        {landingData ? (
          <Landing bg={landingData && landingData.bannerBg} />
        ) : (
          <Flex height="80vh" justifyContent="center" alignItems="center">
            <Spinner color="danger" />
          </Flex>
        )}
        <Section bg="/assets/bg.png" width="100%">
          {refData ? (
            <OurReference refData={refData} />
          ) : (
            <Flex height="80vh" justifyContent="center" alignItems="center">
              <Spinner color="danger" />
            </Flex>
          )}
        </Section>
      </Section>
    </motion.div>
  );
};
export default Reference;
