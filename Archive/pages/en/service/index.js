import React from "react";
import Head from "next/head";
import { Section } from "components";
import { motion } from "framer-motion";
import { Landing, Intro } from "../../../containers/service";
import useSWR from "swr";
const Service = () => {
  const fetcher = (url) => fetch(url).then((r) => r.json());
  const { data: serviceData, error: serviceError } = useSWR(
    `${process.env.api_url}/service`,
    fetcher
  );
  const { data: aboutusData, error: aboutusError } = useSWR(
    `${process.env.api_url}/aboutus`,
    fetcher
  );
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{ padding: "0" }}
    >
      <Section>
        <Head>
          <title>การบริการ • TRANS.AD SOLUTION</title>
          <link rel="icon" href="/assets/logo.png" />
        </Head>

        <Landing data={aboutusData && aboutusData} />
        <Section bg="/assets/bg.png">
          <Intro data={serviceData && serviceData} />
        </Section>
      </Section>
    </motion.div>
  );
};
export default Service;
