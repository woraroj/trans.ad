export default {
  white: "#FFFFFF",
  grey: "#414042",
  bgGrey: "#e2e2e2",
  blue: "#3760AA",
  red: "#f90023",
  xRed: "rgb(230, 49, 51,0.5)",
};
