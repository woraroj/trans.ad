import { Button } from "reactstrap";
import styled from "styled-components";

export const ButtonStyle = styled(Button)`
  background-color: ${(props) => props.theme.color[props.bgcolor]};
  border: 2px solid ${(props) => props.theme.color[props.bgcolor]};
  border-radius: 20px;
  font-weight: 200;
  padding: 0.3em 1.5em;
  margin: ${(props) => props.margin};
  &.mute {
    background-color: rgb(255, 255, 255, 0.5);
    border: 0;
    color: black;
  }
  &.inline {
    background-color: rgb(0, 0, 0, 0);
    border: 2px solid ${(props) => props.theme.color[props.bgcolor]};
    color: ${(props) => props.theme.color[props.bgcolor]};
  }
  &.share-btn {
    display: flex;
    border-radius: 10px;
    background-color: ${(props) => props.theme.color.blue};
    color: white;
  }
  &.footer {
    border-radius: 2px;
    height: 35px;
    display: flex;
    align-items: center;
    padding: 0.3em;
  }
  &.visible-show {
    opacity: 1;
    visibility: visible;
    transition: visibility 0.3s linear, opacity 0.3s linear, margin-bottom 0.3s;
  }
  &.visible-hide {
    opacity: 0;
    visibility: hidden;
    transition: visibility 0.3s linear, opacity 0.3s linear, margin-bottom 0.3s;
    margin-bottom: -45px;
  }
  &.contact-btn {
    background: linear-gradient(
      to left,
      rgb(230, 230, 230),
      rgb(255, 255, 255),
      rgb(230, 230, 230)
    );
    border: 2px solid rgb(0, 0, 0, 0);
    color: ${(props) => props.theme.color.blue};
    border-radius: 10px;
    margin: 2em;
    font-weight: 400;
  }
  :active {
    outline: none;
    border: none;
  }
  :focus {
    outline: 0;
  }
  :hover {
    background-color: rgb(0, 0, 0, 0);
    color: ${(props) => props.theme.color[props.bgcolor]};
    border: 2px solid ${(props) => props.theme.color[props.bgcolor]};
    font-weight: 500;
    &.share-btn {
      background-color: rgb(0, 0, 0, 0);
      border: 2px solid ${(props) => props.theme.color.blue};
      color: ${(props) => props.theme.color.blue};
    }
    &.contact-btn {
      background: ${(props) => props.theme.color.red};
      border: 2px solid ${(props) => props.theme.color.red};
      color: ${(props) => props.theme.color.white};
      border-radius: 10px;
      margin: 2em;
    }
    &.inline {
      background-color: ${(props) => props.theme.color[props.bgcolor]};
      border: 2px solid ${(props) => props.theme.color[props.bgcolor]};
      color: rgb(255, 255, 255);
    }
    &.footer {
      background-color: ${(props) => props.theme.color.blue};
      color: ${(props) => props.theme.color.blue};
    }
  }
`;
