import React from "react";
import { FlexStyle } from "./style";

export const Flex = (props) => {
  const {
    children,
    className,
    flexDirection,
    justifyContent,
    alignItems,
    margin,
    padding,
    width,
    maxWidth,
    height,
    minHeight,
    border,
    wrap,
    bgcolor,
    bg,
    position,
    index,
    onClick,
    onMouseOver,
    onMouseLeave,
    cursor,
    flex,
    bgFilter,
    color,
    content,
  } = props;
  return (
    <FlexStyle
      className={className}
      flexDirection={flexDirection}
      justifyContent={justifyContent}
      alignItems={alignItems}
      margin={margin}
      padding={padding}
      width={width}
      maxWidth={maxWidth}
      border={border}
      height={height}
      minHeight={minHeight}
      wrap={wrap}
      bgcolor={bgcolor}
      bg={bg}
      position={position}
      index={index}
      onClick={onClick}
      onMouseOver={onMouseOver}
      onMouseLeave={onMouseLeave}
      cursor={cursor}
      flex={flex}
      bgFilter={bgFilter}
      color={color}
      content={content}
    >
      {children}
    </FlexStyle>
  );
};
