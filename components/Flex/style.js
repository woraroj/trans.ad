import styled from "styled-components";

export const FlexStyle = styled.div`
  position: ${(props) => props.position};
  display: flex;
  flex-direction: ${(props) => props.flexDirection};
  justify-content: ${(props) => props.justifyContent};
  align-items: ${(props) => props.alignItems};
  margin: ${(props) => props.margin};
  padding: ${(props) => props.padding};
  width: ${(props) => props.width};
  border: ${(props) => props.border};
  max-width: ${(props) => props.maxWidth};
  height: ${(props) => props.height};
  min-height: ${(props) => props.minHeight};
  flex-wrap: ${(props) => props.wrap};
  flex: ${(props) => props.flex};
  background: ${(props) => props.theme.color[props.bgcolor]};
  z-index: ${(props) => props.index};
  cursor: ${(props) => props.cursor};
  transition-duration: 0.25s;
  &.bg-img {
    background-image: url(${(props) => props.bg});
    background-position: top;
    background-repeat: no-repeat;
    background-size: cover;
  }
  &.contain {
    background-size: contain;
    background-position: center;
    background-color: white;
  }
  &.filter {
    background-image: ${(props) => props.bgFilter}, url(${(props) => props.bg});
  }
  &.about-ref-wrapper {
    width: 300px;
    height: 300px;
    margin: 0 0.5em;
    cursor: pointer;
  }
  &.productWrap {
    width: 100%;
    height: 250px;
    margin: 1em 0;
    background: rgb(237, 238, 240);
    border: 1px solid rgb(237, 238, 240);
    overflow: hidden;

    cursor: pointer;
  }
  &.productWrap-content {
    width: 100%;
    border: 1px 0 1px 1px solid #e2e2e2;
    padding: 25px 50px;
    flex-direction: column;

    align-items: flex-start;
    background: white;
    overflow: hidden;
  }
  &.hide {
    display: none;
  }
  &.shadow {
    -webkit-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.75);
    -moz-box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.75);
    box-shadow: 0px 0px 10px 0px rgba(0, 0, 0, 0.75);
  }
  &.flex2 {
    flex: 1;
  }
  &.hexagon {
    position: relative;
    width: 200px;
    height: 115.47px;
    background-color: ${(props) => props.color};
    margin: 57.74px 0.5px;
    border-left: solid nullpx ${(props) => props.color};
    border-right: solid nullpx ${(props) => props.color};
  }

  &.hexagon:before,
  &.hexagon:after {
    content: "";
    position: absolute;
    z-index: 1;
    width: 141.42px;
    height: 141.42px;
    -webkit-transform: scaleY(0.5774) rotate(-45deg);
    -ms-transform: scaleY(0.5774) rotate(-45deg);
    transform: scaleY(0.5774) rotate(-45deg);
    background-color: inherit;
    left: 29.2893px;
  }

  &.hexagon:before {
    top: -70.7107px;
    border-top: solid 0px ${(props) => props.color};
    border-right: solid 0px ${(props) => props.color};
  }

  &.hexagon:after {
    bottom: -70.7107px;
    border-bottom: solid 0px ${(props) => props.color};
    border-left: solid 0px ${(props) => props.color};
  }

  &.hexagon-img {
    position: relative;
    width: 200px;
    height: 115.47px;
    margin: 57.74px 2px;
    background-image: linear-gradient(
      to bottom,
      rgb(236, 237, 239) 10%,
      #ffff 80%,
      rgb(236, 237, 239)
    );
    background-size: auto 230.9401px;
    background-position: center;
    border-left: solid nullpx #0101da;
    border-right: solid nullpx #0101da;
    transition-duration: 0.25s;
  }

  &.hexTop-img,
  &.hexBottom-img {
    position: absolute;
    z-index: 1;
    width: 141.42px;
    height: 141.42px;
    overflow: hidden;
    -webkit-transform: scaleY(0.5774) rotate(-45deg);
    -ms-transform: scaleY(0.5774) rotate(-45deg);
    transform: scaleY(0.5774) rotate(-45deg);
    background: inherit;
    left: 29.29px;
  }

  /*counter transform the bg image on the caps*/
  &.hexTop-img:after,
  &.hexBottom-img:after {
    content: "";
    position: absolute;
    width: 200px;
    height: 115.47005383792516px;
    -webkit-transform: rotate(45deg) scaleY(1.7321) translateY(-57.735px);
    -ms-transform: rotate(45deg) scaleY(1.7321) translateY(-57.735px);
    transform: rotate(45deg) scaleY(1.7321) translateY(-57.735px);
    -webkit-transform-origin: 0 0;
    -ms-transform-origin: 0 0;
    transform-origin: 0 0;
    background: inherit;
  }

  &.hexTop-img {
    top: -70.7107px;
    border-top: solid 0px #0101da;
    border-right: solid 0px #0101da;
  }

  &.hexTop-img:after {
    background-position: center top;
  }

  &.hexBottom-img {
    bottom: -70.7107px;
    border-bottom: solid 0px #0101da;
    border-left: solid 0px #0101da;
  }

  &.hexBottom-img:after {
    background-position: center bottom;
  }

  &.hexagon-img:after {
    content: "";
    position: absolute;
    top: 0px;
    left: 0;
    width: 200px;
    height: 115.4701px;
    z-index: 2;
    background: inherit;
  }
  ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  &.hexagon-img2 {
    position: relative;
    width: 200px;
    height: 115.47px;
    margin: 57.74px 2px;
    background-color: ${(props) => props.bgcolor};

    background-size: auto 230.9401px;
    background-position: center;
    border-left: solid nullpx #0101da;
    border-right: solid nullpx #0101da;
    transition-duration: 0.25s;
  }

  &.hexTop-img2,
  &.hexBottom-img2 {
    position: absolute;
    z-index: 1;
    width: 141.42px;
    height: 141.42px;
    overflow: hidden;
    -webkit-transform: scaleY(0.5774) rotate(-45deg);
    -ms-transform: scaleY(0.5774) rotate(-45deg);
    transform: scaleY(0.5774) rotate(-45deg);
    background: inherit;
    left: 29.29px;
  }

  /*counter transform the bg image on the caps*/
  &.hexTop-img2:after,
  &.hexBottom-img2:after {
    content: "";
    position: absolute;
    width: 200px;
    height: 115.47005383792516px;
    -webkit-transform: rotate(45deg) scaleY(1.7321) translateY(-57.735px);
    -ms-transform: rotate(45deg) scaleY(1.7321) translateY(-57.735px);
    transform: rotate(45deg) scaleY(1.7321) translateY(-57.735px);
    -webkit-transform-origin: 0 0;
    -ms-transform-origin: 0 0;
    transform-origin: 0 0;
    background: inherit;
  }

  &.hexTop-img2 {
    top: -70.7107px;
    border-top: solid 0px #0101da;
    border-right: solid 0px #0101da;
  }

  &.hexTop-img2:after {
    background-position: center top;
  }

  &.hexBottom-img2 {
    bottom: -70.7107px;
    border-bottom: solid 0px #0101da;
    border-left: solid 0px #0101da;
  }

  &.hexBottom-img2:after {
    background-position: center bottom;
  }

  &.hexagon-img2:after {
    content: "";
    position: absolute;
    top: 0px;
    left: 0;
    width: 200px;
    height: 115.4701px;
    z-index: 2;
    background: inherit;
  }

  :hover {
    &.hexagon-img {
      /* filter: grayscale(100%) brightness(40%) sepia(100%) hue-rotate(-50deg)
        saturate(600%) contrast(0.8); */
      background-image: linear-gradient(
        to bottom,
        rgb(250, 79, 90) 10%,
        rgb(250, 79, 90) 80%,
        rgb(250, 79, 90)
      );
    }

    &.productWrap {
      transform: scale(1.025);
      -webkit-box-shadow: 10px 10px 16px -10px rgba(0, 0, 0, 0.75);
      -moz-box-shadow: 10px 10px 16px -10px rgba(0, 0, 0, 0.75);
      box-shadow: 10px 10px 16px -10px rgba(0, 0, 0, 0.75);
    }
    &.hover-red {
      filter: grayscale(100%) brightness(40%) sepia(100%) hue-rotate(-50deg)
        saturate(600%) contrast(0.8);
    }
    &.hoverWrap {
      transform: scale(1.025);
      -webkit-box-shadow: 10px 10px 16px -10px rgba(0, 0, 0, 0.75);
      -moz-box-shadow: 10px 10px 16px -10px rgba(0, 0, 0, 0.75);
      box-shadow: 10px 10px 16px -10px rgba(0, 0, 0, 0.75);
    }
  }
  @media only screen and (max-width: 768px) {
    &.responsive-hide {
      display: none;
    }
    &.responsive-full-width {
      width: 100%;
    }
    &.responsive-full-height {
      height: 100%;
    }
    &.responsive-half-width {
      width: 50%;
    }
    &.responsive-justify-center {
      justify-content: center;
    }
    &.responsive-align-center {
      align-items: center;
    }
    &.responsive-column {
      flex-direction: column;
    }
    &.responsive-order-0 {
      order: 0;
    }
    &.responsive-order-1 {
      order: 1;
    }
    &.responsive-height250px {
      height: 250px;
    }
    &.responsive-margin0 {
      margin: 0;
    }
    .hexagon {
      position: relative;
      width: 80px;
      height: 46.19px;
      padding: 2em 0;
      background-color: ${(props) => props.color};
    }

    .hexagon:before,
    .hexagon:after {
      content: "";
      position: absolute;
      width: 0;
      border-left: 42.5px solid transparent;
      border-right: 42.5px solid transparent;
    }

    .hexagon:before {
      bottom: 100%;
      border-bottom: 24.54px solid ${(props) => props.color};
    }

    .hexagon:after {
      top: 100%;
      width: 0;
      border-top: 24.54px solid ${(props) => props.color};
    }

    &.hexagon2 {
      margin: 0;
    }
    //////////////////////////////////////////////////////////////////////////////////////////////
    &.hexagon-img {
      position: relative;
      width: 100px;
      height: 57.74px;
      margin: 28.87px 0;
      background-image: linear-gradient(
        to bottom,
        rgb(236, 237, 239) 10%,
        #ffff 80%,
        rgb(236, 237, 239)
      );
      background-size: auto 92.376px;
      background-position: center;
    }

    &.hexTop-img,
    &.hexBottom-img {
      position: absolute;
      z-index: 1;
      width: 70.71px;
      height: 70.71px;
      overflow: hidden;
      -webkit-transform: scaleY(0.5774) rotate(-45deg);
      -ms-transform: scaleY(0.5774) rotate(-45deg);
      transform: scaleY(0.5774) rotate(-45deg);
      background: inherit;
      left: 14.64px;
    }

    /*counter transform the bg image on the caps*/
    &.hexTop-img:after,
    &.hexBottom-img:after {
      content: "";
      position: absolute;
      width: 100px;
      height: 57.73502691896258px;
      -webkit-transform: rotate(45deg) scaleY(1.7321) translateY(-23.094px);
      -ms-transform: rotate(45deg) scaleY(1.7321) translateY(-23.094px);
      transform: rotate(45deg) scaleY(1.7321) translateY(-23.094px);
      -webkit-transform-origin: 0 0;
      -ms-transform-origin: 0 0;
      transform-origin: 0 0;
      background: inherit;
    }

    &.hexTop-img {
      top: -35.3553px;
    }

    &.hexTop-img:after {
      background-position: center top;
    }

    &.hexBottom-img {
      bottom: -35.3553px;
    }

    &.hexBottom-img:after {
      background-position: center bottom;
    }

    &.hexagon-img:after {
      content: "";
      position: absolute;
      top: 0px;
      left: 0;
      width: 100px;
      height: 57.735px;
      z-index: 2;
      background: inherit;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    &.hexagon-img2 {
      position: relative;
      width: 100px;
      height: 57.74px;
      margin: 28.87px 0;
      background-color: ${(props) => props.bgcolor};
      background-size: auto 92.376px;
      background-position: center;
    }

    &.hexTop-img2,
    &.hexBottom-img2 {
      position: absolute;
      z-index: 1;
      width: 70.71px;
      height: 70.71px;
      overflow: hidden;
      -webkit-transform: scaleY(0.5774) rotate(-45deg);
      -ms-transform: scaleY(0.5774) rotate(-45deg);
      transform: scaleY(0.5774) rotate(-45deg);
      background: inherit;
      left: 14.64px;
    }

    /*counter transform the bg image on the caps*/
    &.hexTop-img2:after,
    &.hexBottom-img2:after {
      content: "";
      position: absolute;
      width: 100px;
      height: 57.73502691896258px;
      -webkit-transform: rotate(45deg) scaleY(1.7321) translateY(-23.094px);
      -ms-transform: rotate(45deg) scaleY(1.7321) translateY(-23.094px);
      transform: rotate(45deg) scaleY(1.7321) translateY(-23.094px);
      -webkit-transform-origin: 0 0;
      -ms-transform-origin: 0 0;
      transform-origin: 0 0;
      background: inherit;
    }

    &.hexTop-img2 {
      top: -35.3553px;
    }

    &.hexTop-img2:after {
      background-position: center top;
    }

    &.hexBottom-img2 {
      bottom: -35.3553px;
    }

    &.hexBottom-img2:after {
      background-position: center bottom;
    }

    &.hexagon-img2:after {
      content: "";
      position: absolute;
      top: 0px;
      left: 0;
      width: 100px;
      height: 57.735px;
      z-index: 2;
      background: inherit;
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    &.hexagon-img2-wrap {
      margin: -27.74px 0 0 0;
    }
    &.hexagon-img-wrap {
      margin: -27.74px 0 0 0;
    }
  }
`;
