import React from "react";
import { Section, Flex, Text, EmailForm } from "components";
import { Container } from "reactstrap";
import { LogoWrapper, BlueBar } from "./style";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFacebookF, faTwitter } from "@fortawesome/free-brands-svg-icons";
import {
  faPhoneAlt,
  faEnvelope,
  faMapMarkerAlt,
} from "@fortawesome/free-solid-svg-icons";
export const Footer = () => {
  const handleClick = (url) => {
    window.open(url, "_blank");
  };
  return (
    <Section>
      <BlueBar />
      <Container>
        <Flex
          width="100%"
          justifyContent="center"
          alignItems="flex-start"
          className="responsive-column responsive-align-center "
        >
          <Flex
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
            margin="1.5em 0"
          >
            <LogoWrapper className="large">
              <FontAwesomeIcon icon={faPhoneAlt} style={{ width: "25px" }} />
            </LogoWrapper>
            <Text.H5 weight={400} margin="1em 0 0 0">
              +66(2) 001 9900-2
            </Text.H5>
          </Flex>
          <Flex
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
            margin="1.5em 3.5em"
          >
            <LogoWrapper className="large">
              <FontAwesomeIcon icon={faEnvelope} style={{ width: "25px" }} />
            </LogoWrapper>
            <Text.H5 weight={400} margin="1em 0 0 0">
              info@transad.co.th
            </Text.H5>
          </Flex>
          <Flex
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
            margin="1.5em 3.5em"
            // className="responsive-margin0 "
          >
            <LogoWrapper className="large">
              <FontAwesomeIcon
                icon={faMapMarkerAlt}
                style={{ width: "25px" }}
              />
            </LogoWrapper>
            <Text.H5 weight={400} margin="1em 0 0 0">
              Trans.Ad Solutions Co.,Ltd.
            </Text.H5>
            <Text.H5 textAlign="center" weight={200}>
              21, TST Tower, 21/F, Vibhavadi-Rangsit Road,
              <br />
              Chomphon, Chatuchak, Bangkok 10900
            </Text.H5>
          </Flex>
          <Flex
            flexDirection="column"
            justifyContent="center"
            alignItems="flex-start"
            className="flex2"
            margin="1.5em 0"
            padding="50px 0 0 0"
          >
            <Text.H5 weight={400} margin="1em 0 0 0">
              Subscribe to our news update
            </Text.H5>
            <Text.H5 textAlign="center" weight={200}>
              send directly to your email.
            </Text.H5>
            <EmailForm />
          </Flex>
        </Flex>
      </Container>

      <Flex bgcolor="bgGrey" padding="2em 0">
        <Container>
          <Flex flexDirection="column" alignItems="center">
            <Flex margin="1em 0">
              <LogoWrapper
                onClick={() =>
                  handleClick("https://www.facebook.com/TransAdSolutions")
                }
              >
                <FontAwesomeIcon icon={faFacebookF} style={{ width: "15px" }} />
              </LogoWrapper>
              <LogoWrapper
                onClick={() =>
                  handleClick("https://twitter.com/TransAdSolution")
                }
              >
                <FontAwesomeIcon icon={faTwitter} style={{ width: "20px" }} />
              </LogoWrapper>
            </Flex>
            <Text.H5 weight={300}>
              Copyright©2021, Trans.Ad Solutions Co.,Ltd. All rights reserved.
            </Text.H5>
          </Flex>
        </Container>
      </Flex>
    </Section>
  );
};
