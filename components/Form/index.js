import React, { useState, useRef } from "react";
import { Formik, ErrorMessage } from "formik";
import * as Yup from "yup";
import { Spinner, Alert } from "reactstrap";
import { TextField, FormikForm, Checkbox } from "./style";
import { DivWrapper, Flex, Button, Text } from "components";
import axios from "axios";
import { faCaretRight } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
export const ContactForm = () => {
  const schema = Yup.object().shape({
    name: Yup.string().required("Please enter your name"),
    telephone: Yup.string().required("please enter your telephone number"),
    email: Yup.string().email().required("Please input your email"),
    message: Yup.string(),
    file: Yup.mixed()
      .required("A file is required")
      .test(
        "fileSize",
        "File too large",
        (value) => value && value.size <= FILE_SIZE
      )
      .test(
        "fileFormat",
        "Unsupported Format",
        (value) => value && SUPPORTED_FORMATS.includes(value.type)
      ),
  });
  return (
    <Formik
      initialValues={{
        name: "",
        telephone: "",
        email: "",
        message: "",
        agree: false,
      }}
      validationSchema={schema}
      onSubmit={(e, actions) => (
        setState((prevState) => ({ ...prevState, loading: true })),
        handleSubmit(e)
      )}
    >
      {({
        touched,
        errors,
        values,
        handleChange,
        setFieldTouched,
        setFieldValue,
      }) => (
        <FormikForm>
          <Text.H2 fontColor="blue" weight={500}>
            CONTACT US
          </Text.H2>
          <Text.H4 margin="0 0 3em 0" fontColor="grey">
            Contact Trans.Ad Solutions Co., Ltd.
          </Text.H4>
          <Flex margin="15px 0" justifyContent="space-between">
            <DivWrapper width="47.5%">
              <TextField
                type="text"
                name="name"
                value={values["name"]}
                placeholder="First- Last name"
                //   onChange={event =>
                //     validation(event, setFieldValue, setFieldTouched)
                //   }
                className={`FormikForm-control ${
                  touched.name && errors.name && "is-invalid"
                }`}
              />
              <ErrorMessage
                component="div"
                name="name"
                className="invalid-feedback"
              />
            </DivWrapper>
            <DivWrapper width="47.5%">
              <TextField
                type="text"
                name="company"
                value={values["company"]}
                placeholder="Company"
                //   onChange={event =>
                //     validation(event, setFieldValue, setFieldTouched)
                //   }
                className={`FormikForm-control ${
                  touched.company && errors.company && "is-invalid"
                }`}
              />
              <ErrorMessage
                component="div"
                name="email"
                className="invalid-feedback"
              />
            </DivWrapper>
          </Flex>
          <Flex margin="15px 0" justifyContent="space-between">
            <DivWrapper width="47.5%">
              <TextField
                type="text"
                name="telephone"
                maxLength="10"
                value={values["telephone"]}
                placeholder="Phone number"
                onChange={(event) => {
                  validation(event, setFieldValue, setFieldTouched);
                }}
                className={`FormikForm-control ${
                  touched.telephone && errors.telephone && "is-invalid"
                }`}
              />
              <ErrorMessage
                component="div"
                name="telephone"
                className="invalid-feedback"
              />
            </DivWrapper>
            <DivWrapper width="47.5%">
              <TextField
                type="text"
                name="email"
                value={values["email"]}
                placeholder="E-mail"
                //   onChange={event =>
                //     validation(event, setFieldValue, setFieldTouched)
                //   }
                className={`FormikForm-control ${
                  touched.email && errors.email && "is-invalid"
                }`}
              />
              <ErrorMessage
                component="div"
                name="email"
                className="invalid-feedback"
              />
            </DivWrapper>
          </Flex>
          <DivWrapper width="100%" margin="0 0 15px 0">
            <TextField
              component="textarea"
              name="message"
              value={values["message"]}
              placeholder="Message"
              maxLength="500"
              rows="6"
              //   onChange={event =>
              //     validation(event, setFieldValue, setFieldTouched)
              //   }
              className={`FormikForm-control ${
                touched.message && errors.message && "is-invalid"
              }`}
            />
            <ErrorMessage
              component="div"
              name="message"
              className="invalid-feedback"
            />
          </DivWrapper>
          {/* <DivWrapper margin="0 0 15px 0" width="100%">
            <Flex alignItems="center" width="100%">
              <Checkbox
                type="checkbox"
                name="agree"
                className={`FormikForm-control ${
                  touched.agree && errors.agree && "is-invalid"
                }`}
              />
              <Text.H6 fontColor="white" margin="0 0 0 1em" weight={700}>
                AGREE with Privacy Policy
              </Text.H6>
              <ErrorMessage
                component="div"
                name="agree"
                className="invalid-feedback"
              />
            </Flex>
          </DivWrapper> */}
          <Flex justifyContent="flex-start">
            {/* {state.loading ? (
              <Spinner color="primary" />
            ) : ( */}
            <Button bgcolor="red" type="submit">
              SUBMIT
            </Button>
            {/* )} */}
          </Flex>
        </FormikForm>
      )}
    </Formik>
  );
};

export const EmailForm = () => {
  return (
    <Formik
      initialValues={{
        email: "",
      }}
      onSubmit={(e, actions) => (
        setState((prevState) => ({ ...prevState, loading: true })),
        handleSubmit(e)
      )}
    >
      {({
        touched,
        errors,
        values,
        handleChange,
        setFieldTouched,
        setFieldValue,
      }) => (
        <FormikForm>
          {/* {state.failed ? (
            <Alert color="danger" isOpen={state.visible} toggle={onDismiss}>
              Sorry! something went wrong, please try again later.
            </Alert>
          ) : (
            <Alert color="success" isOpen={state.visible} toggle={onDismiss}>
              Thnak you! we will get back to you as soon as possible.
            </Alert>
          )} */}

          <Flex margin="15px 0">
            <DivWrapper width="80%%">
              <TextField
                type="text"
                name="email"
                value={values["email"]}
                placeholder="Insert your e-mail"
                //   onChange={event =>
                //     validation(event, setFieldValue, setFieldTouched)
                //   }
                className={`FormikForm-control ${
                  touched.email && errors.email && "is-invalid"
                }`}
              />{" "}
              <ErrorMessage
                component="div"
                name="email"
                className="invalid-feedback"
              />
            </DivWrapper>
            <Button className="footer" bgcolor="blue" type="submit">
              <FontAwesomeIcon icon={faCaretRight} color="white" width="10px" />
            </Button>
          </Flex>

          {/* <DivWrapper margin="0 0 15px 0" width="100%">
            <Flex alignItems="center" width="100%">
              <Checkbox
                type="checkbox"
                name="agree"
                className={`FormikForm-control ${
                  touched.agree && errors.agree && "is-invalid"
                }`}
              />
              <Text.H6 fontColor="white" margin="0 0 0 1em" weight={700}>
                AGREE with Privacy Policy
              </Text.H6>
              <ErrorMessage
                component="div"
                name="agree"
                className="invalid-feedback"
              />
            </Flex>
          </DivWrapper> */}
          <Flex justifyContent="flex-start">
            {/* {state.loading ? (
              <Spinner color="primary" />
            ) : ( */}

            {/* )} */}
          </Flex>
        </FormikForm>
      )}
    </Formik>
  );
};
