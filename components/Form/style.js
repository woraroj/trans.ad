import styled from "styled-components";
import { Field, Form } from "formik";

export const TextField = styled(Field)`
  background: ${(props) => props.theme.color.bgGrey};
  border: 0;
  border-radius: 0;
  margin: ${(props) => props.margin};
  color: ${(props) => props.theme.color.blue};
  height: ${(props) => props.height};
  width: 100%;
  font-size: 1em;
  padding: 5px 10px;
  ::placeholder {
    color: ${(props) => props.theme.color.blue};
  }
`;

export const FormikForm = styled(Form)`
  width: 100%;
`;

export const Checkbox = styled(Field)``;
