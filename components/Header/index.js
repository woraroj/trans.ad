import React, { useState, useEffect } from "react";
import Link from "next/link";
import { DivWrapper, Flex, Text } from "components";
import {
  Container,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import {
  Wrapper,
  A,
  FlagWrapper,
  NavWrapper,
  SlideDrawerStyled,
  SpanStyle,
  HamburgerWrapper,
  ProductWrapper,
  ProductSubWrapper,
} from "./style.js";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHome, faUsers, faSearch } from "@fortawesome/free-solid-svg-icons";
import { useRouter } from "next/router";
import * as Scroll from "react-scroll";
import ReactSearchBox from "react-search-box";
import { Data } from "../../public/searchData";
export const Header = () => {
  const router = useRouter();
  let LinkScroll = Scroll.Link;

  const [hover, setHover] = useState(router.route);
  const [modal, setModal] = useState(false);
  const [isCollapsed, setCollapsed] = useState(true);
  const [searchValue, setSearchValue] = useState();
  const toggle = () => setModal(!modal);
  useEffect(() => {
    setHover(router.route);
    if (!isCollapsed) {
      document.body.classList.add("lock-screen");
    } else {
      document.body.classList.remove("lock-screen");
    }
  }, [isCollapsed, router]);

  const onSearchClick = (record) => {
    toggle();
    router.push(record.url);
  };

  return (
    <DivWrapper className="absolute" width="100%">
      <Container fluid={true}>
        <Modal isOpen={modal} toggle={toggle}>
          <ModalBody>
            <ReactSearchBox
              placeholder="Search"
              data={Data}
              onSelect={(record) => onSearchClick(record)}
            />
          </ModalBody>
        </Modal>
        <DivWrapper>
          <Flex justifyContent="space-between">
            <Wrapper className="logo">
              <Link href="/en/home">
                <a>
                  <img src="/assets/logo.png" style={{ width: "180px" }} />
                </a>
              </Link>
            </Wrapper>
            <Wrapper className="nav">
              <Flex>
                <div
                  style={{
                    position: "relative",
                    display: "flex",
                    cursor: "pointer",
                  }}
                  onMouseOver={() => setHover("home")}
                  onMouseLeave={() => setHover(router.route)}
                >
                  <Link href="/en/home">
                    <Wrapper
                      className={
                        // hover === "home"
                        //   ? "active noshadow noradius"
                        //   : router.route.includes("home")
                        //   ? "active"
                        //   : "unactive"
                        hover === "/en/home"
                          ? "active"
                          : hover === "home"
                          ? "active"
                          : "unactive"
                      }
                    >
                      <A className="active">HOME</A>
                      <FontAwesomeIcon
                        icon={faHome}
                        style={{ color: "white", width: "15px" }}
                      />
                    </Wrapper>
                  </Link>
                  {/* <ProductWrapper
                    className={hover === "home" ? "" : "unactive"}
                  >
                    {router.route.includes("home") ? (
                      <>
                        <LinkScroll to="homeintro" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              1. Introduction
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="partner" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">2. Our Partner</Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                      </>
                    ) : (
                      <>
                        <Link href="/en/home#homeintro">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                1. Introduction
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/home#partner">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                2. Our Partner
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                      </>
                    )}
                  </ProductWrapper> */}
                  <A
                    className={
                      hover === "/en/home"
                        ? "hide"
                        : hover === "home"
                        ? "hide"
                        : ""
                    }
                  >
                    HOME
                  </A>
                </div>

                <div
                  style={{
                    position: "relative",
                    display: "flex",
                    cursor: "pointer",
                  }}
                  onMouseOver={() => setHover("aboutus")}
                  onMouseLeave={() => setHover(router.route)}
                >
                  <Link href="/en/aboutus">
                    <Wrapper
                      className={
                        // hover === "aboutus"
                        //   ? "active noshadow noradius"
                        //   : router.route.includes("aboutus")
                        //   ? "active"
                        //   : "unactive"
                        hover === "/en/aboutus"
                          ? "active"
                          : hover === "aboutus"
                          ? "active noshadow noradius"
                          : "unactive"
                      }
                    >
                      <A className="active">ABOUT US</A>
                      <FontAwesomeIcon
                        icon={faUsers}
                        style={{ color: "white", width: "15px" }}
                      />
                    </Wrapper>
                  </Link>
                  <ProductWrapper
                    className={hover === "aboutus" ? "" : "unactive"}
                  >
                    {router.route.includes("aboutus") ? (
                      <>
                        <LinkScroll to="aboutintro" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">Introduction</Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="overview" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              Overview of Trans.Ad Group's Business
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="cer" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              Certified Quality
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="news" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">Company News</Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                      </>
                    ) : (
                      <>
                        <Link href="/en/aboutus#aboutintro">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">Introduction</Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/aboutus#overview">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                Overview of Trans.Ad Group's Business
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/aboutus#cer">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                Certified Quality
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/aboutus#news">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">Company News</Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                      </>
                    )}
                  </ProductWrapper>
                  <A
                    className={
                      hover === "/en/aboutus"
                        ? "hide"
                        : hover === "aboutus"
                        ? "hide"
                        : ""
                    }
                  >
                    ABOUT US
                  </A>
                </div>

                <div
                  style={{
                    position: "relative",
                    display: "flex",
                    cursor: "pointer",
                  }}
                  onMouseOver={() => setHover("product")}
                  onMouseLeave={() => setHover(router.route)}
                >
                  <Link href="/en/product">
                    <Wrapper
                      className={
                        hover.includes("/en/product")
                          ? "active"
                          : hover === "product"
                          ? "active noshadow noradius"
                          : "unactive"
                      }
                    >
                      <A className="active">PRODUCTS</A>

                      <FontAwesomeIcon
                        icon={faUsers}
                        style={{ color: "white", width: "15px" }}
                      />
                    </Wrapper>
                  </Link>
                  <ProductWrapper
                    className={hover === "product" ? "" : "unactive"}
                  >
                    <Link href="/en/product/digital_displays">
                      <a style={{ textDecoration: "none" }}>
                        <ProductSubWrapper>
                          <Text.H5 className="nowrap">Digital Display</Text.H5>
                        </ProductSubWrapper>
                      </a>
                    </Link>
                    <Link href="/en/product/digital_displays#led_screen">
                      <a style={{ textDecoration: "none" }}>
                        <ProductSubWrapper>
                          <Text.H5 margin="0 0 0 15px" className="nowrap">
                            • LED Screen
                          </Text.H5>
                        </ProductSubWrapper>
                      </a>
                    </Link>
                    <Link href="/en/product/digital_displays#lcd_screen">
                      <a style={{ textDecoration: "none" }}>
                        <ProductSubWrapper>
                          <Text.H5 margin="0 0 0 15px" className="nowrap">
                            • LCD Screen
                          </Text.H5>
                        </ProductSubWrapper>
                      </a>
                    </Link>
                    <Link href="/en/product/digital_displays#custom-made_screen">
                      <a style={{ textDecoration: "none" }}>
                        <ProductSubWrapper>
                          <Text.H5 margin="0 0 0 15px" className="nowrap">
                            • Custom-made Screen
                          </Text.H5>
                        </ProductSubWrapper>
                      </a>
                    </Link>

                    <Link href="/en/product/digital_content_management_platform">
                      <a style={{ textDecoration: "none" }}>
                        <ProductSubWrapper>
                          <Text.H5 className="nowrap">
                            Digital Content Management Platform
                          </Text.H5>
                        </ProductSubWrapper>
                      </a>
                    </Link>

                    <Link href="/en/product/communication_systems_and_cyber_security">
                      <a style={{ textDecoration: "none" }}>
                        <ProductSubWrapper>
                          <Text.H5 className="nowrap">
                            Communication Systems & Cyber Security
                          </Text.H5>
                        </ProductSubWrapper>
                      </a>
                    </Link>
                  </ProductWrapper>

                  <A
                    className={
                      hover.includes("/en/product")
                        ? "hide"
                        : hover === "product"
                        ? "hide"
                        : ""
                    }
                  >
                    PRODUCTS
                  </A>
                </div>

                <div
                  style={{
                    position: "relative",
                    display: "flex",
                    cursor: "pointer",
                  }}
                  onMouseOver={() => setHover("service")}
                  onMouseLeave={() => setHover(router.route)}
                >
                  <Link href="/en/service">
                    <Wrapper
                      className={
                        // hover === "home"
                        //   ? "active noshadow noradius"
                        //   : router.route.includes("home")
                        //   ? "active"
                        //   : "unactive"
                        hover === "/en/service"
                          ? "active"
                          : hover === "service"
                          ? "active noshadow noradius"
                          : "unactive"
                      }
                    >
                      <A className="active">SERVICES</A>
                      <FontAwesomeIcon
                        icon={faUsers}
                        style={{ color: "white", width: "15px" }}
                      />
                    </Wrapper>
                  </Link>
                  <ProductWrapper
                    className={hover === "service" ? "" : "unactive"}
                  >
                    {router.route.includes("service") ? (
                      <>
                        <LinkScroll to="1" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              Project Management
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="2" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              Systems Intergration & Implementation
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="3" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              Content Management
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="4" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              Maintenance Services
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="5" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              Digital Creatives and 3D Content Production
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="6" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">Consultation</Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                      </>
                    ) : (
                      <>
                        <Link href="/en/service#1">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                Project Management
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/service#2">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                Systems Intergration & Implementation
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/service#3">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                Content Management
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/service#4">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                Maintenance Services
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/service#5">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                Digital Creatives and 3D Content Production
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/service#6">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">Consultation</Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                      </>
                    )}
                  </ProductWrapper>
                  <A
                    className={
                      hover === "/en/service"
                        ? "hide"
                        : hover === "service"
                        ? "hide"
                        : ""
                    }
                  >
                    SERVICES
                  </A>
                </div>
                <div
                  style={{
                    position: "relative",
                    display: "flex",
                    cursor: "pointer",
                  }}
                  onMouseOver={() => setHover("reference")}
                  onMouseLeave={() => setHover(router.route)}
                >
                  <Link href="/en/reference">
                    <Wrapper
                      className={
                        hover === "/en/reference"
                          ? "active"
                          : hover === "reference"
                          ? "active noshadow noradius"
                          : "unactive"
                      }
                    >
                      <A className="active"> REFERENCES</A>
                      <FontAwesomeIcon
                        icon={faUsers}
                        style={{ color: "white", width: "15px" }}
                      />
                    </Wrapper>
                  </Link>
                  <ProductWrapper
                    className={hover === "reference" ? "" : "unactive"}
                  >
                    {router.route.includes("reference") ? (
                      <>
                        <LinkScroll to="transportation" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">Transportation</Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="out" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              Digital Out of Home Media
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="office" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              Office Building & Shopping Malls
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="other" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">Others</Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                      </>
                    ) : (
                      <>
                        <Link href="/en/reference#transportation">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                Transportation
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/reference#out">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                Digital Out of Home Media
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/reference#office">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                Office Building & Shopping Malls
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/reference#other">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">Others</Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                      </>
                    )}
                  </ProductWrapper>
                  <A
                    className={
                      hover === "/en/reference"
                        ? "hide"
                        : hover === "reference"
                        ? "hide"
                        : ""
                    }
                  >
                    REFERENCES
                  </A>
                </div>
                <div
                  style={{
                    position: "relative",
                    display: "flex",
                    cursor: "pointer",
                  }}
                  onMouseOver={() => setHover("contact")}
                  onMouseLeave={() => setHover(router.route)}
                >
                  <Link href="/en/contact">
                    <Wrapper
                      className={
                        hover.includes("/en/contact")
                          ? "active"
                          : hover === "contact"
                          ? "active noshadow noradius"
                          : "unactive"
                      }
                      width="114px"
                    >
                      <A className="active">CONTACT</A>
                      <FontAwesomeIcon
                        icon={faUsers}
                        style={{ color: "white", width: "15px" }}
                      />
                    </Wrapper>
                  </Link>
                  <ProductWrapper
                    className={hover === "contact" ? "" : "unactive"}
                  >
                    {router.route.includes("contact") ? (
                      <>
                        <LinkScroll to="con" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">Contact Us</Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>

                        <LinkScroll to="job" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">Career</Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                      </>
                    ) : (
                      <>
                        <Link href="/en/contact#con">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">Contact Us</Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>

                        <Link href="/en/contact#job">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">Career</Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                      </>
                    )}
                  </ProductWrapper>
                  <A
                    className={
                      hover.includes("/en/contact")
                        ? "hide"
                        : hover === "contact"
                        ? "hide"
                        : ""
                    }
                  >
                    CONTACT
                  </A>
                </div>
              </Flex>
              <FlagWrapper
                className="disable"
                background={`url(/assets/flag-usa.jpg)`}
              />
              <Link href="/th/home">
                <a>
                  <FlagWrapper
                    className="active"
                    background={`url(/assets/flag-thai.png)`}
                  />
                </a>
              </Link>
              <div
                style={{
                  margin: "0 2em",
                  marginTop: "0.8em",
                  cursor: "pointer",
                }}
                onClick={() => toggle()}
              >
                <FontAwesomeIcon
                  icon={faSearch}
                  style={{
                    color: "black",
                    width: "15px",
                  }}
                />
              </div>
            </Wrapper>
            <NavWrapper className="responsive-visble " index="10">
              <Hamburger
                setCollapsed={setCollapsed}
                isCollapsed={isCollapsed}
                bgColor="white"
              />
              <SlideDrawerStyled collapsed={isCollapsed}>
                <Flex
                  padding="2em"
                  flexDirection="column"
                  justifyContent="space-between"
                  height="100%"
                  overflow="scroll"
                >
                  <>
                    <Link href="/en/home">
                      <A
                        className={
                          router.route === "/en/home"
                            ? "respon-active red"
                            : "blue"
                        }
                        onClick={() => setCollapsed(!isCollapsed)}
                      >
                        <Text.H2 weight={400}>HOME</Text.H2>
                      </A>
                    </Link>
                    <Link href="/en/aboutus">
                      <A
                        className={
                          router.route === "/en/aboutus"
                            ? "respon-active red"
                            : "blue"
                        }
                        onClick={() => setCollapsed(!isCollapsed)}
                      >
                        <Text.H2 weight={400}>ABOUT US</Text.H2>
                      </A>
                    </Link>
                    <Link href="/en/product">
                      <A
                        className={
                          router.route.includes("product")
                            ? "respon-active red"
                            : "blue"
                        }
                        onClick={() => setCollapsed(!isCollapsed)}
                      >
                        <Text.H2 weight={400}>PRODUCTS</Text.H2>
                      </A>
                    </Link>
                    <Link href="/en/service">
                      <A
                        className={
                          router.route === "/en/service"
                            ? "respon-active red"
                            : "blue"
                        }
                        onClick={() => setCollapsed(!isCollapsed)}
                      >
                        <Text.H2 weight={400}>SERVICES</Text.H2>
                      </A>
                    </Link>
                    <Link href="/en/reference">
                      <A
                        className={
                          router.route === "/en/reference"
                            ? "respon-active red"
                            : "blue"
                        }
                        onClick={() => setCollapsed(!isCollapsed)}
                      >
                        <Text.H2>REFERENCE</Text.H2>
                      </A>
                    </Link>
                    <Link href="/en/contact">
                      <A
                        className={
                          router.route.includes("contact")
                            ? "respon-active red"
                            : "blue"
                        }
                        onClick={() => setCollapsed(!isCollapsed)}
                      >
                        <Text.H2>CONTACT US</Text.H2>
                      </A>
                    </Link>
                  </>
                  <Flex flexDirection="column" margin="5em 0 0 0">
                    <img
                      src="/assets/logo.png"
                      style={{ width: "180px", margin: "0.5em 0" }}
                    />
                    <Text.H5 weight={300} fontColor="white">
                      Trans.Ad Solutions Co.,Ltd. 21th Floor, TST Tower, No. 21
                      Viphawadi-Rangsit Road, Chomphon, Chatuchak, Bangkok 10900
                      TH
                    </Text.H5>
                  </Flex>
                </Flex>
              </SlideDrawerStyled>
            </NavWrapper>
          </Flex>
        </DivWrapper>
      </Container>
    </DivWrapper>
  );
};
const Hamburger = (props) => {
  const { setCollapsed, isCollapsed, isHome, bgColor } = props;

  return (
    <HamburgerWrapper onClick={() => setCollapsed(!isCollapsed)}>
      <SpanStyle
        className="one"
        bgColor={bgColor}
        collapsed={isCollapsed}
      ></SpanStyle>
      <SpanStyle
        bgColor={bgColor}
        collapsed={isCollapsed}
        className="two"
      ></SpanStyle>
      <SpanStyle
        className="three"
        bgColor={bgColor}
        collapsed={isCollapsed}
      ></SpanStyle>
    </HamburgerWrapper>
  );
};

export const HeaderTh = () => {
  const router = useRouter();
  let LinkScroll = Scroll.Link;

  const [hover, setHover] = useState(router.route);
  const [modal, setModal] = useState(false);
  const [isCollapsed, setCollapsed] = useState(true);
  const [searchValue, setSearchValue] = useState();
  const toggle = () => setModal(!modal);
  useEffect(() => {
    setHover(router.route);
    if (!isCollapsed) {
      document.body.classList.add("lock-screen");
    } else {
      document.body.classList.remove("lock-screen");
    }
  }, [isCollapsed, router]);

  const onSearchClick = (record) => {
    toggle();
    router.push(record.url);
  };

  return (
    <DivWrapper className="absolute" width="100%">
      <Container fluid={true}>
        <Modal isOpen={modal} toggle={toggle}>
          <ModalBody>
            <ReactSearchBox
              placeholder="Search"
              data={Data}
              onSelect={(record) => onSearchClick(record)}
            />
          </ModalBody>
        </Modal>
        <DivWrapper>
          <Flex justifyContent="space-between">
            <Wrapper className="logo">
              <Link href="/th/home">
                <a>
                  <img src="/assets/logo.png" style={{ width: "180px" }} />
                </a>
              </Link>
            </Wrapper>
            <Wrapper className="nav">
              <Flex>
                <div
                  style={{
                    position: "relative",
                    display: "flex",
                    cursor: "pointer",
                  }}
                  onMouseOver={() => setHover("home")}
                  onMouseLeave={() => setHover(router.route)}
                >
                  <Link href="/th/home">
                    <Wrapper
                      className={
                        // hover === "home"
                        //   ? "active noshadow noradius"
                        //   : router.route.includes("home")
                        //   ? "active"
                        //   : "unactive"
                        hover === "/th/home"
                          ? "active"
                          : hover === "home"
                          ? "active"
                          : "unactive"
                      }
                    >
                      <A className="active">หน้าแรก</A>
                      <FontAwesomeIcon
                        icon={faHome}
                        style={{ color: "white", width: "15px" }}
                      />
                    </Wrapper>
                  </Link>
                  {/* <ProductWrapper
                    className={hover === "home" ? "" : "unactive"}
                  >
                    {router.route.includes("home") ? (
                      <>
                        <LinkScroll to="homeintro" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              1. Introduction
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="partner" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">2. Our Partner</Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                      </>
                    ) : (
                      <>
                        <Link href="/en/home#homeintro">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                1. Introduction
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/en/home#partner">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                2. Our Partner
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                      </>
                    )}
                  </ProductWrapper> */}
                  <A
                    className={
                      hover === "/th/home"
                        ? "hide"
                        : hover === "home"
                        ? "hide"
                        : ""
                    }
                  >
                    หน้าหลัก
                  </A>
                </div>

                <div
                  style={{
                    position: "relative",
                    display: "flex",
                    cursor: "pointer",
                  }}
                  onMouseOver={() => setHover("aboutus")}
                  onMouseLeave={() => setHover(router.route)}
                >
                  <Link href="/th/aboutus">
                    <Wrapper
                      className={
                        // hover === "aboutus"
                        //   ? "active noshadow noradius"
                        //   : router.route.includes("aboutus")
                        //   ? "active"
                        //   : "unactive"
                        hover === "/th/aboutus"
                          ? "active"
                          : hover === "aboutus"
                          ? "active noshadow noradius"
                          : "unactive"
                      }
                    >
                      <A className="active">เกี่ยวกับเรา</A>
                      <FontAwesomeIcon
                        icon={faUsers}
                        style={{ color: "white", width: "15px" }}
                      />
                    </Wrapper>
                  </Link>
                  <ProductWrapper
                    className={hover === "aboutus" ? "" : "unactive"}
                  >
                    {router.route.includes("aboutus") ? (
                      <>
                        <LinkScroll to="aboutintro" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">บทนำ</Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="overview" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              ภาพรวมธุรกิจของกลุ่มบริษัททรานส์.แอด
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="cer" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              มาตรฐานการรับรอง
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="news" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">ข่าวสารองค์กร</Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                      </>
                    ) : (
                      <>
                        <Link href="/th/aboutus#aboutintro">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">บทนำ</Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/th/aboutus#overview">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                ภาพรวมธุรกิจของกลุ่มบริษัททรานส์.แอด
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/th/aboutus#cer">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                มาตรฐานการรับรอง{" "}
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/th/aboutus#news">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                ข่าวสารองค์กร
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                      </>
                    )}
                  </ProductWrapper>
                  <A
                    className={
                      hover === "/th/aboutus"
                        ? "hide"
                        : hover === "aboutus"
                        ? "hide"
                        : ""
                    }
                  >
                    เกี่ยวกับเรา
                  </A>
                </div>

                <div
                  style={{
                    position: "relative",
                    display: "flex",
                    cursor: "pointer",
                  }}
                  onMouseOver={() => setHover("product")}
                  onMouseLeave={() => setHover(router.route)}
                >
                  <Link href="/th/product">
                    <Wrapper
                      className={
                        hover.includes("/th/product")
                          ? "active"
                          : hover === "product"
                          ? "active noshadow noradius"
                          : "unactive"
                      }
                    >
                      <A className="active">สินค้า/ผลิตภัณฑ์</A>

                      <FontAwesomeIcon
                        icon={faUsers}
                        style={{ color: "white", width: "15px" }}
                      />
                    </Wrapper>
                  </Link>
                  <ProductWrapper
                    className={hover === "product" ? "" : "unactive"}
                  >
                    <Link href="/th/product/digital_displays">
                      <a style={{ textDecoration: "none" }}>
                        <ProductSubWrapper>
                          <Text.H5 className="nowrap">จอดิจิทัล</Text.H5>
                        </ProductSubWrapper>
                      </a>
                    </Link>
                    <Link href="/th/product/digital_displays#led_screen">
                      <a style={{ textDecoration: "none" }}>
                        <ProductSubWrapper>
                          <Text.H5 margin="0 0 0 15px" className="nowrap">
                            • LED Screen
                          </Text.H5>
                        </ProductSubWrapper>
                      </a>
                    </Link>
                    <Link href="/th/product/digital_displays#lcd_screen">
                      <a style={{ textDecoration: "none" }}>
                        <ProductSubWrapper>
                          <Text.H5 margin="0 0 0 15px" className="nowrap">
                            • LCD Screen
                          </Text.H5>
                        </ProductSubWrapper>
                      </a>
                    </Link>
                    <Link href="/th/product/digital_displays#custom-made_screen">
                      <a style={{ textDecoration: "none" }}>
                        <ProductSubWrapper>
                          <Text.H5 margin="0 0 0 15px" className="nowrap">
                            • จอสั่งทำพิเศษตามความต้องการของลูกค้า
                          </Text.H5>
                        </ProductSubWrapper>
                      </a>
                    </Link>

                    <Link href="/th/product/digital_content_management_platform">
                      <a style={{ textDecoration: "none" }}>
                        <ProductSubWrapper>
                          <Text.H5 className="nowrap">
                            ระบบจัดการสื่อโฆษณาดิจิทัล
                          </Text.H5>
                        </ProductSubWrapper>
                      </a>
                    </Link>

                    <Link
                      href="/th/product/communication_systems_and_cyber_security
"
                    >
                      <a style={{ textDecoration: "none" }}>
                        <ProductSubWrapper>
                          <Text.H5 className="nowrap">
                            ระบบสื่อสารและการรักษาความปลอดภัยของระบบเครือข่าย
                          </Text.H5>
                        </ProductSubWrapper>
                      </a>
                    </Link>
                  </ProductWrapper>

                  <A
                    className={
                      hover.includes("/th/product")
                        ? "hide"
                        : hover === "product"
                        ? "hide"
                        : ""
                    }
                  >
                    ผลิตภัณฑ์
                  </A>
                </div>

                <div
                  style={{
                    position: "relative",
                    display: "flex",
                    cursor: "pointer",
                  }}
                  onMouseOver={() => setHover("service")}
                  onMouseLeave={() => setHover(router.route)}
                >
                  <Link href="/th/service">
                    <Wrapper
                      className={
                        // hover === "home"
                        //   ? "active noshadow noradius"
                        //   : router.route.includes("home")
                        //   ? "active"
                        //   : "unactive"
                        hover === "/th/service"
                          ? "active"
                          : hover === "service"
                          ? "active noshadow noradius"
                          : "unactive"
                      }
                    >
                      <A className="active">บริการ</A>
                      <FontAwesomeIcon
                        icon={faUsers}
                        style={{ color: "white", width: "15px" }}
                      />
                    </Wrapper>
                  </Link>
                  <ProductWrapper
                    className={hover === "service" ? "" : "unactive"}
                  >
                    {router.route.includes("service") ? (
                      <>
                        <LinkScroll to="1" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              บริหารจัดการโครงการ
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="2" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              บูรณาการและวางระบบ
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="3" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              การจัดการสื่อโฆษณาดิจิทัล
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="4" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              บริการซ่อมบำรุงรักษา
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="5" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              บริการออกแบบและจัดทำสื่อโฆษณาดิจิทัลแบบต่างๆ
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="6" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              บริการให้คำปรึกษา
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                      </>
                    ) : (
                      <>
                        <Link href="/th/service#1">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                บริหารจัดการโครงการ
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/th/service#2">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                บูรณาการและวางระบบ
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/th/service#3">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                การจัดการสื่อโฆษณาดิจิทัล
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/th/service#4">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                บริการซ่อมบำรุงรักษา
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/th/service#5">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                บริการออกแบบและจัดทำสื่อโฆษณาดิจิทัลแบบต่างๆ
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/th/service#6">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                บริการให้คำปรึกษา
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                      </>
                    )}
                  </ProductWrapper>
                  <A
                    className={
                      hover === "/th/service"
                        ? "hide"
                        : hover === "service"
                        ? "hide"
                        : ""
                    }
                  >
                    การบริการ
                  </A>
                </div>
                <div
                  style={{
                    position: "relative",
                    display: "flex",
                    cursor: "pointer",
                  }}
                  onMouseOver={() => setHover("reference")}
                  onMouseLeave={() => setHover(router.route)}
                >
                  <Link href="/th/reference">
                    <Wrapper
                      className={
                        hover === "/th/reference"
                          ? "active"
                          : hover === "reference"
                          ? "active noshadow noradius"
                          : "unactive"
                      }
                    >
                      <A className="active"> ผลงานที่ผ่านมา</A>
                      <FontAwesomeIcon
                        icon={faUsers}
                        style={{ color: "white", width: "15px" }}
                      />
                    </Wrapper>
                  </Link>
                  <ProductWrapper
                    className={hover === "reference" ? "" : "unactive"}
                  >
                    {router.route.includes("reference") ? (
                      <>
                        <LinkScroll to="transportation" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">ระบบขนส่งมวลชน</Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="out" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              สื่อโฆษณาดิจิทัล
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="office" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">
                              อาคารสำนักงานและศูนย์การค้า
                            </Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                        <LinkScroll to="other" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">อื่นๆ</Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                      </>
                    ) : (
                      <>
                        <Link href="/th/reference#transportation">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                ระบบขนส่งมวลชน
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/th/reference#out">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                สื่อโฆษณาดิจิทัล
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/th/reference#office">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                อาคารสำนักงานและศูนย์การค้า
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                        <Link href="/th/reference#other">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">อื่นๆ</Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                      </>
                    )}
                  </ProductWrapper>
                  <A
                    className={
                      hover === "/th/reference"
                        ? "hide"
                        : hover === "reference"
                        ? "hide"
                        : ""
                    }
                  >
                    ผลงานที่ผ่านมา
                  </A>
                </div>
                <div
                  style={{
                    position: "relative",
                    display: "flex",
                    cursor: "pointer",
                  }}
                  onMouseOver={() => setHover("contact")}
                  onMouseLeave={() => setHover(router.route)}
                >
                  <Link href="/th/contact">
                    <Wrapper
                      className={
                        hover.includes("/th/contact")
                          ? "active"
                          : hover === "contact"
                          ? "active noshadow noradius"
                          : "unactive"
                      }
                      width="114px"
                    >
                      <A className="active">ติดต่อเรา</A>
                      <FontAwesomeIcon
                        icon={faUsers}
                        style={{ color: "white", width: "15px" }}
                      />
                    </Wrapper>
                  </Link>
                  <ProductWrapper
                    className={hover === "contact" ? "" : "unactive"}
                  >
                    {router.route.includes("contact") ? (
                      <>
                        <LinkScroll to="con" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">ติดต่อเรา</Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>

                        <LinkScroll to="job" smooth={true}>
                          <ProductSubWrapper>
                            <Text.H5 className="nowrap">ร่วมงานกับเรา</Text.H5>
                          </ProductSubWrapper>
                        </LinkScroll>
                      </>
                    ) : (
                      <>
                        <Link href="/th/contact#con">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">ติดต่อเรา</Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>

                        <Link href="/th/contact#job">
                          <a style={{ textDecoration: "none" }}>
                            <ProductSubWrapper>
                              <Text.H5 className="nowrap">
                                ร่วมงานกับเรา
                              </Text.H5>
                            </ProductSubWrapper>
                          </a>
                        </Link>
                      </>
                    )}
                  </ProductWrapper>
                  <A
                    className={
                      hover.includes("/th/contact")
                        ? "hide"
                        : hover === "contact"
                        ? "hide"
                        : ""
                    }
                  >
                    ติดต่อเรา
                  </A>
                </div>
              </Flex>
              <Link href="/">
                <a>
                  <FlagWrapper
                    className="active"
                    background={`url(/assets/flag-usa.jpg)`}
                  />
                </a>
              </Link>
              <FlagWrapper
                className="disable"
                background={`url(/assets/flag-thai.png)`}
              />

              <div
                style={{
                  margin: "0 2em",
                  marginTop: "0.8em",
                  cursor: "pointer",
                }}
                onClick={() => toggle()}
              >
                <FontAwesomeIcon
                  icon={faSearch}
                  style={{
                    color: "black",
                    width: "15px",
                  }}
                />
              </div>
            </Wrapper>
            <NavWrapper className="responsive-visble " index="10">
              <Hamburger
                setCollapsed={setCollapsed}
                isCollapsed={isCollapsed}
                bgColor="white"
              />
              <SlideDrawerStyled collapsed={isCollapsed}>
                <Flex
                  padding="2em"
                  flexDirection="column"
                  justifyContent="space-between"
                  height="100%"
                  overflow="scroll"
                >
                  <>
                    <Link href="/th/home">
                      <A
                        className={
                          router.route === "/th/home"
                            ? "respon-active red"
                            : "blue"
                        }
                        onClick={() => setCollapsed(!isCollapsed)}
                      >
                        <Text.H2 weight={400}>หน้าแรก</Text.H2>
                      </A>
                    </Link>
                    <Link href="/th/aboutus">
                      <A
                        className={
                          router.route === "/th/aboutus"
                            ? "respon-active red"
                            : "blue"
                        }
                        onClick={() => setCollapsed(!isCollapsed)}
                      >
                        <Text.H2 weight={400}>เกี่ยวกับเรา</Text.H2>
                      </A>
                    </Link>
                    <Link href="/th/product">
                      <A
                        className={
                          router.route.includes("product")
                            ? "respon-active red"
                            : "blue"
                        }
                        onClick={() => setCollapsed(!isCollapsed)}
                      >
                        <Text.H2 weight={400}>ผลิตภัณฑ์</Text.H2>
                      </A>
                    </Link>
                    <Link href="/th/service">
                      <A
                        className={
                          router.route === "/th/service"
                            ? "respon-active red"
                            : "blue"
                        }
                        onClick={() => setCollapsed(!isCollapsed)}
                      >
                        <Text.H2 weight={400}>บริการ</Text.H2>
                      </A>
                    </Link>
                    <Link href="/th/reference">
                      <A
                        className={
                          router.route === "/th/reference"
                            ? "respon-active red"
                            : "blue"
                        }
                        onClick={() => setCollapsed(!isCollapsed)}
                      >
                        <Text.H2>งานตัวอย่าง</Text.H2>
                      </A>
                    </Link>
                    <Link href="/th/contact">
                      <A
                        className={
                          router.route.includes("contact")
                            ? "respon-active red"
                            : "blue"
                        }
                        onClick={() => setCollapsed(!isCollapsed)}
                      >
                        <Text.H2>ติดต่อเรา</Text.H2>
                      </A>
                    </Link>
                  </>
                  <Flex flexDirection="column" margin="5em 0 0 0">
                    <img
                      src="/assets/logo.png"
                      style={{ width: "180px", margin: "0.5em 0" }}
                    />
                    <Text.H5 weight={300} fontColor="white">
                      Trans.Ad Solutions Co.,Ltd. 21th Floor, TST Tower, No. 21
                      Viphawadi-Rangsit Road, Chomphon, Chatuchak, Bangkok 10900
                      TH
                    </Text.H5>
                  </Flex>
                </Flex>
              </SlideDrawerStyled>
            </NavWrapper>
          </Flex>
        </DivWrapper>
      </Container>
    </DivWrapper>
  );
};
