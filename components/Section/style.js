import styled from "styled-components";

export const SectionStyle = styled.div`
  background: ${(props) => props.theme.color[props.bgcolor]};
  background-image: url(${(props) => props.bg});
  background-position: top;
  background-repeat: no-repeat;
  background-size: cover;
  width: ${(props) => props.width};
  height: ${(props) => props.height};
  min-height: ${(props) => props.minHeight};
  padding: ${(props) => props.padding};
  position: ${(props) => props.position};
  &.filter {
    background-image: linear-gradient(rgb(0, 0, 0, 0.5), rgb(0, 0, 0, 0.5)),
      url(${(props) => props.bg});
  }
`;
