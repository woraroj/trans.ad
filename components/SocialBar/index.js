import React from "react";
import { Flex } from "components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFacebookF, faTwitter } from "@fortawesome/free-brands-svg-icons";
import { Text, LogoWrapper } from "./style";
export const SocialBar = ({ th }) => {
  const handleClick = (url) => {
    window.open(url, "_blank");
  };
  return (
    <div>
      <Text>{th ? "เยี่ยมชมเราได้ที่" : "visit us on"}</Text>
      <Flex>
        <LogoWrapper
          onClick={() =>
            handleClick("https://www.facebook.com/TransAdSolutions")
          }
        >
          <FontAwesomeIcon icon={faFacebookF} style={{ width: "15px" }} />
        </LogoWrapper>
        <LogoWrapper
          onClick={() => handleClick("https://twitter.com/TransAdSolution")}
        >
          <FontAwesomeIcon icon={faTwitter} style={{ width: "20px" }} />
        </LogoWrapper>
      </Flex>
    </div>
  );
};
