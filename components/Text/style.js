import styled, { css } from "styled-components";

const defaultStyle = css`
  width: ${(props) => props.width};
  text-align: ${(props) => props.textAlign};
  font-weight: ${(props) => props.weight};
  margin: ${(props) => props.margin};
  padding: ${(props) => props.padding};
  color: ${(props) => props.theme.color[props.fontColor]};
  cursor: ${(props) => props.cursor};
  text-decoration: ${(props) => props.decoration};
  font-family: ${(props) => props.fontFamily};
  letter-spacing: ${(props) => props.space};
  line-height: ${(props) => props.lineHeight};
  white-space: pre-line;

  transition-duration: 0.25s;
  &.nowrap {
    white-space: nowrap;
  }
  &.limit {
    display: -webkit-box;
    overflow: hidden;
    text-overflow: ellipsis;
    -webkit-line-clamp: 3;
    -webkit-box-orient: vertical;
  }
  &.text-shadow {
    text-shadow: 2px 2px 4px #000000;
  }
  &.bring-front {
    z-index: 99;
  }
  &.underline {
    border-bottom: 2px solid ${(props) => props.theme.color.blue};
  }
`;

export const TextH1Style = styled.h1`
  ${defaultStyle};
  font-size: ${(props) => props.theme.fontSize.xLarge};
`;

export const TextH2Style = styled.h2`
  ${defaultStyle};
  font-size: ${(props) => props.theme.fontSize.large};
`;

export const TextH3Style = styled.h3`
  ${defaultStyle};
  font-size: ${(props) => props.theme.fontSize.medium};
`;
export const TextH4Style = styled.h4`
  ${defaultStyle};
  font-size: ${(props) => props.theme.fontSize.small};
  @media only screen and (max-width: 768px) {
    font-size: ${(props) => props.theme.fontSize.xSmall};
  }
`;
export const TextH5Style = styled.h5`
  ${defaultStyle};
  font-size: ${(props) => props.theme.fontSize.default};
`;
export const TextH6Style = styled.h6`
  ${defaultStyle};
  font-size: ${(props) => props.theme.fontSize.xSmall};
  @media only screen and (max-width: 768px) {
    font-size: ${(props) => props.theme.fontSize.xxSmall};
  }
`;

export const TextCustomStyle = styled.span`
  ${defaultStyle};
  font-size: ${(props) => props.theme.fontSize[props.size]};
  cursor: ${(props) => props.onClick && "pointer"};
`;
