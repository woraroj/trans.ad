import React from "react";
import { Section, Flex, Text, Button } from "components";
import { Container } from "reactstrap";
import Link from "next/link";
import {
  DirectLink,
  Element,
  Events,
  animateScroll as scroll,
  scrollSpy,
  scroller,
} from "react-scroll";
import { useRouter } from "next/router";
export const AboutIntro = ({ introText, introImg, status, th }) => {
  const router = useRouter();
  console.log(status);
  return (
    <Section padding="5em 0">
      <Element name="aboutintro" />
      <Container>
        <Flex className="shadow">
          <Flex className="responsive-hide " width="50%">
            <img
              src={introImg && introImg.url}
              style={{ width: "100%", height: "100%", objectFit: "cover" }}
            />
          </Flex>
          <Flex
            className="responsive-full-width "
            padding="2em"
            width="50%"
            flexDirection="column"
            alignItems="flex-start"
          >
            <Text.H3 weight={500} margin="0 0 1em 0" fontColor="blue">
              {th ? "บทนำ" : "INTRODUCTION"}
            </Text.H3>
            <Text.H5
              className="textFull"
              textAlign="justify"
              space="1px"
              weight={200}
              margin="0 0 1em 0"
              fontColor="grey"
              lineHeight="23px"
            >
              {introText && introText}
            </Text.H5>
            {router.route.includes("/en/") ? (
              <Link href="/en/reference">
                <a>
                  <Button className="inline" bgcolor="blue">
                    Reference Project
                  </Button>
                </a>
              </Link>
            ) : (
              <Link href="/th/reference">
                <a>
                  <Button className="inline" bgcolor="blue">
                    ชมงานตัวอย่าง
                  </Button>
                </a>
              </Link>
            )}
          </Flex>
        </Flex>
      </Container>
    </Section>
  );
};
