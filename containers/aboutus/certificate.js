import React from "react";
import { Section, Flex, DivWrapper, Text } from "components";
import { Container } from "reactstrap";
import {
  DirectLink,
  Element,
  Events,
  animateScroll as scroll,
  scrollSpy,
  scroller,
} from "react-scroll";
export const Certificate = ({ data, th }) => {
  return (
    <Section padding="2em 0">
      <Element name="cer" />
      <Container>
        <Flex className="shadow responsive-column">
          <Flex className="responsive-full-width " width="50%">
            <img
              src="/assets/cer1.jpeg"
              style={{ width: "100%", height: "100%", objectFit: "cover" }}
            />
          </Flex>
          <Flex
            className="responsive-full-width "
            padding="2em"
            width="50%"
            flexDirection="column"
            alignItems="flex-start"
          >
            <Text.H3 weight={500} margin="0 0 1em 0" fontColor="blue">
              {th ? "มาตรฐานการรับรอง" : "Certified Quality"}
            </Text.H3>
            <Text.H4
              space="1px"
              weight={500}
              margin="1em 0"
              fontColor="blue"
              lineHeight="23px"
            >
              {th ? "ขอบเขต" : "Scope"}
            </Text.H4>
            <Text.H5
              space="1px"
              weight={200}
              margin="0 0 1em 0"
              fontColor="grey"
              lineHeight="23px"
            >
              {th
                ? "จัดหาและจำหน่ายอุปกรณ์จอดิจิทัล ระบบสื่อสาร พร้อมทั้งให้บริการติดตั้ง วางระบบและซ่อมบำรุง"
                : "Provision of Digital Display Products, Communication System, and Maintenance Service"}
            </Text.H5>
            <Text.H4
              space="1px"
              weight={500}
              margin="1em 0"
              fontColor="blue"
              lineHeight="23px"
            >
              {th ? "นโยบายคุณภาพ" : "Quality Policy"}
            </Text.H4>
            <Text.H5
              space="1px"
              weight={200}
              margin="0 0 1em 0"
              fontColor="grey"
              lineHeight="23px"
            >
              {th
                ? "ส่งมอบสินค้าและบริการตรงเวลา ภายใต้งบประมาณที่กำหนด ตามความต้องการของลูกค้า"
                : "Deliver products and services on-time within budget and comply to customer requirements"}
            </Text.H5>
          </Flex>
        </Flex>
      </Container>
    </Section>
  );
};
