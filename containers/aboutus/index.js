export * from "./landing";
export * from "./certificate";
export * from "./history";
export * from "./reference";
export * from "./news";
export * from "./aboutIntro";
