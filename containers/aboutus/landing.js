import React, { useState } from "react";
import { Section, Flex, Text, Button, SocialBar, DivWrapper } from "components";
import { Container } from "reactstrap";
import VideoCover from "react-video-cover";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faVolumeMute, faVolumeUp } from "@fortawesome/free-solid-svg-icons";
export const Landing = ({ data, th }) => {
  const [mute, setMute] = useState(true);
  function toggleMute() {
    var video = document.getElementById("myVideo");

    video.muted = !video.muted;
    setMute(!mute);
  }
  return (
    <Section width="100%">
      <DivWrapper className="video-bg">
        <VideoCover
          videoOptions={{
            id: "myVideo",
            src: "/assets/bgvdo.mp4",
            autoPlay: "autoplay",
            muted: true,
            loop: true,
            playsInline: "playsinline",
          }}
          style={{ zIndex: "99" }}
        />
      </DivWrapper>
      <Container>
        <Flex
          flexDirection="column"
          justifyContent="center"
          alignItems="flex-start"
          height="100vh"
        >
          <Flex
            flexDirection="column"
            justifyContent="center"
            alignItems="center"
            width="100%"
          ></Flex>

          <DivWrapper className="absolute" bottom="15%">
            <SocialBar th={true} />{" "}
            <Button
              margin="1em 0"
              className="mute"
              onClick={() => toggleMute()}
            >
              {mute ? (
                <FontAwesomeIcon
                  icon={faVolumeMute}
                  style={{ width: "15px" }}
                />
              ) : (
                <FontAwesomeIcon icon={faVolumeUp} style={{ width: "15px" }} />
              )}
            </Button>
          </DivWrapper>
        </Flex>
      </Container>
    </Section>
  );
};
