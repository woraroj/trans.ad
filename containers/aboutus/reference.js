import React from "react";
import { Section, Flex, DivWrapper, Text } from "components";
import { Container, Spinner } from "reactstrap";
import Link from "next/link";
import Slider from "react-slick";
import {
  faChevronLeft,
  faChevronRight,
} from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import useSWR from "swr";
import { useRouter } from "next/router";
export const Reference = ({ th }) => {
  const fetcher = (url) => fetch(url).then((r) => r.json());
  const { data, error } = useSWR(`${process.env.api_url}/references`, fetcher);
  var settings = {
    responsive: [
      {
        breakpoint: 768,
        settings: {
          dots: false,
          autoplay: false,
          autoplaySpeed: 2000,
          speed: 2000,
          centerMode: true,
          arrows: false,
          infinite: true,
          slidesToScroll: 1,
          slidesToShow: 1,
          swipeToSlide: true,
          variableWidth: true,
          adaptiveHeight: true,
        },
      },
      {
        breakpoint: 992,
        settings: {
          dots: false,
          autoplay: false,
          autoplaySpeed: 2000,
          speed: 2000,
          centerMode: true,
          arrows: true,
          infinite: true,
          slidesToScroll: 1,
          slidesToShow: 1,
          swipeToSlide: true,
          variableWidth: true,
          adaptiveHeight: true,
        },
      },
    ],
    focusOnSelect: true,
    dots: true,
    infinite: true,
    // lazyLoad: true,
    centerMode: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    variableWidth: true,
    nextArrow: <SampleNextArrow />,
    prevArrow: <SamplePrevArrow />,
  };
  function SampleNextArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block" }}
        onClick={onClick}
      >
        <FontAwesomeIcon
          icon={faChevronRight}
          style={{ width: "30px", height: "30px" }}
          color="red"
        />
      </div>
    );
  }

  function SamplePrevArrow(props) {
    const { className, style, onClick } = props;
    return (
      <div
        className={className}
        style={{ ...style, display: "block", left: "-35px" }}
        onClick={onClick}
      >
        <FontAwesomeIcon
          icon={faChevronLeft}
          color="red"
          style={{ width: "30px", height: "30px", marginRight: "10px" }}
        />
      </div>
    );
  }
  return (
    <Section padding="3em 0">
      <Container>
        <Flex flexDirection="column" alignItems="center">
          <Text.H3 weight={500} fontColor="blue">
            {th ? "ผลงานที่ผ่านมา" : "REFERENCE PROJECT"}
          </Text.H3>
          {/* <Text.H5 margin="0.5em 0" fontColor="grey">
            text text text
          </Text.H5> */}
        </Flex>
        {data ? (
          <DivWrapper className="slwrapper" margin="2em 0">
            <Slider {...settings}>
              {data &&
                data
                  .filter((data) => data.id < 7)
                  .map((ref, index) => <RefWrapper data={ref} key={index} />)}
            </Slider>
          </DivWrapper>
        ) : (
          <Flex height="500px" justifyContent="center" alignItems="center">
            <Spinner color="danger" />
          </Flex>
        )}
      </Container>
    </Section>
  );
};
const RefWrapper = ({ data }) => {
  const router = useRouter();
  return (
    <Link
      href={router.route.includes("/en/") ? "/en/reference" : "/th/reference"}
    >
      <a>
        <Flex
          className="bg-img about-ref-wrapper hover-red"
          bg={data && data.refImg.url}
        ></Flex>
      </a>
    </Link>
  );
};
