import React, { useState } from "react";
import { Section, Flex, Text, DivWrapper, Button } from "components";
import { Container, Collapse } from "reactstrap";
import Link from "next/link";
import { useRouter } from "next/router";
export const OurProduct = ({}) => {
  return (
    <Section padding="3em 0">
      <Container>
        <Text.H3 margin="0 0 1em 0" fontColor="blue">
          PRODUCTS
        </Text.H3>
        <Flex className="responsive-column ">
          <BoxContent
            bg="/assets/home/homebg2.jpeg"
            type="digital_displays"
            name="Digital Displays"
            width="50%"
          />
          <BoxContent
            bg="/assets/producttype2.jpeg"
            type="digital_content_management_platform"
            name="Digital Content Management Platform"
            width="50%"
          />
        </Flex>
        <Flex className="responsive-column ">
          <BoxContent
            bg="/assets/producttype3.jpeg"
            type="communication_systems_and_cyber_security"
            name="Communication Systems and Cyber Security"
            width="100%"
          />
        </Flex>
      </Container>
    </Section>
  );
};

const BoxContent = ({ bg, width, type, name }) => {
  const router = useRouter();
  const [hover, setHover] = useState(false);
  return (
    <Flex
      className="responsive-full-width responsive-height250px filter bg-img "
      bg={bg}
      flexDirection="column"
      width={width}
      minHeight="300px"
      justifyContent="center"
      alignItems="center"
      cursor="pointer"
      bgFilter={
        hover
          ? "linear-gradient(rgb(230, 49, 51,0.5), rgb(230, 49, 51,0.5))"
          : "linear-gradient(rgb(0, 0, 0, 0.5), rgb(0, 0, 0, 0.5))"
      }
      onMouseOver={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
    >
      <Text.H4
        fontColor="white"
        textAlign="center"
        weight={hover === true ? 500 : 100}
      >
        {name}
      </Text.H4>
      {/* {type === "digital displays" ? ( */}
      <Link
        href={
          router.route.includes("/en/")
            ? `/en/product/${type}`
            : `/th/product/${type}`
        }
      >
        <a>
          <Button
            margin="0.5em 0"
            bgcolor="red"
            className={hover === true ? " visible-show" : " visible-hide"}
          >
            View now
          </Button>
        </a>
      </Link>
      {/* ) : (
        <Button
          margin="0.5em 0"
          bgcolor="red"
          className={
            hover === true ? "inline visible-show" : "inline visible-hide"
          }
        >
          Not available
        </Button>
      )} */}
    </Flex>
  );
};
