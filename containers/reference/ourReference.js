import React, { useEffect } from "react";
import { Section, Flex, Text, DivWrapper } from "components";
import { Container } from "reactstrap";
import { useRouter } from "next/router";
import {
  Link,
  DirectLink,
  Element,
  Events,
  animateScroll as scroll,
  scrollSpy,
  scroller,
} from "react-scroll";
export const OurReference = ({ refData, th }) => {
  const router = useRouter();
  const duplicateType = [
    ...new Set(refData && refData.map((data, index) => data.refType)),
  ];
  const duplicateSubType = [
    ...new Set(refData && refData.map((data, index) => data.refSubType)),
  ];
  useEffect(() => {
    let id = router.asPath.split("#")[1];
    if (refData && router.asPath.includes("#")) {
      scroller.scrollTo(`${id}`, {
        duration: 500,
        delay: 0,
        smooth: "easeInOutQuart",
        offset: -200,
      });
    }
  });
  return (
    <Section padding="3em 0">
      <Container>
        <Text.H3 weight={500} fontColor="blue" margin="2em 0">
          {th ? "ผลงานที่ผ่านมา" : "REFERENCES"}
        </Text.H3>

        <ReferenceHeader text={duplicateType[0]} />
        <Element name="transportation" />
        <ReferenceComponent
          refData={refData}
          data={duplicateSubType[0]}
          th={th}
        />
        <ReferenceComponent
          refData={refData}
          data={duplicateSubType[1]}
          th={th}
        />
        <ReferenceComponent
          refData={refData}
          data={duplicateSubType[2]}
          th={th}
        />

        <ReferenceHeader text={duplicateType[2]} />
        <Element name="office" />
        <ReferenceComponent
          refData={refData}
          data={duplicateSubType[4]}
          th={th}
        />
        <ReferenceComponent
          refData={refData}
          data={duplicateSubType[5]}
          th={th}
        />
        <ReferenceHeader text={duplicateType[1]} />
        <Element name="other" />
        <ReferenceComponent
          refData={refData}
          data={duplicateSubType[6]}
          th={th}
        />
        <ReferenceComponent
          refData={refData}
          data={duplicateSubType[3]}
          th={th}
        />

        <ReferenceHeader text={duplicateType[3]} />
        <Element name="out" />

        <ReferenceComponent
          refData={refData}
          data={duplicateSubType[7]}
          th={th}
        />
      </Container>
    </Section>
  );
};

const ReferenceComponent = ({ data, refData, th }) => {
  // console.log(
  //   refData
  //     .filter((ref) => ref.refSubType === data)
  //     .sort((a, b) => b.index - a.index)
  // );
  return (
    <Flex
      className="responsive-align-center "
      flexDirection="column"
      width="100%"
      alignItems="flex-start"
      margin="2em 0"
    >
      <Text.H3 weight="500" fontColor="blue" margin="1em 0">
        {data && data.replace(/_/g, " ")}
      </Text.H3>
      <div className="grid1">
        {refData
          .filter((ref) => ref.refSubType === data)
          .sort((a, b) => b.index - a.index)
          .map((data, index) => (
            <Flex
              // className="hoverWrap"
              flexDirection="column"
              jus
              width="320px"
              key={index}
            >
              <img src={data.refImg.url} width="320px" height="200px" />
              <Flex flexDirection="column" width="100%" padding="10px">
                <Text.H5 weight="500" margin="0.5em 0" fontColor="blue">
                  {th ? data.refHeaderTh : data.refHeader}
                </Text.H5>
                <Text.H6 lineHeight="20px" weight={200}>
                  {th ? data.refTextTh : data.refText}
                </Text.H6>
              </Flex>
            </Flex>
          ))}
      </div>
    </Flex>
  );
};

const ReferenceHeader = ({ text, id }) => {
  return (
    <Flex flexDirection="column" width="100%" alignItems="flex-start">
      <DivWrapper className="product-header">
        <Text.H3 fontColor="white" weight={400}>
          {text && text.replace(/_/g, " ")}
        </Text.H3>
      </DivWrapper>
      <DivWrapper className="product-line"></DivWrapper>
    </Flex>
  );
};
