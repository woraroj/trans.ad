import React, { useState, useEffect } from "react";
import { Section, Flex, Text, DivWrapper, Button } from "components";
import { Container, Collapse } from "reactstrap";
import Link from "next/link";
import styled from "styled-components";
import { useRouter } from "next/router";
import {
  DirectLink,
  Element,
  Events,
  animateScroll as scroll,
  scrollSpy,
  scroller,
} from "react-scroll";
export const Intro = ({ data, status, th }) => {
  const [isCollapse, setCollapse] = useState();
  const router = useRouter();
  useEffect(() => {
    let id = router.asPath.split("#")[1];
    if (data && router.asPath.includes("#")) {
      scroller.scrollTo(`${id}`, {
        duration: 500,
        delay: 0,
        smooth: "easeInOutQuart",
        offset: -200,
      });
    }
  });

  console.log(status);
  return (
    <Section padding="5em 0" id="service">
      <Container>
        <Flex flexDirection="column" alignItems="center">
          <Text.H3 weight={500} fontColor="blue">
            {th ? "การบริการ" : "SERVICES"}
          </Text.H3>

          <Flex className="responsive-column " margin="2em 0 0 0" width="100%">
            <BoxContent
              bg={data && data.Serv1Img.url}
              type={data && th ? data.Serv1HeaderTh : data.Serv1HeaderEn}
              width="33%"
              setCollapse={setCollapse}
              index="1"
            />

            <BoxContent
              bg={data && data.Serv2Img.url}
              type={data && th ? data.Serv2HeaderTh : data.Serv2HeaderEn}
              width="33%"
              setCollapse={setCollapse}
              index="2"
            />
            <BoxContent
              bg={data && data.Serv3Img.url}
              type={data && th ? data.Serv3HeaderTh : data.Serv3HeaderEn}
              width="33%"
              setCollapse={setCollapse}
              index="3"
            />
          </Flex>
          <Flex className="responsive-column " margin="0 0 2em 0" width="100%">
            <BoxContent
              bg={data && data.Serv4Img.url}
              type={data && th ? data.Serv4HeaderTh : data.Serv4HeaderEn}
              width="33%"
              setCollapse={setCollapse}
              index="4"
            />
            <BoxContent
              bg={data && data.Serv5Img.url}
              type={data && th ? data.Serv5HeaderTh : data.Serv5HeaderEn}
              width="33%"
              setCollapse={setCollapse}
              index="5"
            />
            <BoxContent
              bg={data && data.Serv6Img.url}
              type={data && th ? data.Serv6HeaderTh : data.Serv6HeaderEn}
              width="33%"
              setCollapse={setCollapse}
              index="6"
            />
          </Flex>
          {/* /////////////////////////////////////////////////////////////////////// */}
          <DivWrapper margin="3em 0">
            {/* <Collapse isOpen={isCollapse === "1"}> */}
            <Flex className="shadow" margin="2em 0" minHeight="500px">
              <Element name="1" />
              <Flex className="responsive-hide " width="50%">
                <img
                  src={data && data.Serv1Img.url}
                  style={{
                    width: "100%",
                    height: "100%",
                    objectFit: "cover",
                  }}
                />
              </Flex>
              <Flex
                className="responsive-full-width "
                padding="2em"
                width="50%"
                flexDirection="column"
                alignItems="flex-start"
              >
                <Text.H3 weight={500} margin="0 0 1em 0" fontColor="blue">
                  {data && th ? data.Serv1HeaderTh : data.Serv1HeaderEn}
                </Text.H3>
                <Text.H5
                  space="1px"
                  weight={200}
                  margin="0 0 1em 0"
                  fontColor="grey"
                  lineHeight="23px"
                >
                  {data && th ? data.Serv1TextTh : data.Serv1TextEn}
                </Text.H5>
              </Flex>
            </Flex>
            {/* </Collapse> */}
            {/* <Collapse isOpen={isCollapse === "2"}> */}
            <Flex className="shadow" margin="2em 0" id="2" minHeight="500px">
              <Element name="2" />
              <Flex className="responsive-hide " width="50%">
                <img
                  src={data && data.Serv2Img.url}
                  style={{
                    width: "100%",
                    height: "100%",
                    objectFit: "cover",
                  }}
                />
              </Flex>
              <Flex
                className="responsive-full-width "
                padding="2em"
                width="50%"
                flexDirection="column"
                alignItems="flex-start"
              >
                <Text.H3 weight={500} margin="0 0 1em 0" fontColor="blue">
                  {data && th ? data.Serv2HeaderTh : data.Serv2HeaderEn}
                </Text.H3>
                <Text.H5
                  space="1px"
                  weight={200}
                  margin="0 0 1em 0"
                  fontColor="grey"
                  lineHeight="23px"
                >
                  {data && th ? data.Serv2TextTh : data.Serv2TextEn}
                </Text.H5>
              </Flex>
            </Flex>
            {/* </Collapse> */}
            {/* <Collapse isOpen={isCollapse === "3"}> */}
            <Flex className="shadow" margin="2em 0" id="3" minHeight="500px">
              <Element name="3" />
              <Flex className="responsive-hide " width="50%">
                <img
                  src={data && data.Serv3Img.url}
                  style={{
                    width: "100%",
                    height: "100%",
                    objectFit: "cover",
                  }}
                />
              </Flex>
              <Flex
                className="responsive-full-width "
                padding="2em"
                width="50%"
                flexDirection="column"
                alignItems="flex-start"
              >
                <Text.H3 weight={500} margin="0 0 1em 0" fontColor="blue">
                  {data && th ? data.Serv3HeaderTh : data.Serv3HeaderEn}
                </Text.H3>
                <Text.H5
                  space="1px"
                  weight={200}
                  margin="0 0 1em 0"
                  fontColor="grey"
                  lineHeight="23px"
                >
                  {data && th ? data.Serv3TextTh : data.Serv3TextEn}
                </Text.H5>
              </Flex>
            </Flex>
            {/* </Collapse> */}
            {/* <Collapse isOpen={isCollapse === "4"}> */}
            <Flex className="shadow" margin="2em 0" id="4" minHeight="500px">
              <Element name="4" />
              <Flex className="responsive-hide " width="50%">
                <img
                  src={data && data.Serv4Img.url}
                  style={{
                    width: "100%",
                    height: "100%",
                    objectFit: "cover",
                  }}
                />
              </Flex>
              <Flex
                className="responsive-full-width "
                padding="2em"
                width="50%"
                flexDirection="column"
                alignItems="flex-start"
              >
                <Text.H3 weight={500} margin="0 0 1em 0" fontColor="blue">
                  {data && th ? data.Serv4HeaderTh : data.Serv4HeaderEn}
                </Text.H3>
                <Text.H5
                  space="1px"
                  weight={200}
                  margin="0 0 1em 0"
                  fontColor="grey"
                  lineHeight="23px"
                >
                  {data && th ? data.Serv4TextTh : data.Serv4TextEn}
                </Text.H5>
              </Flex>
            </Flex>
            {/* </Collapse> */}
            {/* <Collapse isOpen={isCollapse === "5"}> */}
            <Flex className="shadow" margin="2em 0" id="5" minHeight="500px">
              <Element name="5" />
              <Flex className="responsive-hide " width="50%">
                <img
                  src={data && data.Serv5Img.url}
                  style={{
                    width: "100%",
                    height: "100%",
                    objectFit: "cover",
                  }}
                />
              </Flex>
              <Flex
                className="responsive-full-width "
                padding="2em"
                width="50%"
                flexDirection="column"
                alignItems="flex-start"
              >
                <Text.H3 weight={500} margin="0 0 1em 0" fontColor="blue">
                  {data && th ? data.Serv5HeaderTh : data.Serv5HeaderEn}
                </Text.H3>
                <Text.H5
                  space="1px"
                  weight={200}
                  margin="0 0 1em 0"
                  fontColor="grey"
                  lineHeight="23px"
                >
                  {data && th ? data.Serv5TextTh : data.Serv5TextEn}
                </Text.H5>
              </Flex>
            </Flex>
            {/* </Collapse> */}
            {/* <Collapse isOpen={isCollapse === "6"}> */}
            <Flex className="shadow" margin="2em 0" id="6" minHeight="500px">
              <Element name="6" />
              <Flex className="responsive-hide " width="50%">
                <img
                  src={data && data.Serv6Img.url}
                  style={{
                    width: "100%",
                    height: "100%",
                    objectFit: "cover",
                  }}
                />
              </Flex>
              <Flex
                className="responsive-full-width "
                padding="2em"
                width="50%"
                flexDirection="column"
                alignItems="flex-start"
              >
                <Text.H3 weight={500} margin="0 0 1em 0" fontColor="blue">
                  {data && th ? data.Serv6HeaderTh : data.Serv6HeaderEn}
                </Text.H3>
                <Text.H5
                  space="1px"
                  weight={200}
                  margin="0 0 1em 0"
                  fontColor="grey"
                  lineHeight="23px"
                >
                  {data && th ? data.Serv6TextTh : data.Serv6TextEn}
                </Text.H5>
              </Flex>
            </Flex>
            {/* </Collapse> */}
          </DivWrapper>
        </Flex>
      </Container>
    </Section>
  );
};

const BoxContent = ({ bg, width, type, setCollapse, index }) => {
  const [hover, setHover] = useState(false);
  const router = useRouter();
  return (
    <Flex
      className="responsive-full-width responsive-height250px filter bg-img "
      bg={bg}
      flexDirection="column"
      width={width}
      minHeight="300px"
      justifyContent="flex-end"
      alignItems="center"
      cursor="pointer"
      bgFilter={
        hover
          ? "linear-gradient(rgb(230, 49, 51,0.5), rgb(230, 49, 51,0.5))"
          : ""
      }
      onMouseOver={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
      onClick={() => router.push(`#${index}`)}
    >
      <div
        style={{
          background: "rgb(190,187,187)",
          margin: "3px",
          padding: "0 5px",
        }}
      >
        <Text.H5 fontColor="blue" textAlign="center" weight={500}>
          {type}
        </Text.H5>
      </div>
    </Flex>
  );
};
