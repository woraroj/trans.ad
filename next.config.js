const path = require("path");

module.exports = {
  trailingSlash: true,
  async redirects() {
    return [
      {
        source: "/",
        destination: "/en/home", // Matched parameters can be used in the destination
        permanent: false,
      },
    ];
  },
  env: {
    api_url: "https://transad-strapi.herokuapp.com",
  },
  target: "serverless",
  webpack: (config, { isServer }) => {
    if (!isServer) {
      config.node = {
        fs: "empty",
      };
    }

    config.resolve.alias["components"] = path.join(__dirname, "components");
    config.resolve.alias["public"] = path.join(__dirname, "public");

    return config;
  },
};
