import "bootstrap/dist/css/bootstrap.css";
import "../styles/globals.css";
import { ThemeProvider } from "styled-components";
import theme from "../themes";
import { AnimatePresence } from "framer-motion";
import { Header, HeaderTh, Footer } from "components";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import { useRouter } from "next/router";
function MyApp({ Component, pageProps }) {
  const router = useRouter();
  return (
    <ThemeProvider theme={theme}>
      {router.route.includes("/en/") ? <Header /> : <HeaderTh />}
      <AnimatePresence exitBeforeEnter>
        <Component {...pageProps} />
      </AnimatePresence>

      <Footer />
    </ThemeProvider>
  );
}

export default MyApp;
