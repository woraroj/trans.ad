import React, { useEffect } from "react";
import Head from "next/head";
import { Section } from "components";
import { motion } from "framer-motion";
import useSWR from "swr";
import {
  Landing,
  Certificate,
  History,
  Reference,
  News,
  AboutIntro,
} from "../../../containers/aboutus";

import { useRouter } from "next/router";
import {
  DirectLink,
  Element,
  Events,
  animateScroll as scroll,
  scrollSpy,
  scroller,
} from "react-scroll";
const Aboutus = ({
  defaultaboutusData,
  defaultaboutusnewsData,
  defaultabintroData,
}) => {
  const fetcher = (url) => fetch(url).then((r) => r.json());
  const { data: aboutusData, error: aboutusErr } = useSWR(
    `${process.env.api_url}/aboutus-overviews`,
    fetcher
  );
  const { data: aboutusnewsData, error: aboutusnewsErr } = useSWR(
    `${process.env.api_url}/aboutus-news`,
    fetcher
  );
  const { data: introData, error: intorErr } = useSWR(
    `${process.env.api_url}/home`,
    fetcher
  );
  const router = useRouter();
  useEffect(() => {
    let id = router.asPath.split("#")[1];
    if (aboutusData && router.asPath.includes("#")) {
      scroller.scrollTo(`${id}`, {
        duration: 500,
        delay: 0,
        smooth: "easeInOutQuart",
        offset: -200,
      });
    }
  });
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{ padding: "0" }}
    >
      <Section>
        <Head>
          <title>About us • TRANS.AD SOLUTION</title>
          <link rel="icon" href="/assets/logo.png" />
        </Head>

        <Landing data={aboutusData ? aboutusData : defaultaboutusData} />
        <Section bg="/assets/bg.png">
          {!introData ? (
            <AboutIntro
              introImg={defaultabintroData.IntroImg}
              introText={defaultabintroData.IntroEng}
              status={"Aboutus Page - Section Aboutus: Static Generated"}
            />
          ) : (
            <AboutIntro
              introImg={
                introData.IntroImg.url == defaultabintroData.IntroImg.url
                  ? defaultabintroData.IntroImg
                  : introData.IntroImg
              }
              introText={
                introData.IntroEng == defaultabintroData.IntroEng
                  ? defaultabintroData.IntroEng
                  : introData.IntroEng
              }
              status={
                introData.IntroEng == defaultabintroData.IntroEng
                  ? "Aboutus Page - Section Aboutus: Static Generated"
                  : "Aboutus Page - Section Aboutus: useSWR Fetched"
              }
            />
          )}

          {!aboutusData ? (
            <History
              data={defaultaboutusData}
              status={"Aboutus Page - Section Overview: Static Generated"}
            />
          ) : (
            <History
              data={
                JSON.stringify(defaultaboutusData) ===
                JSON.stringify(aboutusData)
                  ? defaultaboutusData
                  : aboutusData
              }
              status={
                JSON.stringify(defaultaboutusData) ===
                JSON.stringify(aboutusData)
                  ? "Aboutus Page - Section Overview: Static Generated"
                  : "Aboutus Page - Section Overview: useSWR Fetched"
              }
            />
          )}
          <Certificate />
          {!aboutusnewsData ? (
            <News
              data={defaultaboutusnewsData}
              status={"Aboutus Page - Section News: Static Generated"}
            />
          ) : (
            <News
              data={
                aboutusnewsData.length === defaultaboutusnewsData.length
                  ? defaultaboutusnewsData
                  : aboutusnewsData
              }
              status={
                aboutusnewsData.length === defaultaboutusnewsData.length
                  ? "Aboutus Page - Section News: Static Generated"
                  : "Aboutus Page - Section News: useSWR Fetched"
              }
            />
          )}
          <Reference />
        </Section>
      </Section>
    </motion.div>
  );
};

export async function getStaticProps() {
  const aboutusData = await fetch(`${process.env.api_url}/aboutus-overviews`);
  const aboutusnewsData = await fetch(`${process.env.api_url}/aboutus-news`);
  const introData = await fetch(`${process.env.api_url}/home`);
  const defaultaboutusData = await aboutusData.json();
  const defaultaboutusnewsData = await aboutusnewsData.json();
  const defaultabintroData = await introData.json();

  return {
    props: { defaultaboutusData, defaultaboutusnewsData, defaultabintroData }, // will be passed to the page component as props
  };
}
export default Aboutus;
