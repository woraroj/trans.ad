import React, { useState, useEffect } from "react";
import { Section, Flex, Text, DivWrapper, Button } from "components";
import { Container, Spinner } from "reactstrap";
import Head from "next/head";
import { motion } from "framer-motion";
import { useRouter } from "next/router";
import { Landing } from "../../../containers";

import axios from "axios";
import Custom404 from "../../404";
import useSWR from "swr";
import {
  Link,
  DirectLink,
  Element,
  Events,
  animateScroll as scroll,
  scrollSpy,
  scroller,
} from "react-scroll";

const Digital_Content_Management_Platform = () => {
  const router = useRouter();

  // function capitalizeLetter(str) {
  //   var splitStr = str.toLowerCase().split(" ");
  //   for (var i = 0; i < splitStr.length; i++) {
  //     splitStr[i] =
  //       splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);
  //   }
  //   return splitStr.join(" ");
  // }

  return (
    <motion.div
      initial={{ x: -60, opacity: 0 }}
      animate={{ x: 0, opacity: 1 }}
      exit={{ x: -60, opacity: 0 }}
    >
      <Section width="100%" minHeight="100vh" id="pro">
        <Head>
          <title>Digital Content Management Platform• TRANS.AD SOLUTION</title>
          <meta
            property="og:url"
            content="	http://transad2.in3.fcomet.com/en/product/"
          />
          <meta property="og:type" content="website" />
          <meta property="fb:app_id" content="315780520323401" />
          <meta property="og:title" content="TRANS.AD SOLUTION" key="title" />
          <meta property="og:description" content="Product" />
          <meta property="og:image" content={"/assets/logo.png"} />
          <link rel="icon" href="/assets/logo.png" />
        </Head>

        <Landing />
        <Container>
          <Section padding="3em 0">
            <Text.H3 weight="500" margin="0 0 1em 0" fontColor="blue">
              PRODUCTS
            </Text.H3>
            <Element className="element" name="product"></Element>
            <ReferenceHeader text="Digital Content Management Platform" />

            <div className="grid1" style={{ margin: "2em 0" }}>
              <img src="/assets/cms3.jpeg" style={{ width: "320px" }} />
              <img
                src="/assets/Booking room2.jpeg"
                style={{ maxWidth: "320px" }}
              />
            </div>
          </Section>
        </Container>
      </Section>
    </motion.div>
  );
};

const ReferenceHeader = ({ text }) => {
  return (
    <Flex flexDirection="column" width="100%" alignItems="flex-start">
      <DivWrapper className="product-header">
        <Text.H3 fontColor="white" weight={400}>
          {text}
        </Text.H3>
      </DivWrapper>
      <DivWrapper className="product-line"></DivWrapper>
    </Flex>
  );
};

export default Digital_Content_Management_Platform;
