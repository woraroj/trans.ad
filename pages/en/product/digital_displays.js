import React, { useState, useEffect } from "react";
import { Section, Flex, Text, DivWrapper, Button } from "components";
import { Container, Spinner } from "reactstrap";
import Head from "next/head";
import { motion } from "framer-motion";
import { useRouter } from "next/router";
import { Landing } from "../../../containers";
import useSWR from "swr";
import {
  Link,
  DirectLink,
  Element,
  Events,
  animateScroll as scroll,
  scrollSpy,
  scroller,
} from "react-scroll";

const Digital_display = ({ defaultData }) => {
  const router = useRouter();
  const fetcher = (url) => fetch(url).then((r) => r.json());
  const { data: typeData, error: typeDataError } = useSWR(
    `${process.env.api_url}/products?Type=Digital_Displays`,
    fetcher
  );

  useEffect(() => {
    // if (typeData && router.query.type === "led screen") {
    //   return scroller.scrollTo("Outdoor LED", {
    //     duration: 500,
    //     delay: 0,
    //     smooth: "easeInOutQuart",
    //     offset: -200,
    //   });
    // } else if (typeData && router.query.type === "lcd screen") {
    //   return scroller.scrollTo("Outdoor LCD", {
    //     duration: 500,
    //     delay: 0,
    //     smooth: "easeInOutQuart",
    //     offset: -200,
    //   });
    // } else if (typeData && router.query.type === "custom-made screen") {
    //   return scroller.scrollTo("Custom-made Screen", {
    //     duration: 500,
    //     delay: 0,
    //     smooth: "easeInOutQuart",
    //     offset: -200,
    //   });
    // } else {
    //   return scroller.scrollTo("product", {
    //     duration: 500,
    //     delay: 0,
    //     smooth: "easeInOutQuart",
    //     offset: -200,
    //   });
    // }

    const id = router.asPath.split("#")[1];
    console.log(id);
    if (router.asPath.includes("#")) {
      scroller.scrollTo(`${id}`, {
        duration: 500,
        delay: 0,
        smooth: "easeInOutQuart",
        offset: -200,
      });
    } else {
      scroller.scrollTo(`product`, {
        duration: 500,
        delay: 0,
        smooth: "easeInOutQuart",
        offset: -200,
      });
    }
  });

  return (
    <motion.div
      initial={{ x: -60, opacity: 0 }}
      animate={{ x: 0, opacity: 1 }}
      exit={{ x: -60, opacity: 0 }}
    >
      <Section width="100%" minHeight="100vh" id="pro">
        <Head>
          <title>Digital Displays • TRANS.AD SOLUTION</title>
          <meta
            property="og:url"
            content="	http://transad2.in3.fcomet.com/en/product/"
          />
          <meta property="og:type" content="website" />
          <meta property="fb:app_id" content="315780520323401" />
          <meta property="og:title" content="TRANS.AD SOLUTION" key="title" />
          <meta property="og:description" content="Product" />
          <meta property="og:image" content={"/assets/logo.png"} />
          <link rel="icon" href="/assets/logo.png" />
        </Head>

        <Landing />
        <Container>
          <Section padding="3em 0">
            <Text.H3 weight="500" margin="0 0 1em 0" fontColor="blue">
              PRODUCTS
            </Text.H3>
            <Element name="product" />
            <ReferenceHeader text="Digital Displays" />
            {!typeData ? (
              <DigitalDisPlay
                typeData={defaultData}
                status={"Digital Display Page: Static Generated"}
              />
            ) : (
              <DigitalDisPlay
                typeData={
                  JSON.stringify(defaultData) === JSON.stringify(typeData)
                    ? defaultData
                    : typeData
                }
                status={
                  JSON.stringify(defaultData) === JSON.stringify(typeData)
                    ? "Digital Display Page: Static Generated"
                    : "Digital Display Page: useSWR Fetched"
                }
              />
            )}
          </Section>
        </Container>
      </Section>
    </motion.div>
  );
};

const DigitalDisPlay = ({ typeData, status }) => {
  console.log(status);

  return (
    <>
      <Text.H3 weight="500" margin="1em 0" fontColor="blue">
        Outdoor LED
      </Text.H3>
      <Element name="led_screen" />
      {typeData
        .filter((sub) => sub.SubType === "Outdoor_LED")
        .sort((a, b) => a.index - b.index)
        .map((data, index) => {
          if (index % 2 == 0) {
            return (
              <LeftTab
                img={data.img.url}
                header={data.header}
                text={data.text}
                key={index}
              />
            );
          } else {
            return (
              <RightTab
                img={data.img.url}
                header={data.header}
                text={data.text}
                key={index}
              />
            );
          }
        })}
      <Text.H3 weight="500" margin="1em 0" fontColor="blue">
        Indoor LED
      </Text.H3>
      {typeData
        .filter((sub) => sub.SubType === "Indoor_LED")
        .sort((a, b) => a.index - b.index)
        .map((data, index) => {
          if (index % 2 == 0) {
            return (
              <LeftTab
                img={data.img.url}
                header={data.header}
                text={data.text}
                key={index}
              />
            );
          } else {
            return (
              <RightTab
                img={data.img.url}
                header={data.header}
                text={data.text}
                key={index}
              />
            );
          }
        })}
      <Text.H3 weight="500" margin="1em 0" fontColor="blue">
        Transparent LED
      </Text.H3>
      {typeData
        .filter((sub) => sub.SubType === "Transparent_LED")
        .sort((a, b) => a.index - b.index)
        .map((data, index) => {
          if (index % 2 == 0) {
            return (
              <LeftTab
                img={data.img.url}
                header={data.header}
                text={data.text}
                key={index}
              />
            );
          } else {
            return (
              <RightTab
                img={data.img.url}
                header={data.header}
                text={data.text}
                key={index}
              />
            );
          }
        })}
      <Text.H3 weight="500" margin="1em 0" fontColor="blue">
        Outdoor LCD
      </Text.H3>
      <Element name="lcd_screen" />
      <Element name="custom-made_screen" />
      {typeData
        .filter((sub) => sub.SubType === "Outdoor_LCD")
        .sort((a, b) => a.index - b.index)
        .map((data, index) => {
          if (index % 2 == 0) {
            return (
              <LeftTab
                img={data.img.url}
                header={data.header}
                text={data.text}
                key={index}
              />
            );
          } else {
            return (
              <RightTab
                img={data.img.url}
                header={data.header}
                text={data.text}
                key={index}
              />
            );
          }
        })}
      <Text.H3 weight="500" margin="1em 0" fontColor="blue">
        Indoor LCD/SLCD
      </Text.H3>
      {typeData
        .filter((sub) => sub.SubType === "Indoor_LCD_SLCD")
        .sort((a, b) => a.index - b.index)
        .map((data, index) => {
          if (index % 2 == 0) {
            return (
              <LeftTab
                img={data.img.url}
                header={data.header}
                text={data.text}
                key={index}
              />
            );
          } else {
            return (
              <RightTab
                img={data.img.url}
                header={data.header}
                text={data.text}
                key={index}
              />
            );
          }
        })}
      <Text.H3 weight="500" margin="1em 0" fontColor="blue">
        Video Wall
      </Text.H3>
      {typeData
        .filter((sub) => sub.SubType === "Video_Wall")
        .sort((a, b) => a.index - b.index)
        .map((data, index) => {
          if (index % 2 == 0) {
            return (
              <LeftTab
                img={data.img.url}
                header={data.header}
                text={data.text}
                key={index}
              />
            );
          } else {
            return (
              <RightTab
                img={data.img.url}
                header={data.header}
                text={data.text}
                key={index}
              />
            );
          }
        })}
      <Text.H3 weight="500" margin="1em 0" fontColor="blue">
        Custom-made Screen
      </Text.H3>
      <Element name="custom-made_screen" />
      {typeData
        .filter((sub) => sub.SubType === "Custom_made_Screen")
        .sort((a, b) => a.index - b.index)
        .map((data, index) => {
          if (index % 2 == 0) {
            return (
              <LeftTab
                img={data.img.url}
                header={data.header}
                text={data.text}
                key={index}
              />
            );
          } else {
            return (
              <RightTab
                img={data.img.url}
                header={data.header}
                text={data.text}
                key={index}
              />
            );
          }
        })}
    </>
  );
};

const LeftTab = ({ img, header, text }) => {
  return (
    <Flex className="productWrap responsive-column responsive-full-height ">
      <Flex
        className="bg-img responsive-full-width contain"
        bg={img}
        width="590px"
      ></Flex>
      <Flex className="productWrap-content responsive-order-1">
        <>
          <Text.H4 fontColor="blue" margin="0 0 0.5em 0" weight="500">
            {header}
          </Text.H4>
          <Text.H6 lineHeight="25px" fontColor="grey">
            {text}
          </Text.H6>
        </>

        <Flex alignItems="center"></Flex>
      </Flex>
    </Flex>
  );
};
const RightTab = ({ img, header, text }) => {
  return (
    <Flex className="productWrap responsive-column responsive-full-height ">
      <Flex className="productWrap-content responsive-order-1">
        <Text.H4 fontColor="blue" margin="0 0 0.5em 0" weight="500">
          {header}
        </Text.H4>
        <Text.H6 lineHeight="25px" fontColor="grey">
          {text}
        </Text.H6>

        <Flex alignItems="center"></Flex>
      </Flex>
      <Flex
        className="bg-img responsive-full-width contain"
        bg={img}
        width="590px"
      ></Flex>
    </Flex>
  );
};
const ReferenceHeader = ({ text }) => {
  return (
    <Flex flexDirection="column" width="100%" alignItems="flex-start">
      <DivWrapper className="product-header">
        <Text.H3 fontColor="white" weight={400}>
          {text}
        </Text.H3>
      </DivWrapper>
      <DivWrapper className="product-line"></DivWrapper>
    </Flex>
  );
};

export async function getStaticProps() {
  const res = await fetch(
    `${process.env.api_url}/products?Type=Digital_Displays`
  );
  const defaultData = await res.json();

  return {
    props: { defaultData }, // will be passed to the page component as props
  };
}
export default Digital_display;
