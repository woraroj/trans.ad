import React from "react";
import Head from "next/head";
import { Section, Flex } from "components";
import useSWR from "swr";
import { Landing, OurReference } from "../../../containers";
import { Spinner } from "reactstrap";
import { motion } from "framer-motion";
const Reference = ({ defaultrefData }) => {
  const fetcher = (url) => fetch(url).then((r) => r.json());

  const { data: refData, error: refDataError } = useSWR(
    `${process.env.api_url}/references`,
    fetcher
  );

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{ padding: "0" }}
    >
      <Section minHeight="80vh">
        <Head>
          <title>References • TRANS.AD SOLUTION</title>
          <link rel="icon" href="/assets/logo.png" />
        </Head>
        <Landing />
        <Section bg="/assets/bg.png" width="100%">
          {!refData ? (
            <OurReference
              refData={defaultrefData}
              status={"Reference page: Static Generated"}
            />
          ) : (
            <OurReference
              refData={
                JSON.stringify(defaultrefData) === JSON.stringify(refData)
                  ? defaultrefData
                  : refData
              }
              status={
                JSON.stringify(defaultrefData) === JSON.stringify(refData)
                  ? "Reference page:  Static Generated"
                  : "Reference page:  useSWR Fetched"
              }
            />
          )}
        </Section>
      </Section>
    </motion.div>
  );
};

export async function getStaticProps() {
  const refData = await fetch(`${process.env.api_url}/references`);

  const defaultrefData = await refData.json();

  return {
    props: { defaultrefData }, // will be passed to the page component as props
  };
}

export default Reference;
