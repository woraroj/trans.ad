import React from "react";
import Head from "next/head";
import { Section } from "components";
import { motion } from "framer-motion";
import { Landing, Intro } from "../../../containers/service";
import useSWR from "swr";
const Service = ({ defaultserviceData }) => {
  const fetcher = (url) => fetch(url).then((r) => r.json());
  const { data: serviceData, error: serviceError } = useSWR(
    `${process.env.api_url}/service`,
    fetcher
  );

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{ padding: "0" }}
    >
      <Section>
        <Head>
          <title>Services • TRANS.AD SOLUTION</title>
          <link rel="icon" href="/assets/logo.png" />
        </Head>

        <Landing />
        <Section bg="/assets/bg.png">
          {!serviceData ? (
            <Intro
              data={defaultserviceData}
              status={"Service page: Static Generated"}
            />
          ) : (
            <Intro
              data={
                JSON.stringify(serviceData) ===
                JSON.stringify(defaultserviceData)
                  ? defaultserviceData
                  : serviceData
              }
              status={
                JSON.stringify(serviceData) ===
                JSON.stringify(defaultserviceData)
                  ? "Service page:  Static Generated"
                  : "Service page:  useSWR Fetched"
              }
            />
          )}
        </Section>
      </Section>
    </motion.div>
  );
};

export async function getStaticProps() {
  const refData = await fetch(`${process.env.api_url}/service`);

  const defaultserviceData = await refData.json();

  return {
    props: { defaultserviceData }, // will be passed to the page component as props
  };
}
export default Service;
