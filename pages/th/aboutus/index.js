import React, { useEffect } from "react";
import Head from "next/head";
import { Section } from "components";
import { motion } from "framer-motion";
import useSWR from "swr";
import {
  Landing,
  Certificate,
  History,
  Reference,
  News,
  AboutIntro,
} from "../../../containers/aboutus";

import { useRouter } from "next/router";
import {
  DirectLink,
  Element,
  Events,
  animateScroll as scroll,
  scrollSpy,
  scroller,
} from "react-scroll";
const Aboutus = ({
  defaultaboutusData,
  defaultaboutusnewsData,
  defaultabintroData,
}) => {
  const fetcher = (url) => fetch(url).then((r) => r.json());
  const { data: aboutusData, error: aboutusErr } = useSWR(
    `${process.env.api_url}/aboutus-overviews`,
    fetcher
  );
  const { data: aboutusnewsData, error: aboutusnewsErr } = useSWR(
    `${process.env.api_url}/aboutus-news`,
    fetcher
  );
  const { data: introData, error: intorErr } = useSWR(
    `${process.env.api_url}/home`,
    fetcher
  );
  const router = useRouter();
  useEffect(() => {
    let id = router.asPath.split("#")[1];
    if (aboutusData && router.asPath.includes("#")) {
      scroller.scrollTo(`${id}`, {
        duration: 500,
        delay: 0,
        smooth: "easeInOutQuart",
        offset: -200,
      });
    }
  });
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{ padding: "0" }}
    >
      <Section>
        <Head>
          <title>เกี่ยวกับเรา • TRANS.AD SOLUTION</title>
          <link rel="icon" href="/assets/logo.png" />
        </Head>

        <Landing data={aboutusData && aboutusData} th={true} />
        <Section bg="/assets/bg.png">
          {!introData ? (
            <AboutIntro
              introImg={defaultabintroData.IntroImg}
              introText={defaultabintroData.IntroTh}
              status={"Section Aboutus: Static Generated"}
              th={true}
            />
          ) : (
            <AboutIntro
              introImg={
                introData.IntroImg.url == defaultabintroData.IntroImg.url
                  ? defaultabintroData.IntroImg
                  : introData.IntroImg
              }
              introText={
                introData.IntroEng == defaultabintroData.IntroTh
                  ? defaultabintroData.IntroTh
                  : introData.IntroTh
              }
              status={
                introData.IntroTh == defaultabintroData.IntroTh
                  ? "Section Aboutus: Static Generated"
                  : "Section Aboutus: useSWR Fetched"
              }
              th={true}
            />
          )}

          {!aboutusData ? (
            <History
              data={defaultaboutusData}
              status={"Section History: Static Generated"}
              th={true}
            />
          ) : (
            <History
              data={
                JSON.stringify(defaultaboutusData) ===
                JSON.stringify(aboutusData)
                  ? defaultaboutusData
                  : aboutusData
              }
              status={
                JSON.stringify(defaultaboutusData) ===
                JSON.stringify(aboutusData)
                  ? "Aboutus Page - Section Overview: Static Generated"
                  : "Aboutus Page - Section Overview: useSWR Fetched"
              }
              th={true}
            />
          )}
          <Certificate th={true} />
          {!aboutusnewsData ? (
            <News
              data={defaultaboutusnewsData}
              status={"Section News: Static Generated"}
              th={true}
            />
          ) : (
            <News
              data={
                aboutusnewsData.length === defaultaboutusnewsData.length
                  ? defaultaboutusnewsData
                  : aboutusnewsData
              }
              status={
                aboutusnewsData.length === defaultaboutusnewsData.length
                  ? "Section News: Static Generated"
                  : "Section News: useSWR Fetched"
              }
              th={true}
            />
          )}
          <Reference th={true} />
        </Section>
      </Section>
    </motion.div>
  );
};

export async function getStaticProps() {
  const aboutusData = await fetch(`${process.env.api_url}/aboutus-overviews`);
  const aboutusnewsData = await fetch(`${process.env.api_url}/aboutus-news`);
  const introData = await fetch(`${process.env.api_url}/home`);
  const defaultaboutusData = await aboutusData.json();
  const defaultaboutusnewsData = await aboutusnewsData.json();
  const defaultabintroData = await introData.json();

  return {
    props: { defaultaboutusData, defaultaboutusnewsData, defaultabintroData }, // will be passed to the page component as props
  };
}
export default Aboutus;
