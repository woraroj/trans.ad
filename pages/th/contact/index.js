import React from "react";
import Head from "next/head";
import { Spinner } from "reactstrap";
import { Section, Flex } from "components";
import useSWR from "swr";
import { Landing, Content } from "../../../containers";
import { motion } from "framer-motion";
const Contact = ({ defaultcontact }) => {
  const fetcher = (url) => fetch(url).then((r) => r.json());

  const { data: partyData, error: partyErr } = useSWR(
    `${process.env.api_url}/contact`,
    fetcher
  );

  const { data: JobData, error: JobErr } = useSWR(
    `${process.env.api_url}/jobs`,
    fetcher
  );

  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{ padding: "0" }}
    >
      <Section minHeight="80vh">
        <Head>
          <title>ติดต่อเรา • TRANS.AD SOLUTION</title>
          <link rel="icon" href="/assets/logo.png" />
        </Head>
        <Landing th={true} />
        <Section bg="/assets/bg.png" width="100%">
          {!partyData ? (
            <Content
              data={defaultcontact}
              job={JobData}
              status={"Contact page: Static Generated"}
              th={true}
            />
          ) : (
            <Content
              data={
                JSON.stringify(partyData) === JSON.stringify(defaultcontact)
                  ? defaultcontact
                  : partyData
              }
              job={JobData}
              status={
                JSON.stringify(partyData) === JSON.stringify(defaultcontact)
                  ? "Contact page:  Static Generated"
                  : "Contact page:  useSWR Fetched"
              }
              th={true}
            />
          )}
        </Section>
      </Section>
    </motion.div>
  );
};
export async function getStaticProps() {
  const refData = await fetch(`${process.env.api_url}/contact`);

  const defaultcontact = await refData.json();

  return {
    props: { defaultcontact }, // will be passed to the page component as props
  };
}
export default Contact;
