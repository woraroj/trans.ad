import React, { useEffect } from "react";
import Head from "next/head";
import { Section } from "components";
import useSWR from "swr";
import {
  Landing,
  Intro,
  Partner,
  Customer,
  Certified,
} from "../../../containers";
import { motion } from "framer-motion";
import { useRouter } from "next/router";
import {
  DirectLink,
  Element,
  Events,
  animateScroll as scroll,
  scrollSpy,
  scroller,
} from "react-scroll";
const Home = ({ defaultData }) => {
  const fetcher = (url) => fetch(url).then((r) => r.json());
  const { data, error } = useSWR(`${process.env.api_url}/home`, fetcher);
  const router = useRouter();
  useEffect(() => {
    let id = router.asPath.split("#")[1];
    if (data && router.asPath.includes("#")) {
      scroller.scrollTo(`${id}`, {
        duration: 500,
        delay: 0,
        smooth: "easeInOutQuart",
        offset: -200,
      });
    }
  });
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{ padding: "0" }}
    >
      <Section>
        <Head>
          <title>หน้าหลัก • TRANS.AD SOLUTION</title>
          <link rel="icon" href="/assets/logo.png" />
        </Head>
        <Landing th={true} />
        <Section bg="/assets/bg.png">
          {!data ? (
            <Intro
              introImg={defaultData.IntroImg}
              introText={defaultData.IntroTh}
              status={"Home page: Static Generated"}
              th={true}
            />
          ) : (
            <Intro
              introImg={
                defaultData.IntroImg.url == data.IntroImg.url
                  ? defaultData.IntroImg
                  : data.IntroImg
              }
              introText={
                data.IntroTh == defaultData.IntroTh
                  ? defaultData.IntroTh
                  : data.IntroTh
              }
              status={
                data.IntroTh == defaultData.IntroTh
                  ? "Home page: Static Generated"
                  : "Home page: useSWR Fetched"
              }
              th={true}
            />
          )}
          {/* <Partner customerImg={data && data.customer} /> */}
        </Section>
      </Section>
    </motion.div>
  );
};

export async function getStaticProps() {
  const res = await fetch(`${process.env.api_url}/home`);
  const defaultData = await res.json();

  return {
    props: { defaultData }, // will be passed to the page component as props
  };
}
export default Home;
