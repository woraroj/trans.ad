import React, { useState, useEffect } from "react";
import { Section, Flex, Text, DivWrapper, Button } from "components";
import { Container, Spinner } from "reactstrap";
import Head from "next/head";
import { motion } from "framer-motion";
import { useRouter } from "next/router";
import { Landing } from "../../../containers";

import axios from "axios";
import Custom404 from "../../404";
import useSWR from "swr";
import {
  Link,
  DirectLink,
  Element,
  Events,
  animateScroll as scroll,
  scrollSpy,
  scroller,
} from "react-scroll";

const Communication_systems_and_cyber_security = ({ defaultData }) => {
  const router = useRouter();
  const fetcher = (url) => fetch(url).then((r) => r.json());
  const { data: typeData, error: typeDataError } = useSWR(
    `${process.env.api_url}/products?Type=Communication_Systems_and_Cyber_Security`,
    fetcher
  );

  return (
    <motion.div
      initial={{ x: -60, opacity: 0 }}
      animate={{ x: 0, opacity: 1 }}
      exit={{ x: -60, opacity: 0 }}
    >
      <Section width="100%" minHeight="100vh" id="pro">
        <Head>
          <title>
            Communication Systems and Cyber Security • TRANS.AD SOLUTION
          </title>
          <meta
            property="og:url"
            content="	http://transad2.in3.fcomet.com/en/product/"
          />
          <meta property="og:type" content="website" />
          <meta property="fb:app_id" content="315780520323401" />
          <meta property="og:title" content="TRANS.AD SOLUTION" key="title" />
          <meta property="og:description" content="Product" />
          <meta property="og:image" content={"/assets/logo.png"} />
          <link rel="icon" href="/assets/logo.png" />
        </Head>

        <Landing th={true} />
        <Container>
          <Section padding="3em 0">
            <Text.H3 weight="500" margin="0 0 1em 0" fontColor="blue">
              สินค้า/ผลิตภัณฑ์
            </Text.H3>
            <Element className="element" name="product"></Element>
            <ReferenceHeader text="ระบบสื่อสารและการรักษาความปลอดภัยของระบบเครือข่าย" />

            {!typeData ? (
              <Communication
                typeData={defaultData}
                status={
                  "Communication Systems and Cyber Security: Static Generated"
                }
              />
            ) : (
              <Communication
                typeData={
                  JSON.stringify(defaultData) === JSON.stringify(typeData)
                    ? defaultData
                    : typeData
                }
                status={
                  JSON.stringify(defaultData) === JSON.stringify(typeData)
                    ? "Communication Systems and Cyber Security Page: Static Generated"
                    : "Communication Systems and Cyber Security Page: useSWR Fetched"
                }
              />
            )}
          </Section>
        </Container>
      </Section>
    </motion.div>
  );
};

const Communication = ({ typeData, status }) => {
  console.log(status);
  return (
    <>
      <Element className="element" name='"Outdoor LED"'></Element>
      {typeData
        .filter(
          (sub) => sub.SubType === "Communication_Systems_and_Cyber_Security"
        )
        .sort((a, b) => a.index - b.index)
        .map((data, index) => {
          if (index % 2 == 0) {
            return (
              <LeftTab
                img={data.img.url}
                header={data.header}
                text={data.textTh}
                key={index}
              />
            );
          } else {
            return (
              <RightTab
                img={data.img.url}
                header={data.header}
                text={data.textTh}
                key={index}
              />
            );
          }
        })}
    </>
  );
};
const LeftTab = ({ img, header, text }) => {
  return (
    <Flex className="productWrap responsive-column responsive-full-height ">
      <Flex
        className="bg-img responsive-full-width contain"
        bg={img}
        width="590px"
      ></Flex>
      <Flex className="productWrap-content responsive-order-1">
        <>
          <Text.H4 fontColor="blue" margin="0 0 0.5em 0" weight="500">
            {header}
          </Text.H4>
          <Text.H6 lineHeight="25px" fontColor="grey">
            {text}
          </Text.H6>
        </>

        <Flex alignItems="center"></Flex>
      </Flex>
    </Flex>
  );
};
const RightTab = ({ img, header, text }) => {
  return (
    <Flex className="productWrap responsive-column responsive-full-height ">
      <Flex className="productWrap-content responsive-order-1">
        <Text.H4 fontColor="blue" margin="0 0 0.5em 0" weight="500">
          {header}
        </Text.H4>
        <Text.H6 lineHeight="25px" fontColor="grey">
          {text}
        </Text.H6>

        <Flex alignItems="center"></Flex>
      </Flex>
      <Flex
        className="bg-img responsive-full-width contain"
        bg={img}
        width="590px"
      ></Flex>
    </Flex>
  );
};
const ReferenceHeader = ({ text }) => {
  return (
    <Flex flexDirection="column" width="100%" alignItems="flex-start">
      <DivWrapper className="product-header">
        <Text.H3 fontColor="white" weight={400}>
          {text}
        </Text.H3>
      </DivWrapper>
      <DivWrapper className="product-line"></DivWrapper>
    </Flex>
  );
};

export async function getStaticProps() {
  const res = await fetch(
    `${process.env.api_url}/products?Type=Communication_Systems_and_Cyber_Security`
  );
  const defaultData = await res.json();

  return {
    props: { defaultData }, // will be passed to the page component as props
  };
}
export default Communication_systems_and_cyber_security;
