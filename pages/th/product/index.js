import React from "react";
import Head from "next/head";
import { Section } from "components";
import useSWR from "swr";
import { Landing, OurProduct } from "../../../containers";
import { motion } from "framer-motion";
const Product = () => {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      animate={{ opacity: 1 }}
      exit={{ opacity: 0 }}
      style={{ padding: "0" }}
    >
      <Section minHeight="80vh">
        <Head>
          <title>Products • TRANS.AD SOLUTION</title>
          <link rel="icon" href="/assets/logo.png" />
        </Head>
        <Landing />
        <Section bg="/assets/bg.png" width="100%">
          <OurProduct />
        </Section>
      </Section>
    </motion.div>
  );
};
export default Product;
