export const Data = [
  {
    url: "/en/home#homeintro",
    value: "Introduction",
    key: "Introduction",
  },
  {
    url: "/en/aboutus",
    value: "About Us",
    key: "About Us",
  },
  {
    url: "/en/aboutus#overview",
    value: "Overview",
    key: "Overview",
  },
  {
    url: "/en/aboutus#overview",
    value: "Roctec Technology",
    key: "Roctec Technology",
  },
  {
    url: "/en/aboutus#overview",
    value: "Trans.Ad Malaysia",
    key: "Trans.Ad Malaysia",
  },
  {
    url: "/en/aboutus#overview",
    value: "Trans.Ad Vietnam",
    key: "Trans.Ad Vietnam",
  },
  {
    url: "/en/aboutus#cer",
    value: "Quality Certification",
    key: "Quality Certification",
  },
  {
    url: "/en/aboutus#news",
    value: "Company News",
    key: "Company News",
  },
  {
    url: "/en/product",
    value: "Product",
    key: "Product",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "Digital Displays",
    key: "Digital Displays",
  },
  {
    url: "/en/product/digital%20content%20management%20platform/",
    value: "Digital Content Management Platform",
    key: "Digital Content Management Platform",
  },
  {
    url: "/en/product/communication%20systems/",
    value: "Comunication Systems",
    key: "Comunication Systems",
  },
  {
    url: "/en/product/network%20security/",
    value: "Network Security",
    key: "Network Security",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "Outdoor High Brightness Pixel Pitch",
    key: "Outdoor High Brightness Pixel Pitch",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "Outdoor Fine Pitch LED Pixel Pitch",
    key: "Outdoor Fine Pitch LED Pixel Pitch",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "Outdoor Seamless Corner Pixel Pitch",
    key: "Outdoor Seamless Corner Pixel Pitch",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "Outdoor Section Aluminium Standard LED Display",
    key: "Outdoor Section Aluminium Standard LED Display",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "Outdoor Mesh LED Pixel Pitch",
    key: "Outdoor Mesh LED Pixel Pitch",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "Outdoor Billboard LED Display Pixel Pitch",
    key: "Outdoor Billboard LED Display Pixel Pitch",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "Outdoor Pixel Pitch",
    key: "Outdoor Pixel Pitch",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "Indoor LED",
    key: "Indoor LED",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "Outdoor LED",
    key: "Outdoor LED",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "High Flatness Front Maintenance Pixel Pitch",
    key: "High Flatness Front Maintenance Pixel Pitch",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "Ultra Slim & Light Design Pixel Pitch",
    key: "Ultra Slim & Light Design Pixel Pitch",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "Pixel Pitch",
    key: "Pixel Pitch",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "Fine Pitch with Commercial-Grade Pixel Pitch",
    key: "Fine Pitch with Commercial-Grade Pixel Pitch",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "Ultra Light Weight Carbon Fiber LED Display",
    key: "Ultra Light Weight Carbon Fiber LED Display",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "Flexible LED Display Pixel Pitch",
    key: "Flexible LED Display Pixel Pitch",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "Transparent LED",
    key: "Transparent LED",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "Transparent digital poster",
    key: "Transparent digital poster",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "Transparent LED",
    key: "Transparent LED",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "Glass LED Barrier",
    key: "Glass LED Barrier",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "Ultra-transparent glass LED",
    key: "Ultra-transparent glass LED",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "Customized LED",
    key: "Customized LED",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "Perimeter Screen Pixel Pitch",
    key: "Perimeter Screen Pixel Pitch",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "Arena Hanging Screen Pixel Pitch",
    key: "Arena Hanging Screen Pixel Pitch",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "Sliding Door",
    key: "Sliding Door",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "Special Shape",
    key: "Special Shape",
  },
  {
    url: "/en/product/digital%20displays/",
    value: "Customized LED",
    key: "Customized LED",
  },
  {
    url: "/en/product/communication%20systems/",
    value: "Passenger Information System",
    key: "Passenger Information System",
  },
  {
    url: "/en/product/communication%20systems/",
    value: "Dynamic Route Map",
    key: "Dynamic Route Map",
  },
  {
    url: "/en/product/communication%20systems/",
    value: "CCTV",
    key: "CCTV",
  },
  {
    url: "/en/product/communication%20systems/",
    value: "Data Network (Wireless Access Point",
    key: "Data Network (Wireless Access Point",
  },
  {
    url: "/en/service",
    value: "Service",
    key: "Service",
  },
  {
    url: "/en/service#1",
    value: "Project Management",
    key: "Project Management",
  },
  {
    url: "/en/service#2",
    value: "Systems Integration & Implementation",
    key: "Systems Integration & Implementation",
  },
  {
    url: "/en/service#3",
    value: "Content Management",
    key: "Content Management",
  },
  {
    url: "/en/service#4",
    value: "Maintenance Services",
    key: "Maintenance Services",
  },
  {
    url: "/en/service#5",
    value: "Digital Creatives and 3D Content Production",
    key: "Digital Creatives and 3D Content Production",
  },
  {
    url: "/en/service#6",
    value: "Consultation",
    key: "Consultation",
  },
  {
    url: "/en/reference",
    value: "Reference",
    key: "Reference",
  },
  {
    url: "/en/reference#transportation",
    value: "Transportation",
    key: "Transportation",
  },
  {
    url: "/en/reference#transportation",
    value: "In train - Digital Display and Communications System",
    key: "In train - Digital Display and Communications System",
  },
  {
    url: "/en/reference#transportation",
    value: "Passenger information system (PIS)",
    key: "Passenger information system (PIS)",
  },
  {
    url: "/en/reference#transportation",
    value: "Dynamic Route Map (DRM)",
    key: "Dynamic Route Map (DRM)",
  },
  {
    url: "/en/reference#transportation",
    value: "CCTV (In-train)",
    key: "CCTV (In-train)",
  },
  {
    url: "/en/reference#transportation",
    value: "On station - Digital Display and Communications System",
    key: "On station - Digital Display and Communications System",
  },
  {
    url: "/en/reference#transportation",
    value: "Platform Truss LED Screens",
    key: "Platform Truss LED Screens",
  },
  {
    url: "/en/reference#transportation",
    value: "Platform Screen Door LCD Screens",
    key: "Platform Screen Door LCD Screens",
  },
  {
    url: "/en/reference#transportation",
    value: "55” LCD TV",
    key: "55” LCD TV",
  },
  {
    url: "/en/reference#transportation",
    value: "Electronic Poster",
    key: "Electronic Poster",
  },
  {
    url: "/en/reference#transportation",
    value: "High Definition Curve Screens",
    key: "High Definition Curve Screens",
  },
  {
    url: "/en/reference#transportation",
    value: "CCTV on station",
    key: "CCTV on station",
  },
  {
    url: "/en/reference#transportation",
    value: "Face recognition (Interface with Rabbit LINE Pay)",
    key: "Face recognition (Interface with Rabbit LINE Pay)",
  },

  {
    url: "/en/reference#transportation",
    value: "Sidebeam LED at Siam Station",
    key: "Sidebeam LED at Siam Station",
  },
  {
    url: "/en/reference#transportation",
    value: "Lift LED at Siam Station",
    key: "Lift LED at Siam Station",
  },
  {
    url: "/en/reference#transportation",
    value: "Escalator Stand & Stairway Stand LED",
    key: "Escalator Stand & Stairway Stand LED",
  },
  {
    url: "/en/reference#transportation",
    value: "Airports - Digital Display and Communications System",
    key: "Airports - Digital Display and Communications System",
  },
  {
    url: "/en/reference#transportation",
    value: "Flight Information Display System",
    key: "Flight Information Display System",
  },
  {
    url: "/en/reference#out",
    value: "Out Of Home Media",
    key: "Out Of Home Media",
  },
  {
    url: "/en/reference#out",
    value: "Outdoor LED",
    key: "Outdoor LED",
  },
  {
    url: "/en/reference#out",
    value: "MBK Outdoor P16",
    key: "MBK Outdoor P16",
  },
  {
    url: "/en/reference#out",
    value: "Street LED",
    key: "Street LED",
  },
  {
    url: "/en/reference#out",
    value: "KPN Outdoor P16",
    key: "KPN Outdoor P16",
  },
  {
    url: "/en/reference#out",
    value: "Eastin Hotel Makasan",
    key: "Eastin Hotel Makasan",
  },
  {
    url: "/en/reference#out",
    value: "Trivision Seamless curve outdoor P16",
    key: "Trivision Seamless curve outdoor P16",
  },
  {
    url: "/en/reference#out",
    value: "BTS Column outdoor P6 with corner",
    key: "BTS Column outdoor P6 with corner",
  },
  {
    url: "/en/reference#out",
    value: "Empire outdoor LED P16",
    key: "Empire outdoor LED P16",
  },
  {
    url: "/en/reference#out",
    value: "Center point siam square",
    key: "Center point siam square",
  },
  {
    url: "/en/reference#out",
    value: "Interchange (Asoke)",
    key: "Interchange (Asoke)",
  },
  {
    url: "/en/reference#out",
    value: "Indoor LED",
    key: "Indoor LED",
  },
  {
    url: "/en/reference#out",
    value: "VGI Lobby",
    key: "VGI Lobby",
  },
  {
    url: "/en/reference#out",
    value: "The stock exchange of Thailand",
    key: "The stock exchange of Thailand",
  },
  {
    url: "/en/reference#out",
    value: "Topnews studio",
    key: "Topnews studio",
  },
  {
    url: "/en/reference#office",
    value: "Office Building",
    key: "Office Building",
  },
  {
    url: "/en/reference#office",
    value: "Lift",
    key: "Lift",
  },
  {
    url: "/en/reference#office",
    value: "Lift Embedded LCD Displays",
    key: "Lift Embedded LCD Displays",
  },
  {
    url: "/en/reference#office",
    value: "Lobby",
    key: "Lobby",
  },
  {
    url: "/en/reference#office",
    value: "Lobby LCD Displays",
    key: "Lobby LCD Displays",
  },
  {
    url: "/en/reference#office",
    value: "Digital Signage and Directory",
    key: "Digital Signage and Directory",
  },
  {
    url: "/en/reference#other",
    value: "Others",
    key: "Others",
  },
  {
    url: "/en/reference#other",
    value: "LINE Beacon",
    key: "LINE Beacon",
  },
  {
    url: "/en/reference#other",
    value:
      "Face recognition (Interface with Rabbit LINE Pay)Digital Signage and Directory",
    key: "Face recognition (Interface with Rabbit LINE Pay)Digital Signage and Directory",
  },
  {
    url: "/en/reference#other",
    value: "Smartphone Interactive Media",
    key: "Smartphone Interactive Media",
  },
  {
    url: "/en/contact/#con",
    value: "Contact",
    key: "Contact",
  },
  {
    url: "/en/contact/#con",
    value: "Email: info@transad.co.th",
    key: "Email: info@transad.co.th",
  },
  {
    url: "/en/contact/#con",
    value: "Fax: +66 (0) 2-001-9903",
    key: "Fax: +66 (0) 2-001-9903",
  },
  {
    url: "/en/contact/#con",
    value: "Tel: +66 (0) 2-001-9900-2",
    key: "Tel: +66 (0) 2-001-9900-2",
  },
  {
    url: "/en/contact/#why",
    value: "Why Join Us?",
    key: "Why Join Us?",
  },
  {
    url: "/en/contact/#why",
    value: "Benefits",
    key: "Benefits",
  },
  {
    url: "/en/contact/#why",
    value: "Company Activities",
    key: "Company Activities",
  },
  {
    url: "/en/contact/#job",
    value: "Job Opening",
    key: "Job Opening",
  },
  {
    url: "/en/contact/#job",
    value: "Carrer",
    key: "Carrer",
  },
];
